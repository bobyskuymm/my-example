<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * NewsArticle
 *
 * @FileStore\Uploadable
 * @ORM\Table(name="news_articles")
 * @Gedmo\TranslationEntity(class="NewsArticleTranslation")
 * @ORM\Entity
 */
class NewsArticle extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=512)
     */
    private $name;

    /**
     * @var array
     *
     * @Assert\File(maxSize="2M")
     * @FileStore\UploadableField(mapping="news_image")
     *
     * @ORM\Column(name="image", type="array", nullable=true)
     */
    protected $image;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="short_description", type="text", nullable=true)
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="seo_title", type="string", length=512, nullable=true)
     */
    private $seoTitle;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="seo_keywords", type="string", length=512, nullable=true)
     */
    private $seoKeywords;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="seo_description", type="string", length=512, nullable=true)
     */
    private $seoDescription;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="pub_date", type="datetime")
     */
    private $pubDate;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(length=128, unique=true)
     */
    private $url;

    /**
     * @ORM\ManyToMany(targetEntity="NewsArticleTag", inversedBy="articles", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="news_article_tags_index",
     *      joinColumns={@ORM\JoinColumn(name="news_article_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     * )
     */
    private $tags;

    /**
     * @ORM\ManyToMany(targetEntity="Clinic", inversedBy="linkedNews", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="news_article_clinic_index",
     *      joinColumns={@ORM\JoinColumn(name="news_article_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="clinic_id", referencedColumnName="id")}
     * )
     */
    private $linkedClinics;

    /**
     * @ORM\OneToMany(
     *   targetEntity="NewsArticleTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;


    public function __toString()
    {
        return ($this->getName()) ? $this->getName() : '';
    }

    public function getIdentifier()
    {
        return $this->getId() . '-' . $this->getUpdatedAt()->format('dmY-Hi');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return NewsArticle
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     *
     * @return NewsArticle
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return NewsArticle
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     *
     * @return NewsArticle
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;

        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Set seoKeywords
     *
     * @param string $seoKeywords
     *
     * @return NewsArticle
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seoKeywords
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     *
     * @return NewsArticle
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return NewsArticle
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return NewsArticle
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return NewsArticle
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Set image
     *
     * @param array $image
     * @return NewsArticle
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return array
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return NewsArticle
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     * @return NewsArticle
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setPubDate(new \DateTime());
    }

    /**
     * Add tags
     *
     * @param \AppBundle\Entity\NewsArticleTag $tags
     * @return NewsArticle
     */
    public function addTag(\AppBundle\Entity\NewsArticleTag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \AppBundle\Entity\NewsArticleTag $tags
     */
    public function removeTag(\AppBundle\Entity\NewsArticleTag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add linkedClinics
     *
     * @param \AppBundle\Entity\Clinic $linkedClinics
     * @return NewsArticle
     */
    public function addLinkedClinic(\AppBundle\Entity\Clinic $linkedClinics)
    {
        $this->linkedClinics[] = $linkedClinics;

        return $this;
    }

    /**
     * Remove linkedClinics
     *
     * @param \AppBundle\Entity\Clinic $linkedClinics
     */
    public function removeLinkedClinic(\AppBundle\Entity\Clinic $linkedClinics)
    {
        $this->linkedClinics->removeElement($linkedClinics);
    }

    /**
     * Get linkedClinics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinkedClinics()
    {
        return $this->linkedClinics;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\NewsArticleTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\NewsArticleTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Set pubDate
     *
     * @param \DateTime $pubDate
     * @return NewsArticle
     */
    public function setPubDate($pubDate)
    {
        $this->pubDate = $pubDate;

        return $this;
    }

    /**
     * Get pubDate
     *
     * @return \DateTime
     */
    public function getPubDate()
    {
        return $this->pubDate;
    }
}
