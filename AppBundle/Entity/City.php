<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Clinic
 *
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CityRepository")
 * @Gedmo\TranslationEntity(class="CityTranslation")
 */
class City extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=512, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(
     *   targetEntity="CityTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Clinic", mappedBy="city")
     */
    protected $city;

    /**
     * @ORM\OneToMany(targetEntity="Doctor", mappedBy="city")
     */
    protected $doctors;

    /**
     * @ORM\ManyToMany(targetEntity="Area", mappedBy="cities")
     * @ORM\JoinTable(name="doctor_languages_index",
     *      joinColumns={@ORM\JoinColumn(name="language_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="doctor_id", referencedColumnName="id")}
     * )
     */
    private $areas;


    /**
     * @ORM\OneToMany(targetEntity="Realty", mappedBy="city")
     */
    protected $realties;


    public function __toString()
    {
        if ($this->getName()) {
            return $this->getName();
        }
        return "";
    }


    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Clinic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\ClinicTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\ClinicTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }


    /**
     * Add city
     *
     * @param \AppBundle\Entity\Clinic $city
     * @return City
     */
    public function addCity(\AppBundle\Entity\Clinic $city)
    {
        $this->city[] = $city;

        return $this;
    }

    /**
     * Remove city
     *
     * @param \AppBundle\Entity\Clinic $city
     */
    public function removeCity(\AppBundle\Entity\Clinic $city)
    {
        $this->city->removeElement($city);
    }

    /**
     * Get city
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add areas
     *
     * @param \AppBundle\Entity\Area $areas
     * @return City
     */
    public function addArea(\AppBundle\Entity\Area $areas)
    {
        $this->areas[] = $areas;

        return $this;
    }

    /**
     * Add areas
     *
     * @param \AppBundle\Entity\Area $area
     * @return City
     */
    public function hasArea(Area $area)
    {
        return $this->areas->contains($area);
    }

    /**
     * Remove areas
     *
     * @param \AppBundle\Entity\Area $areas
     */
    public function removeArea(\AppBundle\Entity\Area $areas)
    {
        $this->areas->removeElement($areas);
    }

    /**
     * Get areas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAreas()
    {
        return $this->areas;
    }

    /**
     * Add doctors
     *
     * @param \AppBundle\Entity\Doctor $doctors
     * @return City
     */
    public function addDoctor(\AppBundle\Entity\Doctor $doctors)
    {
        $this->doctors[] = $doctors;

        return $this;
    }

    /**
     * Remove doctors
     *
     * @param \AppBundle\Entity\Doctor $doctors
     */
    public function removeDoctor(\AppBundle\Entity\Doctor $doctors)
    {
        $this->doctors->removeElement($doctors);
    }

    /**
     * Get doctors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDoctors()
    {
        return $this->doctors;
    }

    /**
     * Add realties
     *
     * @param \AppBundle\Entity\Realty $realties
     * @return City
     */
    public function addRealty(\AppBundle\Entity\Realty $realties)
    {
        $this->realties[] = $realties;

        return $this;
    }

    /**
     * Remove realties
     *
     * @param \AppBundle\Entity\Realty $realties
     */
    public function removeRealty(\AppBundle\Entity\Realty $realties)
    {
        $this->realties->removeElement($realties);
    }

    /**
     * Get realties
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRealties()
    {
        return $this->realties;
    }
}
