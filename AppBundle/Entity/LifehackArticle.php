<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * LifehackArticle
 *
 * @FileStore\Uploadable
 * @ORM\Table(name="lifehack_articles")
 * @Gedmo\TranslationEntity(class="LifehackArticleTranslation")
 * @ORM\Entity
 */
class LifehackArticle extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=512)
     */
    private $name;


    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="photo_title", type="string", length=512)
     */
    private $photoTitle;

    /**
     * @var array
     *
     * @Assert\File(maxSize="2M")
     * @FileStore\UploadableField(mapping="lifehack_image")
     *
     * @ORM\Column(name="image", type="array", nullable=true)
     */
    protected $image;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="short_description", type="text", nullable=true)
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="LifehackArticleSection", inversedBy="articles")
     * @ORM\JoinColumn(name="section_id", referencedColumnName="id")
     */
    protected $section;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="seo_title", type="string", length=512, nullable=true)
     */
    private $seoTitle;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="seo_keywords", type="string", length=512, nullable=true)
     */
    private $seoKeywords;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="seo_description", type="string", length=512, nullable=true)
     */
    private $seoDescription;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(length=128, unique=true)
     */
    private $url;

    /**
     * @ORM\ManyToMany(targetEntity="LifehackArticleTag", inversedBy="articles", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="lifehack_article_tags_index",
     *      joinColumns={@ORM\JoinColumn(name="lifehack_article_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     * )
     */
    private $tags;

    /**
     * @ORM\ManyToMany(targetEntity="Clinic", inversedBy="linkedLifehack", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="lifehack_article_clinic_index",
     *      joinColumns={@ORM\JoinColumn(name="lifehack_article_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="clinic_id", referencedColumnName="id")}
     * )
     */
    private $linkedClinics;

    /**
     * @ORM\OneToMany(
     *   targetEntity="LifehackArticleTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    public function __toString()
    {
        return ($this->getName()) ? $this->getName() : '';
    }

    public function getIdentifier()
    {
        return $this->getId() . '-' . $this->getUpdatedAt()->format('dmY-Hi');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LifehackArticle
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     *
     * @return LifehackArticle
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return LifehackArticle
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     *
     * @return LifehackArticle
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;

        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Set seoKeywords
     *
     * @param string $seoKeywords
     *
     * @return LifehackArticle
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seoKeywords
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     *
     * @return LifehackArticle
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LifehackArticle
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return LifehackArticle
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return LifehackArticle
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Set image
     *
     * @param array $image
     * @return LifehackArticle
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return array
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return LifehackArticle
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     * @return LifehackArticle
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add tags
     *
     * @param \AppBundle\Entity\LifehackArticleTag $tags
     * @return LifehackArticle
     */
    public function addTag(\AppBundle\Entity\LifehackArticleTag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \AppBundle\Entity\LifehackArticleTag $tags
     */
    public function removeTag(\AppBundle\Entity\LifehackArticleTag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set section
     *
     * @param \AppBundle\Entity\LifehackArticleSection $section
     * @return LifehackArticle
     */
    public function setSection(\AppBundle\Entity\LifehackArticleSection $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \AppBundle\Entity\LifehackArticleSection
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Add linkedClinics
     *
     * @param \AppBundle\Entity\Clinic $linkedClinics
     * @return LifehackArticle
     */
    public function addLinkedClinic(\AppBundle\Entity\Clinic $linkedClinics)
    {
        $this->linkedClinics[] = $linkedClinics;

        return $this;
    }

    /**
     * Remove linkedClinics
     *
     * @param \AppBundle\Entity\Clinic $linkedClinics
     */
    public function removeLinkedClinic(\AppBundle\Entity\Clinic $linkedClinics)
    {
        $this->linkedClinics->removeElement($linkedClinics);
    }

    /**
     * Get linkedClinics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinkedClinics()
    {
        return $this->linkedClinics;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\LifehackArticleTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\LifehackArticleTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Set photoTitle
     *
     * @param string $photoTitle
     * @return LifehackArticle
     */
    public function setPhotoTitle($photoTitle)
    {
        $this->photoTitle = $photoTitle;

        return $this;
    }

    /**
     * Get photoTitle
     *
     * @return string 
     */
    public function getPhotoTitle()
    {
        return $this->photoTitle;
    }
}
