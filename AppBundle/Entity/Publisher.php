<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Publisher
 *
 * @FileStore\Uploadable
 * @ORM\Table(name="publishers")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="PublisherTranslation")
 */
class Publisher extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=512)
     */
    private $name;

    /**
     * @var array
     *
     * @Assert\File(maxSize="2M")
     * @FileStore\UploadableField(mapping="publisher_logo")
     *
     * @ORM\Column(name="logo", type="array", nullable=true)
     */
    protected $logo;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(length=128, unique=true)
     */
    private $url;

    /**
     * @ORM\OneToMany(targetEntity="MediaAboutUs", mappedBy="publisher")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $publications;

    /**
     * @ORM\OneToMany(
     *   targetEntity="PublisherTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;


    public function __toString()
    {
        return ($this->getName()) ? $this->getName() : '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Publisher
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set logo
     *
     * @param array $logo
     * @return Publisher
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return array
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Publisher
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->publications = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add publications
     *
     * @param \AppBundle\Entity\MediaAboutUs $publications
     * @return Publisher
     */
    public function addPublication(\AppBundle\Entity\MediaAboutUs $publications)
    {
        $this->publications[] = $publications;

        return $this;
    }

    /**
     * Remove publications
     *
     * @param \AppBundle\Entity\MediaAboutUs $publications
     */
    public function removePublication(\AppBundle\Entity\MediaAboutUs $publications)
    {
        $this->publications->removeElement($publications);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublications()
    {
        return $this->publications;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\PublisherTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\PublisherTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }
}
