<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DoctorPhone
 *
 * @ORM\Table(name="doctor_phones")
 * @ORM\Entity
 */
class DoctorPhone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="verify_code", type="string", length=255, nullable=true)
     */
    private $verifyCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="verified_at", type="datetime", nullable=true)
     */
    private $verifiedAt;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Doctor", inversedBy="phones")
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id")
     */
    protected $doctor;

    public function __construct()
    {
        // $this->setVerifiedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return DoctorPhone
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set verifiedAt
     *
     * @param \DateTime $verifiedAt
     * @return DoctorPhone
     */
    public function setVerifiedAt($verifiedAt)
    {
        $this->verifiedAt = $verifiedAt;

        return $this;
    }

    /**
     * Get verifiedAt
     *
     * @return \DateTime
     */
    public function getVerifiedAt()
    {
        return $this->verifiedAt;
    }

    /**
     * Set doctor
     *
     * @param \AppBundle\Entity\Doctor $doctor
     * @return DoctorPhone
     */
    public function setDoctor(\AppBundle\Entity\Doctor $doctor = null)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor
     *
     * @return \AppBundle\Entity\Doctor
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set verifyCode
     *
     * @param string $verifyCode
     * @return DoctorPhone
     */
    public function setVerifyCode($verifyCode)
    {
        $this->verifyCode = $verifyCode;

        return $this;
    }

    /**
     * Get verifyCode
     *
     * @return string 
     */
    public function getVerifyCode()
    {
        return $this->verifyCode;
    }
}
