<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RealtyRating
 *
 * @ORM\Table(name="realty_ratings")
 * @ORM\Entity
 */
class RealtyRating
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cleanliness", type="integer", nullable=true)
     */
    private $cleanliness;

    /**
     * @var integer
     *
     * @ORM\Column(name="location", type="integer", nullable=true)
     */
    private $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="communication", type="integer", nullable=true)
     */
    private $communication;

    /**
     * @var integer
     *
     * @ORM\Column(name="pricequality", type="integer", nullable=true)
     */
    private $pricequality;

    /**
     * @ORM\OneToOne(targetEntity="RealtyComment", mappedBy="rating")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $comment;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;


    public function getTotalRating()
    {
        return $this->cleanliness +
               $this->location +
               $this->communication +
               $this->pricequality;
    }

    public function getCalculatedRating()
    {
        return $this->getTotalRating() / 4;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cleanliness
     *
     * @param integer $cleanliness
     * @return RealtyRating
     */
    public function setCleanliness($cleanliness)
    {
        $this->cleanliness = $cleanliness;

        return $this;
    }

    /**
     * Get cleanliness
     *
     * @return integer 
     */
    public function getCleanliness()
    {
        return $this->cleanliness;
    }

    /**
     * Set location
     *
     * @param integer $location
     * @return RealtyRating
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return integer 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set communication
     *
     * @param integer $communication
     * @return RealtyRating
     */
    public function setCommunication($communication)
    {
        $this->communication = $communication;

        return $this;
    }

    /**
     * Get communication
     *
     * @return integer 
     */
    public function getCommunication()
    {
        return $this->communication;
    }

    /**
     * Set pricequality
     *
     * @param integer $pricequality
     * @return RealtyRating
     */
    public function setPricequality($pricequality)
    {
        $this->pricequality = $pricequality;

        return $this;
    }

    /**
     * Get pricequality
     *
     * @return integer 
     */
    public function getPricequality()
    {
        return $this->pricequality;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return RealtyRating
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return RealtyRating
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set comment
     *
     * @param \AppBundle\Entity\RealtyComment $comment
     * @return RealtyRating
     */
    public function setComment(\AppBundle\Entity\RealtyComment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \AppBundle\Entity\RealtyComment 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return RealtyRating
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     * @return RealtyRating
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
