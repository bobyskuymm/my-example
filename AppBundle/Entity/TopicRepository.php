<?php

namespace AppBundle\Entity;

/**
 * TopicRepository
 *
 */
class TopicRepository extends \Doctrine\ORM\EntityRepository
{

    public function getMostCommented()
    {

        $stmt = $this->getEntityManager()
                     ->getConnection()
                     ->prepare("SELECT * FROM threads WHERE id LIKE 'topic_%' ORDER BY comment_count DESC LIMIT 10");

        $stmt->execute();

        $result = $stmt->fetchAll();

        $topics = [];

        foreach ($result as $row) {
            if (count($topics) < 5) {
                list($else, $id) = explode("_", $row['id']);
                $topic = $this->findOneById($id);
                if ($topic) {
                    $topics[] = $topic;
                }
            }
        }

        return $topics;
    }

    public function getMostCommentedByDoctor($doctorId)
    {

        $sql = "
            SELECT id FROM topics WHERE id IN (
            SELECT topic_id FROM (
            SELECT
                SUBSTR(thread_id, 7) AS topic_id,
                COUNT(id) AS count
            FROM comments
            WHERE thread_id LIKE 'topic_%' AND created_by = " . $doctorId . "
            GROUP BY thread_id
            ORDER BY count DESC, created_at DESC
            ) AS cc
            )
        ";
        // dump($sql); die();
        $stmt = $this->getEntityManager()
                     ->getConnection()
                     ->prepare($sql);

        $stmt->execute();

        $result = $stmt->fetchAll();

        $topics = [];

        foreach ($result as $row) {
            $topic = $this->findOneById($row['id']);
            if ($topic) {
                $topics[] = $topic;
            }
        }

        return $topics;
    }

    public function getByFilter($filter = NULL)
    {

        $topics =
            $this->createQueryBuilder("Topic")
                 ->select("Topic")
                 ->leftJoin("Topic.tags", "Tag")
                 ->leftJoin("Topic.section", "Section")
        ;

        if (!empty($filter['tags'])) {
            $topics = $topics
                ->andWhere("Tag.id IN (:tags)")
                ->setParameter(":tags", $filter['tags']);
        }

        if (!empty($filter['section'])) {
            $topics = $topics
                ->andWhere("Section.id = :section")
                ->setParameter(":section", $filter['section']);
        }

        if (!empty($filter['query'])) {
            $topics = $topics
                 ->andWhere("Topic.title LIKE :name")
                 ->setParameter(":name", '%' . $filter['query'] . '%');
        }

        $topics = $topics
            ->groupBy('Topic.id')
            ->orderBy('Topic.updatedAt', 'DESC')
        ;

        $topics = $topics->getQuery();

        // dump($topics); dump($filter);
        // die();

        return $topics;
    }




}
