<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * LifehackArticleTag
 *
 * @ORM\Table(name="lifehack_article_tags")
 * @ORM\Entity
 * @UniqueEntity("name")
 * @Gedmo\TranslationEntity(class="LifehackArticleTagTranslation")
 */
class LifehackArticleTag extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(length=128, unique=true)
     */
    private $url;

    /**
     * @ORM\ManyToMany(targetEntity="LifehackArticle", mappedBy="tags")
     * @ORM\JoinTable(name="lifehack_article_tags_index",
     *      joinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="lifehack_article_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $articles;

    /**
     * @var $clearUrl string
     */
    public $clearUrl;

    /**
     * @ORM\OneToMany(
     *   targetEntity="LifehackArticleTagTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    public function __toString()
    {
        return ($this->getName()) ? $this->getName() : '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return LifehackArticleTag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set url
     *
     * @param string $url
     * @return LifehackArticleTag
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add articles
     *
     * @param \AppBundle\Entity\LifehackArticle $articles
     * @return LifehackArticleTag
     */
    public function addArticle(\AppBundle\Entity\LifehackArticle $articles)
    {
        $this->articles[] = $articles;

        return $this;
    }

    /**
     * Remove articles
     *
     * @param \AppBundle\Entity\LifehackArticle $articles
     */
    public function removeArticle(\AppBundle\Entity\LifehackArticle $articles)
    {
        $this->articles->removeElement($articles);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\LifehackArticleTagTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\LifehackArticleTagTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }
}
