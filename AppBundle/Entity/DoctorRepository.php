<?php

namespace AppBundle\Entity;

/**
 * DoctorRepository
 *
 */
class DoctorRepository extends \Doctrine\ORM\EntityRepository
{

    public function getDoctors()
    {
        $q = $this->createQueryBuilder('d');
        $q->orderBy('d.id','ASC');
        return $q->getQuery()->getResult();
    }

    public function getByFilter($filter = NULL)
    {

        $doctors =
            $this->createQueryBuilder("Doctor")
                 ->select("Doctor", "Location.name AS HIDDEN location,
                                     Doctor.experience AS HIDDEN experience,
                                     Doctor.childbirthPrice AS HIDDEN price,
                                     Doctor.ratingTotal AS HIDDEN rating
                                     ")
                 ->leftJoin("Doctor.location", "Location")
                 ->leftJoin("Doctor.languages", "Language")
        ;

        if (!empty($filter['language'])) {
            $doctors = $doctors
                ->andWhere("Language.id IN (:language)")
                ->setParameter(":language", $filter['language']);
        }

        if (!empty($filter['clinic'])) {
            $doctors = $doctors
                ->andWhere("Doctor.currentClinic IN (:clinic)")
                ->setParameter(":clinic", $filter['clinic']);
        }

        if (!empty($filter['location'])) {
            $doctors = $doctors
                ->andWhere("Doctor.area = :location")
                ->setParameter(":location", $filter['location']);
        }

        if (!empty($filter['city'])) {
            $doctors = $doctors
                ->andWhere("Doctor.city = :city")
                ->setParameter(":city", $filter['city']);
        }

        if (!empty($filter['type'])) {
            $doctors = $doctors
                ->andWhere("Doctor.type = :type")
                ->setParameter(":type", $filter['type']);
        }

        if (!empty($filter['price'])) {

            if ($filter['price'][0] == 0) {
                $doctors = $doctors
                    ->andWhere("Doctor.childbirthPrice <= :price_max OR Doctor.childbirthPrice IS NULL")
                    ->setParameter(":price_max", $filter['price'][1])
                ;
            } else {
                $doctors = $doctors
                    ->andWhere("Doctor.childbirthPrice >= :price_min AND Doctor.childbirthPrice <= :price_max")
                    ->setParameter(":price_min", $filter['price'][0])
                    ->setParameter(":price_max", $filter['price'][1])
                ;
            }
        }

        if (!empty($filter['exp'])) {
            if ($filter['exp'][0] == 0) {
                $doctors = $doctors
                    ->andWhere("Doctor.experience <= :exp_max OR Doctor.experience IS NULL")
                    ->setParameter(":exp_max", $filter['exp'][1])
                ;
            } else {
                $doctors = $doctors
                    ->andWhere("Doctor.experience >= :exp_min AND Doctor.experience <= :exp_max")
                    ->setParameter(":exp_min", $filter['exp'][0])
                    ->setParameter(":exp_max", $filter['exp'][1])
                ;
            }
        }

        if (!empty($filter['locale']) && $filter['locale'] == 'en') {
            $doctors = $doctors
                ->andWhere("Doctor.showEnglish = 1");
        }

        $doctors = $doctors
            ->andWhere('Doctor.hide != 1')
            ->andWhere('Doctor.moderated = 1')
            ->groupBy('Doctor.id')
            ->orderBy('Doctor.position', 'ASC')
        ;

        $doctors = $doctors->getQuery();

        return $doctors;
    }




}
