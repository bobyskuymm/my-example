<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserPhone
 *
 * @ORM\Table(name="user_phones")
 * @ORM\Entity
 */
class UserPhone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="verify_code", type="string", length=255, nullable=true)
     */
    private $verifyCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="verified_at", type="datetime", nullable=true)
     */
    private $verifiedAt;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="User", inversedBy="phones")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return UserPhone
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set verifyCode
     *
     * @param string $verifyCode
     * @return UserPhone
     */
    public function setVerifyCode($verifyCode)
    {
        $this->verifyCode = $verifyCode;

        return $this;
    }

    /**
     * Get verifyCode
     *
     * @return string 
     */
    public function getVerifyCode()
    {
        return $this->verifyCode;
    }

    /**
     * Set verifiedAt
     *
     * @param \DateTime $verifiedAt
     * @return UserPhone
     */
    public function setVerifiedAt($verifiedAt)
    {
        $this->verifiedAt = $verifiedAt;

        return $this;
    }

    /**
     * Get verifiedAt
     *
     * @return \DateTime 
     */
    public function getVerifiedAt()
    {
        return $this->verifiedAt;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserPhone
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
