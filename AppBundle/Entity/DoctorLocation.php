<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * DoctorLocation
 *
 * @ORM\Table(name="doctor_locations")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="DoctorLocationTranslation")
 */
class DoctorLocation extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(length=128, unique=true)
     */
    private $url;

    /**
     * @ORM\OneToMany(targetEntity="Doctor", mappedBy="location")
     */
    protected $doctors;

    /**
     * @ORM\OneToMany(targetEntity="Clinic", mappedBy="location")
     */
    protected $clinics;

    /**
     * @ORM\OneToMany(
     *   targetEntity="DoctorLocationTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;


    public function __toString()
    {
        return $this->getName() ? $this->getName() : '';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->doctors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DoctorLocation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return DoctorLocation
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add doctors
     *
     * @param \AppBundle\Entity\Doctor $doctors
     * @return DoctorLocation
     */
    public function addDoctor(\AppBundle\Entity\Doctor $doctors)
    {
        $this->doctors[] = $doctors;

        return $this;
    }

    /**
     * Remove doctors
     *
     * @param \AppBundle\Entity\Doctor $doctors
     */
    public function removeDoctor(\AppBundle\Entity\Doctor $doctors)
    {
        $this->doctors->removeElement($doctors);
    }

    /**
     * Get doctors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoctors()
    {
        return $this->doctors;
    }

    /**
     * Add clinics
     *
     * @param \AppBundle\Entity\Clinic $clinics
     * @return DoctorLocation
     */
    public function addClinic(\AppBundle\Entity\Clinic $clinics)
    {
        $this->clinics[] = $clinics;

        return $this;
    }

    /**
     * Remove clinics
     *
     * @param \AppBundle\Entity\Clinic $clinics
     */
    public function removeClinic(\AppBundle\Entity\Clinic $clinics)
    {
        $this->clinics->removeElement($clinics);
    }

    /**
     * Get clinics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClinics()
    {
        return $this->clinics;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\DoctorLocationTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\DoctorLocationTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }
}
