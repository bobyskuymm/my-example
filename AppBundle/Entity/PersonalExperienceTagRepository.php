<?php

namespace AppBundle\Entity;

/**
 * PersonalExperienceTagRepository
 *
 */
class PersonalExperienceTagRepository extends \Doctrine\ORM\EntityRepository
{

    public function getSimilarByName($name)
    {

        $tags =
            $this->createQueryBuilder("PersonalExperienceTag")
                 ->select("PersonalExperienceTag")
                 ->andWhere("PersonalExperienceTag.name LIKE :name")
                 ->setParameter(":name", '%' . $name . '%')
        ;


        $tags = $tags->getQuery();

        return $tags->getResult();
    }




}
