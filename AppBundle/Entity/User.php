<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;
use Symfony\Component\HttpFoundation\Request;

/**
 * @ORM\Entity
 * @FileStore\Uploadable
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="accounts")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\TranslationEntity(class="UserTranslation")
 */
class User extends BaseUser implements TranslatableInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     *
     * @var string
     */
    protected $locale;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="nickname", type="string", length=512, nullable=true)
     */
    private $nickname;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="first_name", type="string", length=512, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="last_name", type="string", length=512, nullable=true)
     */
    private $lastName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide", type="boolean", nullable=true)
     */
    private $hide;

    /**
     * @var \DateTime
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;


    /**
     * @var string
     *
     * @ORM\Column(name="social_vk", type="string", length=512, nullable=true)
     * @Assert\Url()
     */
    private $socialVk;

    /**
     * @var string
     *
     * @ORM\Column(name="social_fb", type="string", length=512, nullable=true)
     * @Assert\Url()
     */
    private $socialFb;

    /**
     * @var string
     *
     * @ORM\Column(name="social_gp", type="string", length=512, nullable=true)
     * @Assert\Url()
     */
    private $socialGp;

    /**
     * @var string
     *
     * @ORM\Column(name="social_ok", type="string", length=512, nullable=true)
     * @Assert\Url()
     */
    private $socialOk;

    /**
     * @var string
     *
     * @ORM\Column(name="social_in", type="string", length=512, nullable=true)
     * @Assert\Url()
     */
    private $socialIn;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="about", type="text", nullable=true)
     */
    private $about;

    /**
     * @var string
     *
     * @ORM\Column(name="skype", type="string", length=512, nullable=true)
     */
    private $skype;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="DoctorLocation")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=true)
     */
    protected $location;

    /**
     * @ORM\OneToMany(targetEntity="UserPhone", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $phones;

    /**
     * @var array
     *
     * @FileStore\UploadableField(mapping="avatar")
     *
     * @ORM\Column(name="avatar", type="array", nullable=true)
     */
    protected $avatar;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="AccountType", inversedBy="users")
     * @ORM\JoinColumn(name="account_type_id", referencedColumnName="id")
     */
    protected $accountType;

    /**
     * @ORM\ManyToMany(targetEntity="Doctor", mappedBy="favoriteBy")
     * @ORM\JoinTable(name="favorite_doctors_index",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="doctor_id", referencedColumnName="id")}
     * )
     */
    private $favoriteDoctors;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="favoriteBy")
     * @ORM\JoinTable(name="favorite_users_index",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="fav_id", referencedColumnName="id")}
     * )
     */
    private $favoriteUsers;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="favoriteUsers", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="favorite_users_index",
     *      joinColumns={@ORM\JoinColumn(name="fav_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $favoriteBy;

    /**
     * @ORM\ManyToMany(targetEntity="Clinic", mappedBy="favoriteBy")
     * @ORM\JoinTable(name="favorite_clinic_index",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="clinic_id", referencedColumnName="id")}
     * )
     */
    private $favoriteClinics;

    /**
     * @ORM\ManyToMany(targetEntity="Realty", mappedBy="favoriteBy")
     * @ORM\JoinTable(name="favorite_realty_index",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="realty_id", referencedColumnName="id")}
     * )
     */
    private $favoriteRealty;

    /**
     * @ORM\OneToOne(targetEntity="Doctor", mappedBy="user")
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $doctor;

    /**
     * @ORM\OneToOne(targetEntity="Clinic", mappedBy="user")
     * @ORM\JoinColumn(name="clinic_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $clinic;

    /**
     * @ORM\ManyToMany(targetEntity="DoctorComment", mappedBy="likes", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="doctor_comment_likes_index",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="comment_id", referencedColumnName="id")}
     * )
     */
    private $likedDoctorComments;

    /**
     * @ORM\ManyToMany(targetEntity="ClinicComment", mappedBy="likes", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="clinic_comment_likes_index",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="comment_id", referencedColumnName="id")}
     * )
     */
    private $likedClinicComments;

    /**
     * @ORM\ManyToMany(targetEntity="RealtyComment", mappedBy="likes", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="realty_comment_likes_index",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="comment_id", referencedColumnName="id")}
     * )
     */
    private $likedRealtyComments;

    /**
     * @ORM\ManyToMany(targetEntity="Topic", mappedBy="likes", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="topic_rating_negative_index",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="topic_id", referencedColumnName="id")}
     * )
     */
    private $likedTopics;

    /**
     * @ORM\OneToMany(targetEntity="ExcursionApplication", mappedBy="user")
     */
    protected $excursionApplications;

    /**
     * @ORM\OneToMany(targetEntity="AppointmentApplication", mappedBy="user")
     */
    protected $appointmentApplications;

    /**
     * @ORM\OneToMany(targetEntity="LeaseApplication", mappedBy="user")
     */
    protected $leaseApplications;

    /**
     * @ORM\OneToMany(targetEntity="Realty", mappedBy="user")
     */
    protected $realty;

    /**
     * @var datetime
     *
     * @ORM\Column(name="registered_at", type="datetime")
     */
    private $registeredAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $ratingTotal;

    /**
     * @ORM\ManyToMany(targetEntity="User", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="user_rating_positive_index",
     *      joinColumns={@ORM\JoinColumn(name="rat_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $positiveRating;

    /**
     * @ORM\ManyToMany(targetEntity="User", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="user_rating_negative_index",
     *      joinColumns={@ORM\JoinColumn(name="rat_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $negativeRating;


    /**
     * @var string
     *
     * @ORM\Column(name="facebook_id", type="string", unique=true, nullable=true)
     */
    private $facebookId;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_access_token", type="string", nullable=true)
     */
    private $facebookAccessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_id", type="string", unique=true, nullable=true)
     */
    private $twitterId;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_access_token", type="string", nullable=true)
     */
    private $twitterAccessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="vkontakte_id", type="string", unique=true, nullable=true)
     */
    private $vkontakteId;

    /**
     * @var string
     *
     * @ORM\Column(name="vkontakte_access_token", type="string", nullable=true)
     */
    private $vkontakteAccessToken;

    /**
     * @var integer
     *
     * @ORM\Column(name="update_trigger", type="integer")
     */
    private $updateTrigger;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\OneToMany(
     *   targetEntity="UserTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    public function __construct()
    {
        parent::__construct();
        $this->setUpdateTrigger(0);
        $this->setRegisteredAt(new \DateTime());
    }

    public function __toString()
    {
        return $this->getFullname();
    }

    private function checkLocale()
    {
        $locale = 'ru';

        $uri = explode('/',$_SERVER['REQUEST_URI']);

        if (!empty($uri[1]) && !empty($uri[2]) && $uri[1] == 'app_dev.php' && $uri[2] == 'en') {
            $locale = 'en';
        }

        if (!empty($uri[1]) && $uri[1] != 'app_dev.php' && $uri[1] == 'en') {
            $locale = 'en';
        }

        return $locale;
    }

    public function getFullname()
    {

        if (($this->getFirstName() || $this->getLastName()) && !$this->getHide()) {
            return $this->getFirstName() . ' ' . $this->getLastName();
        }

        if ($this->getNickname()) {
            return $this->getNickname();
        }

        return $this->getUsername();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {

        if (!$this->getLocale()) {
            $locale = $this->checkLocale();
            $data = $this->getTranslation('firstName', $locale);
            if ($data) {
                return $data;
            }
        }

        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        if (!$this->getLocale()) {
            $locale = $this->checkLocale();
            $data = $this->getTranslation('lastName', $locale);
            if ($data) {
                return $data;
            }
        }

        return $this->lastName;
    }

    /**
     * Set avatar
     *
     * @param array $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return array
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set accountType
     *
     * @param \AppBundle\Entity\AccountType $accountType
     * @return User
     */
    public function setAccountType(\AppBundle\Entity\AccountType $accountType = null)
    {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * Get accountType
     *
     * @return \AppBundle\Entity\AccountType
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * Add favoriteDoctors
     *
     * @param \AppBundle\Entity\Doctor $favoriteDoctors
     * @return User
     */
    public function addFavoriteDoctor(\AppBundle\Entity\Doctor $favoriteDoctors)
    {
        $this->favoriteDoctors[] = $favoriteDoctors;

        return $this;
    }

    /**
     * Remove favoriteDoctors
     *
     * @param \AppBundle\Entity\Doctor $favoriteDoctors
     */
    public function removeFavoriteDoctor(\AppBundle\Entity\Doctor $favoriteDoctors)
    {
        $this->favoriteDoctors->removeElement($favoriteDoctors);
    }

    /**
     * Get favoriteDoctors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteDoctors()
    {
        return $this->favoriteDoctors;
    }

    /**
     * Set twitterId
     *
     * @param string $twitterId
     * @return User
     */
    public function setTwitterId($twitterId)
    {
        $this->twitterId = $twitterId;

        return $this;
    }

    /**
     * Get twitterId
     *
     * @return string
     */
    public function getTwitterId()
    {
        return $this->twitterId;
    }

    /**
     * Set twitterAccessToken
     *
     * @param string $twitterAccessToken
     * @return User
     */
    public function setTwitterAccessToken($twitterAccessToken)
    {
        $this->twitterAccessToken = $twitterAccessToken;

        return $this;
    }

    /**
     * Get twitterAccessToken
     *
     * @return string
     */
    public function getTwitterAccessToken()
    {
        return $this->twitterAccessToken;
    }

    /**
     * Set vkontakteId
     *
     * @param string $vkontakteId
     * @return User
     */
    public function setVkontakteId($vkontakteId)
    {
        $this->vkontakteId = $vkontakteId;

        return $this;
    }

    /**
     * Get vkontakteId
     *
     * @return string
     */
    public function getVkontakteId()
    {
        return $this->vkontakteId;
    }

    /**
     * Set vkontakteAccessToken
     *
     * @param string $vkontakteAccessToken
     * @return User
     */
    public function setVkontakteAccessToken($vkontakteAccessToken)
    {
        $this->vkontakteAccessToken = $vkontakteAccessToken;

        return $this;
    }

    /**
     * Get vkontakteAccessToken
     *
     * @return string
     */
    public function getVkontakteAccessToken()
    {
        return $this->vkontakteAccessToken;
    }

    /**
     * Set doctor
     *
     * @param \AppBundle\Entity\Doctor $doctor
     * @return User
     */
    public function setDoctor(\AppBundle\Entity\Doctor $doctor = null)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor
     *
     * @return \AppBundle\Entity\Doctor
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Add favoriteClinics
     *
     * @param \AppBundle\Entity\Clinic $favoriteClinics
     * @return User
     */
    public function addFavoriteClinic(\AppBundle\Entity\Clinic $favoriteClinics)
    {
        $this->favoriteClinics[] = $favoriteClinics;

        return $this;
    }

    /**
     * Remove favoriteClinics
     *
     * @param \AppBundle\Entity\Clinic $favoriteClinics
     */
    public function removeFavoriteClinic(\AppBundle\Entity\Clinic $favoriteClinics)
    {
        $this->favoriteClinics->removeElement($favoriteClinics);
    }

    /**
     * Get favoriteClinics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteClinics()
    {
        return $this->favoriteClinics;
    }

    /**
     * Set clinic
     *
     * @param \AppBundle\Entity\Clinic $clinic
     * @return User
     */
    public function setClinic(\AppBundle\Entity\Clinic $clinic = null)
    {
        $this->clinic = $clinic;

        return $this;
    }

    /**
     * Get clinic
     *
     * @return \AppBundle\Entity\Clinic
     */
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * Add likedDoctorComments
     *
     * @param \AppBundle\Entity\DoctorComment $likedDoctorComments
     * @return User
     */
    public function addLikedDoctorComment(\AppBundle\Entity\DoctorComment $likedDoctorComments)
    {
        $this->likedDoctorComments[] = $likedDoctorComments;

        return $this;
    }

    /**
     * Remove likedDoctorComments
     *
     * @param \AppBundle\Entity\DoctorComment $likedDoctorComments
     */
    public function removeLikedDoctorComment(\AppBundle\Entity\DoctorComment $likedDoctorComments)
    {
        $this->likedDoctorComments->removeElement($likedDoctorComments);
    }

    /**
     * Get likedDoctorComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikedDoctorComments()
    {
        return $this->likedDoctorComments;
    }

    /**
     * Add likedClinicComments
     *
     * @param \AppBundle\Entity\ClinicComment $likedClinicComments
     * @return User
     */
    public function addLikedClinicComment(\AppBundle\Entity\ClinicComment $likedClinicComments)
    {
        $this->likedClinicComments[] = $likedClinicComments;

        return $this;
    }

    /**
     * Remove likedClinicComments
     *
     * @param \AppBundle\Entity\ClinicComment $likedClinicComments
     */
    public function removeLikedClinicComment(\AppBundle\Entity\ClinicComment $likedClinicComments)
    {
        $this->likedClinicComments->removeElement($likedClinicComments);
    }

    /**
     * Get likedClinicComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikedClinicComments()
    {
        return $this->likedClinicComments;
    }

    /**
     * Add likedTopics
     *
     * @param \AppBundle\Entity\Topic $likedTopics
     * @return User
     */
    public function addLikedTopic(\AppBundle\Entity\Topic $likedTopics)
    {
        $this->likedTopics[] = $likedTopics;

        return $this;
    }

    /**
     * Remove likedTopics
     *
     * @param \AppBundle\Entity\Topic $likedTopics
     */
    public function removeLikedTopic(\AppBundle\Entity\Topic $likedTopics)
    {
        $this->likedTopics->removeElement($likedTopics);
    }

    /**
     * Get likedTopics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikedTopics()
    {
        return $this->likedTopics;
    }

    /**
     * Add excursionApplications
     *
     * @param \AppBundle\Entity\ExcursionApplication $excursionApplications
     * @return User
     */
    public function addExcursionApplication(\AppBundle\Entity\ExcursionApplication $excursionApplications)
    {
        $this->excursionApplications[] = $excursionApplications;

        return $this;
    }

    /**
     * Remove excursionApplications
     *
     * @param \AppBundle\Entity\ExcursionApplication $excursionApplications
     */
    public function removeExcursionApplication(\AppBundle\Entity\ExcursionApplication $excursionApplications)
    {
        $this->excursionApplications->removeElement($excursionApplications);
    }

    /**
     * Get excursionApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExcursionApplications()
    {
        return $this->excursionApplications;
    }

    /**
     * Add appointmentApplications
     *
     * @param \AppBundle\Entity\AppointmentApplication $appointmentApplications
     * @return User
     */
    public function addAppointmentApplication(\AppBundle\Entity\AppointmentApplication $appointmentApplications)
    {
        $this->appointmentApplications[] = $appointmentApplications;

        return $this;
    }

    /**
     * Remove appointmentApplications
     *
     * @param \AppBundle\Entity\AppointmentApplication $appointmentApplications
     */
    public function removeAppointmentApplication(\AppBundle\Entity\AppointmentApplication $appointmentApplications)
    {
        $this->appointmentApplications->removeElement($appointmentApplications);
    }

    /**
     * Get appointmentApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppointmentApplications()
    {
        return $this->appointmentApplications;
    }

    /**
     * Add likedRealtyComments
     *
     * @param \AppBundle\Entity\RealtyComment $likedRealtyComments
     * @return User
     */
    public function addLikedRealtyComment(\AppBundle\Entity\RealtyComment $likedRealtyComments)
    {
        $this->likedRealtyComments[] = $likedRealtyComments;

        return $this;
    }

    /**
     * Remove likedRealtyComments
     *
     * @param \AppBundle\Entity\RealtyComment $likedRealtyComments
     */
    public function removeLikedRealtyComment(\AppBundle\Entity\RealtyComment $likedRealtyComments)
    {
        $this->likedRealtyComments->removeElement($likedRealtyComments);
    }

    /**
     * Get likedRealtyComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikedRealtyComments()
    {
        return $this->likedRealtyComments;
    }

    /**
     * Add leaseApplications
     *
     * @param \AppBundle\Entity\LeaseApplication $leaseApplications
     * @return User
     */
    public function addLeaseApplication(\AppBundle\Entity\LeaseApplication $leaseApplications)
    {
        $this->leaseApplications[] = $leaseApplications;

        return $this;
    }

    /**
     * Remove leaseApplications
     *
     * @param \AppBundle\Entity\LeaseApplication $leaseApplications
     */
    public function removeLeaseApplication(\AppBundle\Entity\LeaseApplication $leaseApplications)
    {
        $this->leaseApplications->removeElement($leaseApplications);
    }

    /**
     * Get leaseApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLeaseApplications()
    {
        return $this->leaseApplications;
    }

    /**
     * Add realty
     *
     * @param \AppBundle\Entity\Realty $realty
     * @return User
     */
    public function addRealty(\AppBundle\Entity\Realty $realty)
    {
        $this->realty[] = $realty;

        return $this;
    }

    /**
     * Remove realty
     *
     * @param \AppBundle\Entity\Realty $realty
     */
    public function removeRealty(\AppBundle\Entity\Realty $realty)
    {
        $this->realty->removeElement($realty);
    }

    /**
     * Get realty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRealty()
    {
        return $this->realty;
    }

    /**
     * Add favoriteRealty
     *
     * @param \AppBundle\Entity\Realty $favoriteRealty
     * @return User
     */
    public function addFavoriteRealty(\AppBundle\Entity\Realty $favoriteRealty)
    {
        $this->favoriteRealty[] = $favoriteRealty;

        return $this;
    }

    /**
     * Remove favoriteRealty
     *
     * @param \AppBundle\Entity\Realty $favoriteRealty
     */
    public function removeFavoriteRealty(\AppBundle\Entity\Realty $favoriteRealty)
    {
        $this->favoriteRealty->removeElement($favoriteRealty);
    }

    /**
     * Get favoriteRealty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteRealty()
    {
        return $this->favoriteRealty;
    }

    /**
     * Set hide
     *
     * @param boolean $hide
     * @return User
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return boolean
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set socialVk
     *
     * @param string $socialVk
     * @return User
     */
    public function setSocialVk($socialVk)
    {
        $this->socialVk = $socialVk;

        return $this;
    }

    /**
     * Get socialVk
     *
     * @return string
     */
    public function getSocialVk()
    {
        return $this->socialVk;
    }

    /**
     * Set socialFb
     *
     * @param string $socialFb
     * @return User
     */
    public function setSocialFb($socialFb)
    {
        $this->socialFb = $socialFb;

        return $this;
    }

    /**
     * Get socialFb
     *
     * @return string
     */
    public function getSocialFb()
    {
        return $this->socialFb;
    }

    /**
     * Set socialGp
     *
     * @param string $socialGp
     * @return User
     */
    public function setSocialGp($socialGp)
    {
        $this->socialGp = $socialGp;

        return $this;
    }

    /**
     * Get socialGp
     *
     * @return string
     */
    public function getSocialGp()
    {
        return $this->socialGp;
    }

    /**
     * Set socialOk
     *
     * @param string $socialOk
     * @return User
     */
    public function setSocialOk($socialOk)
    {
        $this->socialOk = $socialOk;

        return $this;
    }

    /**
     * Get socialOk
     *
     * @return string
     */
    public function getSocialOk()
    {
        return $this->socialOk;
    }

    /**
     * Set socialIn
     *
     * @param string $socialIn
     * @return User
     */
    public function setSocialIn($socialIn)
    {
        $this->socialIn = $socialIn;

        return $this;
    }

    /**
     * Get socialIn
     *
     * @return string
     */
    public function getSocialIn()
    {
        return $this->socialIn;
    }

    /**
     * Set about
     *
     * @param string $about
     * @return User
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        if (!$this->getLocale()) {
            $locale = $this->checkLocale();
            $data = $this->getTranslation('about', $locale);
            if ($data) {
                return $data;
            }
        }

        return $this->about;
    }

    /**
     * Set skype
     *
     * @param string $skype
     * @return User
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\DoctorLocation $location
     * @return User
     */
    public function setLocation(\AppBundle\Entity\DoctorLocation $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\DoctorLocation
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Add phones
     *
     * @param \AppBundle\Entity\UserPhone $phones
     * @return User
     */
    public function addPhone(\AppBundle\Entity\UserPhone $phones)
    {
        $this->phones[] = $phones;

        return $this;
    }

    /**
     * Remove phones
     *
     * @param \AppBundle\Entity\UserPhone $phones
     */
    public function removePhone(\AppBundle\Entity\UserPhone $phones)
    {
        $this->phones->removeElement($phones);
    }

    /**
     * Get phones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     * @return User
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string
     */
    public function getNickname()
    {
        if (!$this->getLocale()) {
            $locale = $this->checkLocale();
            $data = $this->getTranslation('firstName', $locale);
            if ($data) {
                return $data;
            }
        }

        return $this->nickname;
    }

    /**
     * Set registeredAt
     *
     * @param \DateTime $registeredAt
     * @return User
     */
    public function setRegisteredAt($registeredAt)
    {
        $this->registeredAt = $registeredAt;

        return $this;
    }

    /**
     * Get registeredAt
     *
     * @return \DateTime
     */
    public function getRegisteredAt()
    {
        return $this->registeredAt;
    }


    public function getTimeOnSite()
    {

        $d1 = $this->getRegisteredAt();
        $d2 = new \DateTime();

        $i  = new \DateInterval('P1D');
        $d2->add($i);

        $iv = $d2->diff($d1);

        return $iv;
    }



    /**
     * Add favoriteUsers
     *
     * @param \AppBundle\Entity\User $favoriteUsers
     * @return User
     */
    public function addFavoriteUser(\AppBundle\Entity\User $favoriteUsers)
    {
        $this->favoriteUsers[] = $favoriteUsers;

        return $this;
    }

    /**
     * Remove favoriteUsers
     *
     * @param \AppBundle\Entity\User $favoriteUsers
     */
    public function removeFavoriteUser(\AppBundle\Entity\User $favoriteUsers)
    {
        $this->favoriteUsers->removeElement($favoriteUsers);
    }

    /**
     * Get favoriteUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteUsers()
    {
        return $this->favoriteUsers;
    }

    /**
     * Add favoriteBy
     *
     * @param \AppBundle\Entity\User $favoriteBy
     * @return User
     */
    public function addFavoriteBy(\AppBundle\Entity\User $favoriteBy)
    {
        $this->favoriteBy[] = $favoriteBy;

        return $this;
    }

    /**
     * Remove favoriteBy
     *
     * @param \AppBundle\Entity\User $favoriteBy
     */
    public function removeFavoriteBy(\AppBundle\Entity\User $favoriteBy)
    {
        $this->favoriteBy->removeElement($favoriteBy);
    }

    /**
     * Get favoriteBy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteBy()
    {
        return $this->favoriteBy;
    }

    /**
     * Set ratingTotal
     *
     * @param integer $ratingTotal
     * @return User
     */
    public function setRatingTotal($ratingTotal)
    {
        $this->ratingTotal = $ratingTotal;

        return $this;
    }

    /**
     * Get ratingTotal
     *
     * @return integer
     */
    public function getRatingTotal()
    {
        return ($this->ratingTotal) ? $this->ratingTotal : 0;
    }

    /**
     * Add positiveRating
     *
     * @param \AppBundle\Entity\User $positiveRating
     * @return User
     */
    public function addPositiveRating(\AppBundle\Entity\User $positiveRating)
    {
        $this->positiveRating[] = $positiveRating;

        return $this;
    }

    /**
     * Remove positiveRating
     *
     * @param \AppBundle\Entity\User $positiveRating
     */
    public function removePositiveRating(\AppBundle\Entity\User $positiveRating)
    {
        $this->positiveRating->removeElement($positiveRating);
    }

    /**
     * Get positiveRating
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPositiveRating()
    {
        return $this->positiveRating;
    }

    /**
     * Add negativeRating
     *
     * @param \AppBundle\Entity\User $negativeRating
     * @return User
     */
    public function addNegativeRating(\AppBundle\Entity\User $negativeRating)
    {
        $this->negativeRating[] = $negativeRating;

        return $this;
    }

    /**
     * Remove negativeRating
     *
     * @param \AppBundle\Entity\User $negativeRating
     */
    public function removeNegativeRating(\AppBundle\Entity\User $negativeRating)
    {
        $this->negativeRating->removeElement($negativeRating);
    }

    /**
     * Get negativeRating
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNegativeRating()
    {
        return $this->negativeRating;
    }

    public function updateRatingCount()
    {
        $positive = count($this->positiveRating);
        $negative = count($this->negativeRating);

        $total    = $positive - $negative;

        $this->setRatingTotal($total);
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     */
    public function prePersist()
    {
        $this->updateRatingCount();
    }


    /**
     * Set updateTrigger
     *
     * @param integer $updateTrigger
     * @return User
     */
    public function setUpdateTrigger($updateTrigger)
    {
        $this->updateTrigger = $updateTrigger;

        return $this;
    }

    /**
     * Get updateTrigger
     *
     * @return integer
     */
    public function getUpdateTrigger()
    {
        return $this->updateTrigger;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return User
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return ArrayCollection|AbstractPersonalTranslation[]
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param string $field
     * @param string $locale
     *
     * @return null|string
     */
    public function getTranslation($field, $locale)
    {
        foreach ($this->getTranslations() as $translation) {
            if (strcmp($translation->getField(), $field) === 0 && strcmp($translation->getLocale(), $locale) === 0) {
                return $translation->getContent();
            }
        }

        return;
    }

    /**
     * @param AbstractPersonalTranslation $translation
     *
     * @return $this
     */
    public function addTranslation(AbstractPersonalTranslation $translation)
    {
        if (!$this->translations->contains($translation)) {
            $translation->setObject($this);
            $this->translations->add($translation);
        }

        return $this;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\UserTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\UserTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }
}
