<?php

namespace AppBundle\Entity;

/**
 * TopicTagRepository
 *
 */
class TopicTagRepository extends \Doctrine\ORM\EntityRepository
{

    public function getSimilarByName($name)
    {

        $tags =
            $this->createQueryBuilder("TopicTag")
                 ->select("TopicTag")
                 ->andWhere("TopicTag.name LIKE :name")
                 ->setParameter(":name", '%' . $name . '%')
        ;


        $tags = $tags->getQuery();

        return $tags->getResult();
    }




}
