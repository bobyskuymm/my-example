<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Clinic
 *
 * @ORM\Table(name="clinics")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ClinicRepository")
 * @Gedmo\TranslationEntity(class="ClinicTranslation")
 */
class Clinic extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_english", type="boolean", nullable=true)
     */
    private $showEnglish;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=512, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Doctor", mappedBy="currentClinic")
     */
    protected $personnel;

    /**
     * @ORM\OneToMany(targetEntity="ExcursionApplication", mappedBy="clinic")
     */
    protected $excursionApplications;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="DoctorLocation", inversedBy="clinics")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     */
    protected $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="foundation_year", type="integer", nullable=true)
     */
    private $foundationYear;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="about", type="text", nullable=true)
     */
    private $about;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="short_about", type="text", nullable=true)
     */
    private $shortAbout;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="full_about", type="text", nullable=true)
     */
    private $fullAbout;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="special_offer", type="string", length=1024, nullable=true)
     */
    private $specialOffer;

    /**
     * @var integer
     *
     * @ORM\Column(name="rating_total", type="float", nullable=true)
     */
    private $ratingTotal;

    /**
     * @var array
     *
     * @ORM\Column(name="rating_all", type="array", nullable=true)
     */
    private $ratingAll;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="address", type="string", length=512, nullable=true)
     */
    private $address;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide", type="boolean", nullable=true)
     */
    private $hide;

    /**
     * @var boolean
     *
     * @ORM\Column(name="moderated", type="boolean", nullable=true)
     */
    private $moderated;

    /**
     * @var string
     *
     * @ORM\Column(name="moderator_comment", type="string", length=1024, nullable=true)
     */
    private $moderatorComment;

    /**
     * @ORM\OneToMany(targetEntity="ClinicService", mappedBy="clinic", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $services;

    /**
     * @ORM\OneToMany(targetEntity="ClinicDocument", mappedBy="clinic", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $documents;

    /**
     * @ORM\OneToMany(targetEntity="ClinicPhoto", mappedBy="clinic", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $photos;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="favoriteClinics", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="favorite_clinic_index",
     *      joinColumns={@ORM\JoinColumn(name="clinic_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $favoriteBy;

    /**
     * @ORM\OneToOne(targetEntity="ClinicThread", mappedBy="clinic")
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $thread;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="clinic")
     */
    protected $user;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $url;

    /**
     * @ORM\ManyToMany(targetEntity="LifehackArticle", mappedBy="linkedClinics")
     * @ORM\JoinTable(name="lifehack_article_clinic_index",
     *      joinColumns={@ORM\JoinColumn(name="clinic_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="lifehack_article_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $linkedLifehack;

    /**
     * @ORM\ManyToMany(targetEntity="NewsArticle", mappedBy="linkedClinics")
     * @ORM\JoinTable(name="news_article_clinic_index",
     *      joinColumns={@ORM\JoinColumn(name="clinic_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="news_article_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $linkedNews;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="City", inversedBy="clinics")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $city;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Area", inversedBy="area")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $area;


    /**
     * @ORM\OneToMany(
     *   targetEntity="ClinicTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;


    public function __toString()
    {
        if ($this->getName()) {
            return $this->getName();
        }
        return "";
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->personnel = new \Doctrine\Common\Collections\ArrayCollection();
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->favoriteBy = new \Doctrine\Common\Collections\ArrayCollection();

        $this->setModerated(false);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Clinic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Clinic
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set foundationYear
     *
     * @param integer $foundationYear
     * @return Clinic
     */
    public function setFoundationYear($foundationYear)
    {
        $this->foundationYear = $foundationYear;

        return $this;
    }

    /**
     * Get foundationYear
     *
     * @return integer
     */
    public function getFoundationYear()
    {
        return $this->foundationYear;
    }

    /**
     * Set about
     *
     * @param string $about
     * @return Clinic
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set shortAbout
     *
     * @param string $shortAbout
     * @return Clinic
     */
    public function setShortAbout($shortAbout)
    {
        $this->shortAbout = $shortAbout;

        return $this;
    }

    /**
     * Get shortAbout
     *
     * @return string
     */
    public function getShortAbout()
    {
        return $this->shortAbout;
    }

    /**
     * Set fullAbout
     *
     * @param string $fullAbout
     * @return Clinic
     */
    public function setFullAbout($fullAbout)
    {
        $this->fullAbout = $fullAbout;

        return $this;
    }

    /**
     * Get fullAbout
     *
     * @return string
     */
    public function getFullAbout()
    {
        return $this->fullAbout;
    }

    /**
     * Set specialOffer
     *
     * @param string $specialOffer
     * @return Clinic
     */
    public function setSpecialOffer($specialOffer)
    {
        $this->specialOffer = $specialOffer;

        return $this;
    }

    /**
     * Get specialOffer
     *
     * @return string
     */
    public function getSpecialOffer()
    {
        return $this->specialOffer;
    }

    /**
     * Set ratingTotal
     *
     * @param float $ratingTotal
     * @return Clinic
     */
    public function setRatingTotal($ratingTotal)
    {
        $this->ratingTotal = $ratingTotal;

        return $this;
    }

    /**
     * Get ratingTotal
     *
     * @return float
     */
    public function getRatingTotal()
    {
        return $this->ratingTotal;
    }

    /**
     * Set ratingAll
     *
     * @param array $ratingAll
     * @return Clinic
     */
    public function setRatingAll($ratingAll)
    {
        $this->ratingAll = $ratingAll;

        return $this;
    }

    /**
     * Get ratingAll
     *
     * @return array
     */
    public function getRatingAll()
    {
        return $this->ratingAll;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Clinic
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set hide
     *
     * @param boolean $hide
     * @return Clinic
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return boolean
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Clinic
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Clinic
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Clinic
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add personnel
     *
     * @param \AppBundle\Entity\Doctor $personnel
     * @return Clinic
     */
    public function addPersonnel(\AppBundle\Entity\Doctor $personnel)
    {
        $this->personnel[] = $personnel;

        return $this;
    }

    /**
     * Remove personnel
     *
     * @param \AppBundle\Entity\Doctor $personnel
     */
    public function removePersonnel(\AppBundle\Entity\Doctor $personnel)
    {
        $this->personnel->removeElement($personnel);
    }

    /**
     * Get personnel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonnel()
    {
        return $this->personnel;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\DoctorLocation $location
     * @return Clinic
     */
    public function setLocation(\AppBundle\Entity\DoctorLocation $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\DoctorLocation
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Add services
     *
     * @param \AppBundle\Entity\ClinicService $services
     * @return Clinic
     */
    public function addService(\AppBundle\Entity\ClinicService $services)
    {
        $this->services[] = $services;

        return $this;
    }

    /**
     * Remove services
     *
     * @param \AppBundle\Entity\ClinicService $services
     */
    public function removeService(\AppBundle\Entity\ClinicService $services)
    {
        $this->services->removeElement($services);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Add documents
     *
     * @param \AppBundle\Entity\ClinicDocument $documents
     * @return Clinic
     */
    public function addDocument(\AppBundle\Entity\ClinicDocument $documents)
    {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param \AppBundle\Entity\ClinicDocument $documents
     */
    public function removeDocument(\AppBundle\Entity\ClinicDocument $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Add photos
     *
     * @param \AppBundle\Entity\ClinicPhoto $photos
     * @return Clinic
     */
    public function addPhoto(\AppBundle\Entity\ClinicPhoto $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \AppBundle\Entity\ClinicPhoto $photos
     */
    public function removePhoto(\AppBundle\Entity\ClinicPhoto $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Add favoriteBy
     *
     * @param \AppBundle\Entity\User $favoriteBy
     * @return Clinic
     */
    public function addFavoriteBy(\AppBundle\Entity\User $favoriteBy)
    {
        $this->favoriteBy[] = $favoriteBy;

        return $this;
    }

    /**
     * Remove favoriteBy
     *
     * @param \AppBundle\Entity\User $favoriteBy
     */
    public function removeFavoriteBy(\AppBundle\Entity\User $favoriteBy)
    {
        $this->favoriteBy->removeElement($favoriteBy);
    }

    /**
     * Get favoriteBy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteBy()
    {
        return $this->favoriteBy;
    }

    /**
     * Set thread
     *
     * @param \AppBundle\Entity\ClinicThread $thread
     * @return Clinic
     */
    public function setThread(\AppBundle\Entity\ClinicThread $thread = null)
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * Get thread
     *
     * @return \AppBundle\Entity\ClinicThread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Clinic
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return Clinic
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     * @return Clinic
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function getTotalComments()
    {
        if (!$this->getThread()) {
            return 0;
        }

        return ($this->getThread()->getCommentCount()) ? $this->getThread()->getCommentCount() : 0;
    }


    /**
     * Add excursionApplications
     *
     * @param \AppBundle\Entity\ExcursionApplication $excursionApplications
     * @return Clinic
     */
    public function addExcursionApplication(\AppBundle\Entity\ExcursionApplication $excursionApplications)
    {
        $this->excursionApplications[] = $excursionApplications;

        return $this;
    }

    /**
     * Remove excursionApplications
     *
     * @param \AppBundle\Entity\ExcursionApplication $excursionApplications
     */
    public function removeExcursionApplication(\AppBundle\Entity\ExcursionApplication $excursionApplications)
    {
        $this->excursionApplications->removeElement($excursionApplications);
    }

    /**
     * Get excursionApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExcursionApplications()
    {
        return $this->excursionApplications;
    }

    /**
     * Set moderated
     *
     * @param boolean $moderated
     * @return Clinic
     */
    public function setModerated($moderated)
    {
        $this->moderated = $moderated;

        return $this;
    }

    /**
     * Get moderated
     *
     * @return boolean
     */
    public function getModerated()
    {
        return $this->moderated;
    }

    /**
     * Set moderatorComment
     *
     * @param string $moderatorComment
     * @return Clinic
     */
    public function setModeratorComment($moderatorComment)
    {
        $this->moderatorComment = $moderatorComment;

        return $this;
    }

    /**
     * Get moderatorComment
     *
     * @return string
     */
    public function getModeratorComment()
    {
        return $this->moderatorComment;
    }

    /**
     * Add linkedLifehack
     *
     * @param \AppBundle\Entity\LifehackArticle $linkedLifehack
     * @return Clinic
     */
    public function addLinkedLifehack(\AppBundle\Entity\LifehackArticle $linkedLifehack)
    {
        $this->linkedLifehack[] = $linkedLifehack;

        return $this;
    }

    /**
     * Remove linkedLifehack
     *
     * @param \AppBundle\Entity\LifehackArticle $linkedLifehack
     */
    public function removeLinkedLifehack(\AppBundle\Entity\LifehackArticle $linkedLifehack)
    {
        $this->linkedLifehack->removeElement($linkedLifehack);
    }

    /**
     * Get linkedLifehack
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinkedLifehack()
    {
        return $this->linkedLifehack;
    }

    /**
     * Add linkedNews
     *
     * @param \AppBundle\Entity\NewsArticle $linkedNews
     * @return Clinic
     */
    public function addLinkedNews(\AppBundle\Entity\NewsArticle $linkedNews)
    {
        $this->linkedNews[] = $linkedNews;

        return $this;
    }

    /**
     * Remove linkedNews
     *
     * @param \AppBundle\Entity\NewsArticle $linkedNews
     */
    public function removeLinkedNews(\AppBundle\Entity\NewsArticle $linkedNews)
    {
        $this->linkedNews->removeElement($linkedNews);
    }

    /**
     * Get linkedNews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinkedNews()
    {
        return $this->linkedNews;
    }

    /**
     * Set showEnglish
     *
     * @param boolean $showEnglish
     * @return Clinic
     */
    public function setShowEnglish($showEnglish)
    {
        $this->showEnglish = $showEnglish;

        return $this;
    }

    /**
     * Get showEnglish
     *
     * @return boolean
     */
    public function getShowEnglish()
    {
        return $this->showEnglish;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\ClinicTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\ClinicTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     * @return Clinic
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set area
     *
     * @param \AppBundle\Entity\Area $area
     * @return Clinic
     */
    public function setArea(\AppBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \AppBundle\Entity\Area 
     */
    public function getArea()
    {
        return $this->area;
    }
}
