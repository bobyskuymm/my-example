<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 *
 * @ORM\Table(name="service_dictionary")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ServiceDictionaryRepository")
 * @Gedmo\TranslationEntity(class="ServiceDictionaryTranslation")
 */
class ServiceDictionary extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string" )
     */
    private $name;


    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="ServiceLocation", inversedBy="dictionary")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $location;

    /**
     * @ORM\ManyToMany(targetEntity="ServicePackage", mappedBy="options")
     * @ORM\JoinTable(name="service_options_index",
     *      joinColumns={@ORM\JoinColumn(name="package_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="options_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $services;


    /**
     * @ORM\Column(name="enabled", type="boolean")
     * @var boolean
     */
    protected $enabled;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;


    /**
     * @ORM\OneToMany(
     *   targetEntity="ServiceDictionaryTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    
    public function __toString()
    {
        if ($this->getName()) {
            return $this->getName();
        }
        return "";
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->enabled = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return ServiceAdditional
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\ServiceLocation $location
     * @return ServiceAdditional
     */
    public function setLocation(\AppBundle\Entity\ServiceLocation $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\ServiceLocation 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\ServiceAdditionalTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\ServiceAdditionalTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }


    /**
     * Add services
     *
     * @param \AppBundle\Entity\ServicePackage $services
     * @return ServiceDictionary
     */
    public function addService(\AppBundle\Entity\ServicePackage $services)
    {
        $this->services[] = $services;

        return $this;
    }

    /**
     * Remove services
     *
     * @param \AppBundle\Entity\ServicePackage $services
     */
    public function removeService(\AppBundle\Entity\ServicePackage $services)
    {
        $this->services->removeElement($services);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return ServiceDictionary
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return ServiceDictionary
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
}
