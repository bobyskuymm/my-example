<?php

namespace AppBundle\Entity;

/**
 * RealtyRepository
 *
 */
class RealtyRepository extends \Doctrine\ORM\EntityRepository
{

    public function getMinBeds()
    {
        $q = $this->createQueryBuilder('c');

        $q->where('c.bedrooms > 0')
            ->andWhere('c.hide = 0')
            ->andWhere('c.moderated = 1')
            ->orderBy('c.bedrooms','ASC')
            ->setMaxResults(1)
        ;

        $realty  = $q->getQuery()->getResult();
//        dump($re)
        return $realty[0];
//        ->findOneBy(['hide' => false, 'moderated' => true], ['beds' => 'ASC']);
    }

    public function getMinArea()
    {
        $q = $this->createQueryBuilder('c');

        $q->where('c.area > 0')
            ->andWhere('c.hide = 0')
            ->andWhere('c.moderated = 1')
            ->orderBy('c.area','ASC')
            ->setMaxResults(1)
        ;

        $realty  = $q->getQuery()->getResult();
//        dump($re)
        return $realty[0];
//        ->findOneBy(['hide' => false, 'moderated' => true], ['beds' => 'ASC']);
    }


    public function getByFilter($filter = NULL)
    {

        $realty =
            $this->createQueryBuilder("Realty")
                 ->select("Realty", "Realty.price AS HIDDEN price,
                                     Realty.ratingTotal AS HIDDEN rating
                                     ")
                 ->leftJoin("Realty.comfort", "Comfort")
        ;

        if (!empty($filter['comfort'])) {
            $realty = $realty
                ->andWhere("Comfort.id IN (:comfort)")
                ->setParameter(":comfort", $filter['comfort']);
        }

        if (!empty($filter['type'])) {
            $realty = $realty
                ->andWhere("Realty.type = :type")
                ->setParameter(":type", $filter['type']);
        }

        if (!empty($filter['price'])) {

            if($filter['price'][0] != 0) {
                $realty = $realty
                    ->andWhere("Realty.price >= :price_min")
                    ->setParameter(":price_min", $filter['price'][0])
                ;
            }
            if($filter['price'][1] != 0) {
                $realty = $realty
                    ->andWhere("Realty.price <= :price_max")
                    ->setParameter(":price_max", $filter['price'][1])
                ;
            }


        }

        if (!empty($filter['area'])) {
            $realty = $realty
                ->andWhere("Realty.area >= :area_min AND Realty.area <= :area_max")
                ->setParameter(":area_min", $filter['area'][0])
                ->setParameter(":area_max", $filter['area'][1])
            ;
        }

        if (!empty($filter['beds'])) {
            $realty = $realty
                ->andWhere("Realty.bedrooms >= :beds_min AND Realty.bedrooms <= :beds_max")
                ->setParameter(":beds_min", $filter['beds'][0])
                ->setParameter(":beds_max", $filter['beds'][1])
            ;
        }

        if (!empty($filter['locale']) && $filter['locale'] == 'en') {
            $realty = $realty
                ->andWhere("Realty.showEnglish = 1");
        }

        if (!empty($filter['location'])) {
            $realty = $realty
                ->andWhere("Realty.cityArea = :location")
                ->setParameter(":location", $filter['location']);
        }

        if (!empty($filter['city'])) {
            $realty = $realty
                ->andWhere("Realty.city = :city")
                ->setParameter(":city", $filter['city']);
        }


        $realty = $realty
            ->andWhere('Realty.hide != 1')
            ->andWhere('Realty.moderated = 1')
            ->groupBy('Realty.id')
        ;

        $realty = $realty->getQuery();

        // dump($realty); dump($filter);
        // die();

        return $realty;
    }

}
