<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\User;

/**
 * PrivateMessageRepository
 */
class PrivateMessageRepository extends EntityRepository
{

    public function getUserThreads($user)
    {

        $rawThreads =
            $this->createQueryBuilder("PrivateMessage")
                 ->select("PrivateMessage")
                 ->where("PrivateMessage.from = :user")
                 ->orWhere("PrivateMessage.to = :user")
                 ->setParameter(":user", $user)
                 // ->addOrderBy("PrivateMessage.readAt", "DESC")
                 ->addOrderBy("PrivateMessage.createdAt", "ASC")
                 // ->addOrderBy("lastMessage", "DESC")
                 // ->groupBy("PrivateMessage.ident")
                 ->getQuery()
                 ->getResult()
        ;

        $threads = [];

        foreach ($rawThreads as $item) {
            $threads[$item->getIdent()] = $item;
        }

        $sortedThreads = [];
        foreach ($threads as $item) {
            $sortedThreads[$item->getCreatedAt()->getTimestamp()] = $item;
        }

        krsort($sortedThreads);

        return $sortedThreads;
    }

    public function getUnread(User $user)
    {

        $sql = "
            SELECT
                COUNT(id) AS count,
                from_id,
                to_id,
                ident
            FROM private_messages
            WHERE to_id = " . $user->getId() . "
            AND read_at IS NULL
            GROUP BY ident
        ";

        $stmt = $this->getEntityManager()
                     ->getConnection()
                     ->prepare($sql);

        $stmt->execute();

        $result = $stmt->fetchAll();

        $count = [
            'total' => 0,
        ];

        foreach ($result as $row) {
            $count['total'] += $row['count'];
            $count[$row['ident']] = $row['count'];
        }

        return $count;

    }

}
