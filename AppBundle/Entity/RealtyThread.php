<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="realty_threads")
 * @ORM\HasLifecycleCallbacks()
 */
class RealtyThread
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Realty", inversedBy="thread")
     * @ORM\JoinColumn(name="realty_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $realty;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="comment_count", type="integer", nullable=true)
     */
    private $commentCount;

    /**
     * @ORM\OneToMany(targetEntity="RealtyComment", mappedBy="thread", cascade={"all"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $comments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getId();
    }

    public function getFirstLevel()
    {

        return $this->getComments()->map(function($item) {
            if (!$item->getParent()) {
                return $item;
            }
        });

    }

    public function updateCommentCount()
    {
        $this->setCommentCount(count($this->getComments()));
    }

    public function updateRealtyTotalRating()
    {

        $rating = 0; $cou = 0;

        $allRating = [
            'cleanliness'   => 0,
            'location'      => 0,
            'communication' => 0,
            'pricequality'  => 0,
        ];

        foreach ($this->comments as $item) {

            if ($item->getRating()) {
                $rating += $item->getRating()->getCalculatedRating();
                $cou++;

                $allRating['cleanliness']   += $item->getRating()->getCleanliness();
                $allRating['location']      += $item->getRating()->getLocation();
                $allRating['communication'] += $item->getRating()->getCommunication();
                $allRating['pricequality']  += $item->getRating()->getPricequality();

            }

        }

        if ($rating) {

            $rating = $rating / $cou;

            $allRating['cleanliness']   = $allRating['cleanliness'] / $cou;
            $allRating['location']      = $allRating['location'] / $cou;
            $allRating['communication'] = $allRating['communication'] / $cou;
            $allRating['pricequality']  = $allRating['pricequality'] / $cou;

            $this->getRealty()->setRatingTotal($rating);
            $this->getRealty()->setRatingAll($allRating);

        }

        return $rating;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     */
    public function prePersist()
    {
        $this->updateCommentCount();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return RealtyThread
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return RealtyThread
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set commentCount
     *
     * @param integer $commentCount
     * @return RealtyThread
     */
    public function setCommentCount($commentCount)
    {
        $this->commentCount = $commentCount;

        return $this;
    }

    /**
     * Get commentCount
     *
     * @return integer 
     */
    public function getCommentCount()
    {
        return $this->commentCount;
    }

    /**
     * Set realty
     *
     * @param \AppBundle\Entity\Realty $realty
     * @return RealtyThread
     */
    public function setRealty(\AppBundle\Entity\Realty $realty = null)
    {
        $this->realty = $realty;

        return $this;
    }

    /**
     * Get realty
     *
     * @return \AppBundle\Entity\Realty 
     */
    public function getRealty()
    {
        return $this->realty;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return RealtyThread
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     * @return RealtyThread
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Add comments
     *
     * @param \AppBundle\Entity\RealtyComment $comments
     * @return RealtyThread
     */
    public function addComment(\AppBundle\Entity\RealtyComment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \AppBundle\Entity\RealtyComment $comments
     */
    public function removeComment(\AppBundle\Entity\RealtyComment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }
}
