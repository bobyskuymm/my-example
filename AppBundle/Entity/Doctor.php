<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Doctor
 *
 * @FileStore\Uploadable
 * @ORM\Table(name="doctors")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\DoctorRepository")
 * @Gedmo\TranslationEntity(class="DoctorTranslation")
 */
class Doctor extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide", type="boolean", nullable=true)
     */
    private $hide;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_english", type="boolean", nullable=true)
     */
    private $showEnglish;

    /**
     * @var boolean
     *
     * @ORM\Column(name="moderated", type="boolean", nullable=true)
     */
    private $moderated;

    /**
     * @var string
     *
     * @ORM\Column(name="moderator_comment", type="string", length=1024, nullable=true)
     */
    private $moderatorComment;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="first_name", type="string", length=512, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="last_name", type="string", length=512, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="nick_name", type="string", length=512, nullable=true)
     */
    private $nickName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=512, nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var \DateTime
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="social_vk", type="string", length=512, nullable=true)
     * @Assert\Url()
     */
    private $socialVk;

    /**
     * @var string
     *
     * @ORM\Column(name="social_fb", type="string", length=512, nullable=true)
     * @Assert\Url()
     */
    private $socialFb;

    /**
     * @var string
     *
     * @ORM\Column(name="social_gp", type="string", length=512, nullable=true)
     * @Assert\Url()
     */
    private $socialGp;

    /**
     * @var string
     *
     * @ORM\Column(name="social_ok", type="string", length=512, nullable=true)
     * @Assert\Url()
     */
    private $socialOk;

    /**
     * @var string
     *
     * @ORM\Column(name="social_in", type="string", length=512, nullable=true)
     * @Assert\Url()
     */
    private $socialIn;

    /**
     * @var string
     *
     * @ORM\Column(name="skype", type="string", length=512, nullable=true)
     */
    private $skype;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="specialization", type="string", length=512, nullable=true)
     */
    private $specialization;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="full_specialization", type="text", nullable=true)
     */
    private $fullSpecialization;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="special_offer", type="string", length=1024, nullable=true)
     */
    private $specialOffer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="home_visits", type="boolean", nullable=true)
     */
    private $homeVisits;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="cabinet_address", type="string", length=512, nullable=true)
     */
    private $cabinetAddress;

    /**
     * @var array
     *
     * @Assert\File(maxSize="2M")
     * @FileStore\UploadableField(mapping="doctor_cabinet_photo")
     *
     * @ORM\Column(name="cabinet_photo", type="array", nullable=true)
     */
    protected $cabinetPhoto;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="cabinet_about", type="text", nullable=true)
     */
    private $cabinetAbout;

    /**
     * @var integer
     *
     * @ORM\Column(name="publication_count", type="integer", nullable=true)
     */
    private $publicationCount;


    /**
     * @var integer
     *
     * @ORM\Column(name="rating_total", type="float", nullable=true)
     */
    private $ratingTotal;

    /**
     * @var array
     *
     * @ORM\Column(name="rating_all", type="array", nullable=true)
     */
    private $ratingAll;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="about", type="text", nullable=true)
     */
    private $about;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="full_about", type="text", nullable=true)
     */
    private $fullAbout;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="science_degree", type="string", length=1024, nullable=true)
     */
    private $scienceDegree;

    /**
     * @var integer
     *
     * @ORM\Column(name="childbirth_price", type="integer", nullable=true)
     */
    private $childbirthPrice;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Clinic", inversedBy="personnel")
     * @ORM\JoinColumn(name="clinic_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $currentClinic;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="occupation", type="string", length=512, nullable=true)
     */
    private $currentOccupation;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Doctor", inversedBy="substituteFor")
     * @ORM\JoinColumn(name="alternate_doctor_id", referencedColumnName="id")
     */
    protected $alternateDoctor;

    /**
     * @ORM\OneToMany(targetEntity="Doctor", mappedBy="alternateDoctor")
     */
    protected $substituteFor;

    /**
     * @var integer
     *
     * @ORM\Column(name="experience", type="integer", nullable=true)
     */
    private $experience;

    /**
     * @ORM\OneToMany(targetEntity="DoctorDocument", mappedBy="doctor", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $documents;

    /**
     * @ORM\OneToMany(targetEntity="DoctorPhoto", mappedBy="doctor", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $photos;

    /**
     * @ORM\OneToMany(targetEntity="DoctorMedicalPractice", mappedBy="doctor", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $medicalPractice;

    /**
     * @ORM\OneToMany(targetEntity="DoctorEducation", mappedBy="doctor", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $education;

    /**
     * @ORM\OneToMany(targetEntity="DoctorService", mappedBy="doctor", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $services;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="DoctorType", inversedBy="doctors")
     * @ORM\JoinColumn(name="doctor_type_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="DoctorLocation", inversedBy="doctors")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     */
    protected $location;

    /**
     * @ORM\ManyToMany(targetEntity="DoctorLanguage", inversedBy="doctors", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="doctor_languages_index",
     *      joinColumns={@ORM\JoinColumn(name="doctor_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="language_id", referencedColumnName="id")}
     * )
     */
    private $languages;

    /**
     * @ORM\OneToOne(targetEntity="DoctorThread", inversedBy="doctor", cascade={"remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $thread;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="favoriteDoctors", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="favorite_doctors_index",
     *      joinColumns={@ORM\JoinColumn(name="doctor_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $favoriteBy;

    /**
     * @ORM\OneToMany(targetEntity="DoctorPhone", mappedBy="doctor", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $phones;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="doctor")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="AppointmentApplication", mappedBy="doctor")
     */
    protected $appointmentApplications;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @Gedmo\Slug(fields={"firstName", "lastName"})
     * @ORM\Column(length=128, unique=true)
     */
    private $url;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;


    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="City", inversedBy="doctors")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $city;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Area", inversedBy="doctors")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $area;



    /**
     * @ORM\OneToMany(
     *   targetEntity="DoctorTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;


    public function __toString()
    {
        return $this->getName() ? $this->getName() : '';
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->substituteFor = new \Doctrine\Common\Collections\ArrayCollection();
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->medicalPractice = new \Doctrine\Common\Collections\ArrayCollection();
        $this->education = new \Doctrine\Common\Collections\ArrayCollection();
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
        $this->phones = new \Doctrine\Common\Collections\ArrayCollection();

        $this->setModerated(false);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * Set specialization
     *
     * @param string $specialization
     * @return Doctor
     */
    public function setSpecialization($specialization)
    {
        $this->specialization = $specialization;

        return $this;
    }

    /**
     * Get specialization
     *
     * @return string
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * Set fullSpecialization
     *
     * @param string $fullSpecialization
     * @return Doctor
     */
    public function setFullSpecialization($fullSpecialization)
    {
        $this->fullSpecialization = $fullSpecialization;

        return $this;
    }

    /**
     * Get fullSpecialization
     *
     * @return string
     */
    public function getFullSpecialization()
    {
        return $this->fullSpecialization;
    }

    /**
     * Set specialOffer
     *
     * @param string $specialOffer
     * @return Doctor
     */
    public function setSpecialOffer($specialOffer)
    {
        $this->specialOffer = $specialOffer;

        return $this;
    }

    /**
     * Get specialOffer
     *
     * @return string
     */
    public function getSpecialOffer()
    {
        return $this->specialOffer;
    }

    /**
     * Set homeVisits
     *
     * @param boolean $homeVisits
     * @return Doctor
     */
    public function setHomeVisits($homeVisits)
    {
        $this->homeVisits = $homeVisits;

        return $this;
    }

    /**
     * Get homeVisits
     *
     * @return boolean
     */
    public function getHomeVisits()
    {
        return $this->homeVisits;
    }

    /**
     * Set cabinetAddress
     *
     * @param string $cabinetAddress
     * @return Doctor
     */
    public function setCabinetAddress($cabinetAddress)
    {
        $this->cabinetAddress = $cabinetAddress;

        return $this;
    }

    /**
     * Get cabinetAddress
     *
     * @return string
     */
    public function getCabinetAddress()
    {
        return $this->cabinetAddress;
    }

    /**
     * Set cabinetPhoto
     *
     * @param array $cabinetPhoto
     * @return Doctor
     */
    public function setCabinetPhoto($cabinetPhoto)
    {
        $this->cabinetPhoto = $cabinetPhoto;

        return $this;
    }

    /**
     * Get cabinetPhoto
     *
     * @return array
     */
    public function getCabinetPhoto()
    {
        return $this->cabinetPhoto;
    }

    /**
     * Set cabinetAbout
     *
     * @param string $cabinetAbout
     * @return Doctor
     */
    public function setCabinetAbout($cabinetAbout)
    {
        $this->cabinetAbout = $cabinetAbout;

        return $this;
    }

    /**
     * Get cabinetAbout
     *
     * @return string
     */
    public function getCabinetAbout()
    {
        return $this->cabinetAbout;
    }

    /**
     * Set publicationCount
     *
     * @param integer $publicationCount
     * @return Doctor
     */
    public function setPublicationCount($publicationCount)
    {
        $this->publicationCount = $publicationCount;

        return $this;
    }

    /**
     * Get publicationCount
     *
     * @return integer
     */
    public function getPublicationCount()
    {
        return $this->publicationCount;
    }

    /**
     * Set about
     *
     * @param string $about
     * @return Doctor
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set fullAbout
     *
     * @param string $fullAbout
     * @return Doctor
     */
    public function setFullAbout($fullAbout)
    {
        $this->fullAbout = $fullAbout;

        return $this;
    }

    /**
     * Get fullAbout
     *
     * @return string
     */
    public function getFullAbout()
    {
        return $this->fullAbout;
    }

    /**
     * Set scienceDegree
     *
     * @param string $scienceDegree
     * @return Doctor
     */
    public function setScienceDegree($scienceDegree)
    {
        $this->scienceDegree = $scienceDegree;

        return $this;
    }

    /**
     * Get scienceDegree
     *
     * @return string
     */
    public function getScienceDegree()
    {
        return $this->scienceDegree;
    }

    /**
     * Set childbirthPrice
     *
     * @param integer $childbirthPrice
     * @return Doctor
     */
    public function setChildbirthPrice($childbirthPrice)
    {
        $this->childbirthPrice = $childbirthPrice;

        return $this;
    }

    /**
     * Get childbirthPrice
     *
     * @return integer
     */
    public function getChildbirthPrice()
    {
        return $this->childbirthPrice;
    }

    /**
     * Set currentOccupation
     *
     * @param string $currentOccupation
     * @return Doctor
     */
    public function setCurrentOccupation($currentOccupation)
    {
        $this->currentOccupation = $currentOccupation;

        return $this;
    }

    /**
     * Get currentOccupation
     *
     * @return string
     */
    public function getCurrentOccupation()
    {
        return $this->currentOccupation;
    }

    /**
     * Set experience
     *
     * @param integer $experience
     * @return Doctor
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return integer
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Doctor
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Doctor
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Doctor
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set currentClinic
     *
     * @param \AppBundle\Entity\Clinic $currentClinic
     * @return Doctor
     */
    public function setCurrentClinic(\AppBundle\Entity\Clinic $currentClinic = null)
    {
        $this->currentClinic = $currentClinic;

        return $this;
    }

    /**
     * Get currentClinic
     *
     * @return \AppBundle\Entity\Clinic
     */
    public function getCurrentClinic()
    {
        return $this->currentClinic;
    }

    /**
     * Set alternateDoctor
     *
     * @param \AppBundle\Entity\Doctor $alternateDoctor
     * @return Doctor
     */
    public function setAlternateDoctor(\AppBundle\Entity\Doctor $alternateDoctor = null)
    {
        $this->alternateDoctor = $alternateDoctor;

        return $this;
    }

    /**
     * Get alternateDoctor
     *
     * @return \AppBundle\Entity\Doctor
     */
    public function getAlternateDoctor()
    {
        return $this->alternateDoctor;
    }

    /**
     * Add substituteFor
     *
     * @param \AppBundle\Entity\Doctor $substituteFor
     * @return Doctor
     */
    public function addSubstituteFor(\AppBundle\Entity\Doctor $substituteFor)
    {
        $this->substituteFor[] = $substituteFor;

        return $this;
    }

    /**
     * Remove substituteFor
     *
     * @param \AppBundle\Entity\Doctor $substituteFor
     */
    public function removeSubstituteFor(\AppBundle\Entity\Doctor $substituteFor)
    {
        $this->substituteFor->removeElement($substituteFor);
    }

    /**
     * Get substituteFor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubstituteFor()
    {
        return $this->substituteFor;
    }



    /**
     * Add photos
     *
     * @param \AppBundle\Entity\DoctorPhoto $photos
     * @return Doctor
     */
    public function addPhoto(\AppBundle\Entity\DoctorPhoto $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \AppBundle\Entity\DoctorPhoto $photos
     */
    public function removePhoto(\AppBundle\Entity\DoctorPhoto $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Add medicalPractice
     *
     * @param \AppBundle\Entity\DoctorMedicalPractice $medicalPractice
     * @return Doctor
     */
    public function addMedicalPractice(\AppBundle\Entity\DoctorMedicalPractice $medicalPractice)
    {
        $this->medicalPractice[] = $medicalPractice;

        return $this;
    }

    /**
     * Remove medicalPractice
     *
     * @param \AppBundle\Entity\DoctorMedicalPractice $medicalPractice
     */
    public function removeMedicalPractice($medicalPractice)
    {
        $this->medicalPractice->removeElement($medicalPractice);
    }

    /**
     * Get medicalPractice
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedicalPractice()
    {
        return $this->medicalPractice;
    }

    /**
     * Add education
     *
     * @param \AppBundle\Entity\DoctorEducation $education
     * @return Doctor
     */
    public function addEducation(\AppBundle\Entity\DoctorEducation $education)
    {
        $this->education[] = $education;

        return $this;
    }

    /**
     * Remove education
     *
     * @param \AppBundle\Entity\DoctorEducation $education
     */
    public function removeEducation($education)
    {
        $this->education->removeElement($education);
    }

    /**
     * Get education
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * Add services
     *
     * @param \AppBundle\Entity\DoctorService $services
     * @return Doctor
     */
    public function addService(\AppBundle\Entity\DoctorService $services)
    {
        $this->services[] = $services;

        return $this;
    }

    /**
     * Remove services
     *
     * @param \AppBundle\Entity\DoctorService $services
     */
    public function removeService($services)
    {
        $this->services->removeElement($services);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return Doctor
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     * @return Doctor
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\DoctorType $type
     * @return Doctor
     */
    public function setType(\AppBundle\Entity\DoctorType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\DoctorType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\DoctorLocation $location
     * @return Doctor
     */
    public function setLocation(\AppBundle\Entity\DoctorLocation $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\DoctorLocation
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Add languages
     *
     * @param \AppBundle\Entity\DoctorLanguage $languages
     * @return Doctor
     */
    public function addLanguage(\AppBundle\Entity\DoctorLanguage $languages)
    {
        $this->languages[] = $languages;

        return $this;
    }

    /**
     * Remove languages
     *
     * @param \AppBundle\Entity\DoctorLanguage $languages
     */
    public function removeLanguage(\AppBundle\Entity\DoctorLanguage $languages)
    {
        $this->languages->removeElement($languages);
    }

    /**
     * Get languages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Add documents
     *
     * @param \AppBundle\Entity\DoctorDocument $documents
     * @return Doctor
     */
    public function addDocument(\AppBundle\Entity\DoctorDocument $documents)
    {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param \AppBundle\Entity\DoctorDocument $documents
     */
    public function removeDocument($documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set documents
     *
     * @param \Doctrine\Common\Collections\Collection $documents
     * @return Doctor
     */
    public function setDocuments(\Doctrine\Common\Collections\Collection $documents)
    {
        $this->documents = $documents;

        return $this;
    }



    /**
     * Set ratingTotal
     *
     * @param float $ratingTotal
     * @return Doctor
     */
    public function setRatingTotal($ratingTotal)
    {
        $this->ratingTotal = $ratingTotal;

        return $this;
    }

    /**
     * Get ratingTotal
     *
     * @return float
     */
    public function getRatingTotal()
    {
        return $this->ratingTotal;
    }

    /**
     * Set thread
     *
     * @param \AppBundle\Entity\DoctorThread $thread
     * @return Doctor
     */
    public function setThread(\AppBundle\Entity\DoctorThread $thread = null)
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * Get thread
     *
     * @return \AppBundle\Entity\DoctorThread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * Set ratingAll
     *
     * @param array $ratingAll
     * @return Doctor
     */
    public function setRatingAll($ratingAll)
    {
        $this->ratingAll = $ratingAll;

        return $this;
    }

    /**
     * Get ratingAll
     *
     * @return array
     */
    public function getRatingAll()
    {
        return $this->ratingAll;
    }

    /**
     * Add favoriteBy
     *
     * @param \AppBundle\Entity\User $favoriteBy
     * @return Doctor
     */
    public function addFavoriteBy(\AppBundle\Entity\User $favoriteBy)
    {
        $this->favoriteBy[] = $favoriteBy;

        return $this;
    }

    /**
     * Remove favoriteBy
     *
     * @param \AppBundle\Entity\User $favoriteBy
     */
    public function removeFavoriteBy(\AppBundle\Entity\User $favoriteBy)
    {
        $this->favoriteBy->removeElement($favoriteBy);
    }

    /**
     * Get favoriteBy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteBy()
    {
        return $this->favoriteBy;
    }

    public function getTotalComments()
    {
        if (!$this->getThread()) {
            return 0;
        }

        return ($this->getThread()->getCommentCount()) ? $this->getThread()->getCommentCount() : 0;
    }


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Doctor
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set hide
     *
     * @param boolean $hide
     * @return Doctor
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return boolean
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Doctor
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Doctor
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Add phones
     *
     * @param \AppBundle\Entity\DoctorPhone $phones
     * @return Doctor
     */
    public function addPhone(\AppBundle\Entity\DoctorPhone $phones)
    {
        $this->phones[] = $phones;

        return $this;
    }

    /**
     * Remove phones
     *
     * @param \AppBundle\Entity\DoctorPhone $phones
     */
    public function removePhone(\AppBundle\Entity\DoctorPhone $phones)
    {
        $this->phones->removeElement($phones);
    }

    /**
     * Get phones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set nickName
     *
     * @param string $nickName
     * @return Doctor
     */
    public function setNickName($nickName)
    {
        $this->nickName = $nickName;

        return $this;
    }

    /**
     * Get nickName
     *
     * @return string
     */
    public function getNickName()
    {
        return $this->nickName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Doctor
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set socialVk
     *
     * @param string $socialVk
     * @return Doctor
     */
    public function setSocialVk($socialVk)
    {
        $this->socialVk = $socialVk;

        return $this;
    }

    /**
     * Get socialVk
     *
     * @return string
     */
    public function getSocialVk()
    {
        return $this->socialVk;
    }

    /**
     * Set socialFb
     *
     * @param string $socialFb
     * @return Doctor
     */
    public function setSocialFb($socialFb)
    {
        $this->socialFb = $socialFb;

        return $this;
    }

    /**
     * Get socialFb
     *
     * @return string
     */
    public function getSocialFb()
    {
        return $this->socialFb;
    }

    /**
     * Set socialGp
     *
     * @param string $socialGp
     * @return Doctor
     */
    public function setSocialGp($socialGp)
    {
        $this->socialGp = $socialGp;

        return $this;
    }

    /**
     * Get socialGp
     *
     * @return string
     */
    public function getSocialGp()
    {
        return $this->socialGp;
    }

    /**
     * Set socialOk
     *
     * @param string $socialOk
     * @return Doctor
     */
    public function setSocialOk($socialOk)
    {
        $this->socialOk = $socialOk;

        return $this;
    }

    /**
     * Get socialOk
     *
     * @return string
     */
    public function getSocialOk()
    {
        return $this->socialOk;
    }

    /**
     * Set socialIn
     *
     * @param string $socialIn
     * @return Doctor
     */
    public function setSocialIn($socialIn)
    {
        $this->socialIn = $socialIn;

        return $this;
    }

    /**
     * Get socialIn
     *
     * @return string
     */
    public function getSocialIn()
    {
        return $this->socialIn;
    }

    /**
     * Set skype
     *
     * @param string $skype
     * @return Doctor
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Doctor
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Add appointmentApplications
     *
     * @param \AppBundle\Entity\AppointmentApplication $appointmentApplications
     * @return Doctor
     */
    public function addAppointmentApplication(\AppBundle\Entity\AppointmentApplication $appointmentApplications)
    {
        $this->appointmentApplications[] = $appointmentApplications;

        return $this;
    }

    /**
     * Remove appointmentApplications
     *
     * @param \AppBundle\Entity\AppointmentApplication $appointmentApplications
     */
    public function removeAppointmentApplication(\AppBundle\Entity\AppointmentApplication $appointmentApplications)
    {
        $this->appointmentApplications->removeElement($appointmentApplications);
    }

    /**
     * Get appointmentApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppointmentApplications()
    {
        return $this->appointmentApplications;
    }

    /**
     * Set moderated
     *
     * @param boolean $moderated
     * @return Doctor
     */
    public function setModerated($moderated)
    {
        $this->moderated = $moderated;

        return $this;
    }

    /**
     * Get moderated
     *
     * @return boolean
     */
    public function getModerated()
    {
        return $this->moderated;
    }

    /**
     * Set moderatorComment
     *
     * @param string $moderatorComment
     * @return Doctor
     */
    public function setModeratorComment($moderatorComment)
    {
        $this->moderatorComment = $moderatorComment;

        return $this;
    }

    /**
     * Get moderatorComment
     *
     * @return string
     */
    public function getModeratorComment()
    {
        return $this->moderatorComment;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\DoctorTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\DoctorTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Set showEnglish
     *
     * @param boolean $showEnglish
     * @return Doctor
     */
    public function setShowEnglish($showEnglish)
    {
        $this->showEnglish = $showEnglish;

        return $this;
    }

    /**
     * Get showEnglish
     *
     * @return boolean
     */
    public function getShowEnglish()
    {
        return $this->showEnglish;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Doctor
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     * @return Doctor
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set area
     *
     * @param \AppBundle\Entity\Area $area
     * @return Doctor
     */
    public function setArea(\AppBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \AppBundle\Entity\Area 
     */
    public function getArea()
    {
        return $this->area;
    }
}
