<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * @FileStore\Uploadable
 * @ORM\Table(name="banners")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\BannerRepository")
 */
class Banner extends AbstractPersonalTranslatable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string" )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", nullable=true )
     */
    private $link;


    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer" )
     */
    private $type;

    /**
     * @var array
     *
     * @FileStore\UploadableField(mapping="image")
     *
     * @ORM\Column(name="image", type="array", nullable=true)
     */
    protected $image;



    /**
     * @ORM\Column(name="enabled", type="boolean")
     * @var boolean
     */
    protected $enabled;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="ServiceLocation", inversedBy="additional")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $location;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;



    public function __toString()
    {
        if ($this->getName()) {
            return $this->getName();
        }
        return "";
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->enabled = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set name
     *
     * @param string $name
     * @return Banner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Banner
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set image
     *
     * @param array $image
     * @return Banner
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return array 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Banner
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Banner
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }


    /**
     * Set link
     *
     * @param string $link
     * @return Banner
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\ServiceLocation $location
     * @return Banner
     */
    public function setLocation(\AppBundle\Entity\ServiceLocation $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\ServiceLocation 
     */
    public function getLocation()
    {
        return $this->location;
    }
}
