<?php

namespace AppBundle\Entity;

/**
 * TopicRepository
 *
 */
class ServiceAdditionalRepository extends \Doctrine\ORM\EntityRepository
{
    public function getService($location)
    {
        $q = $this->createQueryBuilder('s');

        $q->where('s.location = :l')->setParameter('l', $location);
        $q->andWhere('s.enabled = :e')->setParameter('e', true);
        $q->orderBy('s.position','ASC');

        return $q->getQuery()->getResult();
    }

}
