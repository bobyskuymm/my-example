<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Realty
 *
 * @ORM\Table(name="realty")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\RealtyRepository")
 * @Gedmo\TranslationEntity(class="RealtyTranslation")
 */
class Realty extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=512, nullable=true)
     */
    private $name;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="City", inversedBy="realties")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $city;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Area", inversedBy="realties")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $cityArea;


    /**
     * @ORM\OneToMany(targetEntity="LeaseApplication", mappedBy="realty")
     */
    protected $leaseApplications;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="about", type="text", nullable=true)
     */
    private $about;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="short_about", type="text", nullable=true)
     */
    private $shortAbout;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="possible_discounts", type="string", length=1024, nullable=true)
     */
    private $possibleDiscounts;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="address", type="string", length=512, nullable=true)
     */
    private $address;

    /**
     * @ORM\OneToMany(targetEntity="RealtyPhoto", mappedBy="realty", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $photos;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="realty")
     */
    protected $user;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="RealtyType")
     * @ORM\JoinColumn(name="realty_type_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @ORM\ManyToMany(targetEntity="RealtyComfort", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="realty_comfort_index",
     *      joinColumns={@ORM\JoinColumn(name="realty_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="comfort_id", referencedColumnName="id")}
     * )
     */
    private $comfort;

    /**
     * @var integer
     *
     * @ORM\Column(name="bedrooms", type="integer", nullable=true)
     */
    private $bedrooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="bathrooms", type="integer", nullable=true)
     */
    private $bathrooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="beds", type="integer", nullable=true)
     */
    private $beds;

    /**
     * @var integer
     *
     * @ORM\Column(name="area", type="integer", nullable=true)
     */
    private $area;

    /**
     * @var integer
     *
     * @ORM\Column(name="floor", type="integer", nullable=true)
     */
    private $floor;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_rental_period", type="integer", nullable=true)
     */
    private $minRentalPeriod;

    /**
     * @var boolean
     *
     * @ORM\Column(name="children_allowed", type="boolean", nullable=true)
     */
    private $childrenAllowed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="animals_allowed", type="boolean", nullable=true)
     */
    private $animalsAllowed;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="not_included", type="string", length=2048, nullable=true)
     */
    private $notIncluded;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=128, nullable=true)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="string", length=128, nullable=true)
     */
    private $lng;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="favoriteRealty", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="favorite_realty_index",
     *      joinColumns={@ORM\JoinColumn(name="realty_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $favoriteBy;

    /**
     * @ORM\OneToOne(targetEntity="RealtyThread", mappedBy="realty")
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $thread;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide", type="boolean", nullable=true)
     */
    private $hide;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_english", type="boolean")
     */
    private $showEnglish;

    /**
     * @var boolean
     *
     * @ORM\Column(name="moderated", type="boolean", nullable=true)
     */
    private $moderated;

    /**
     * @var string
     *
     * @ORM\Column(name="moderator_comment", type="string", length=1024, nullable=true)
     */
    private $moderatorComment;

    /**
     * @var integer
     *
     * @ORM\Column(name="rating_total", type="float", nullable=true)
     */
    private $ratingTotal;

    /**
     * @var array
     *
     * @ORM\Column(name="rating_all", type="array", nullable=true)
     */
    private $ratingAll;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $url;

    /**
     * @ORM\OneToMany(
     *   targetEntity="RealtyTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    public function __toString()
    {
        if ($this->getName()) {
            return $this->getName();
        }
        return "";
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->leaseApplications = new \Doctrine\Common\Collections\ArrayCollection();
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comfort = new \Doctrine\Common\Collections\ArrayCollection();
        $this->favoriteBy = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Realty
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Realty
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set about
     *
     * @param string $about
     * @return Realty
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set shortAbout
     *
     * @param string $shortAbout
     * @return Realty
     */
    public function setShortAbout($shortAbout)
    {
        $this->shortAbout = $shortAbout;

        return $this;
    }

    /**
     * Get shortAbout
     *
     * @return string
     */
    public function getShortAbout()
    {
        return $this->shortAbout;
    }

    /**
     * Set possibleDiscounts
     *
     * @param string $possibleDiscounts
     * @return Realty
     */
    public function setPossibleDiscounts($possibleDiscounts)
    {
        $this->possibleDiscounts = $possibleDiscounts;

        return $this;
    }

    /**
     * Get possibleDiscounts
     *
     * @return string
     */
    public function getPossibleDiscounts()
    {
        return $this->possibleDiscounts;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Realty
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set bedrooms
     *
     * @param integer $bedrooms
     * @return Realty
     */
    public function setBedrooms($bedrooms)
    {
        $this->bedrooms = $bedrooms;

        return $this;
    }

    /**
     * Get bedrooms
     *
     * @return integer
     */
    public function getBedrooms()
    {
        return $this->bedrooms;
    }

    /**
     * Set bathrooms
     *
     * @param integer $bathrooms
     * @return Realty
     */
    public function setBathrooms($bathrooms)
    {
        $this->bathrooms = $bathrooms;

        return $this;
    }

    /**
     * Get bathrooms
     *
     * @return integer
     */
    public function getBathrooms()
    {
        return $this->bathrooms;
    }

    /**
     * Set beds
     *
     * @param integer $beds
     * @return Realty
     */
    public function setBeds($beds)
    {
        $this->beds = $beds;

        return $this;
    }

    /**
     * Get beds
     *
     * @return integer
     */
    public function getBeds()
    {
        return $this->beds;
    }

    /**
     * Set area
     *
     * @param integer $area
     * @return Realty
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return integer
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set floor
     *
     * @param integer $floor
     * @return Realty
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return integer
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set minRentalPeriod
     *
     * @param integer $minRentalPeriod
     * @return Realty
     */
    public function setMinRentalPeriod($minRentalPeriod)
    {
        $this->minRentalPeriod = $minRentalPeriod;

        return $this;
    }

    /**
     * Get minRentalPeriod
     *
     * @return integer
     */
    public function getMinRentalPeriod()
    {
        return $this->minRentalPeriod;
    }

    /**
     * Set childrenAllowed
     *
     * @param boolean $childrenAllowed
     * @return Realty
     */
    public function setChildrenAllowed($childrenAllowed)
    {
        $this->childrenAllowed = $childrenAllowed;

        return $this;
    }

    /**
     * Get childrenAllowed
     *
     * @return boolean
     */
    public function getChildrenAllowed()
    {
        return $this->childrenAllowed;
    }

    /**
     * Set animalsAllowed
     *
     * @param boolean $animalsAllowed
     * @return Realty
     */
    public function setAnimalsAllowed($animalsAllowed)
    {
        $this->animalsAllowed = $animalsAllowed;

        return $this;
    }

    /**
     * Get animalsAllowed
     *
     * @return boolean
     */
    public function getAnimalsAllowed()
    {
        return $this->animalsAllowed;
    }

    /**
     * Set notIncluded
     *
     * @param string $notIncluded
     * @return Realty
     */
    public function setNotIncluded($notIncluded)
    {
        $this->notIncluded = $notIncluded;

        return $this;
    }

    /**
     * Get notIncluded
     *
     * @return string
     */
    public function getNotIncluded()
    {
        return $this->notIncluded;
    }

    /**
     * Set hide
     *
     * @param boolean $hide
     * @return Realty
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return boolean
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set moderated
     *
     * @param boolean $moderated
     * @return Realty
     */
    public function setModerated($moderated)
    {
        $this->moderated = $moderated;

        return $this;
    }

    /**
     * Get moderated
     *
     * @return boolean
     */
    public function getModerated()
    {
        return $this->moderated;
    }

    /**
     * Set moderatorComment
     *
     * @param string $moderatorComment
     * @return Realty
     */
    public function setModeratorComment($moderatorComment)
    {
        $this->moderatorComment = $moderatorComment;

        return $this;
    }

    /**
     * Get moderatorComment
     *
     * @return string
     */
    public function getModeratorComment()
    {
        return $this->moderatorComment;
    }

    /**
     * Set ratingTotal
     *
     * @param float $ratingTotal
     * @return Realty
     */
    public function setRatingTotal($ratingTotal)
    {
        $this->ratingTotal = $ratingTotal;

        return $this;
    }

    /**
     * Get ratingTotal
     *
     * @return float
     */
    public function getRatingTotal()
    {
        return $this->ratingTotal;
    }

    /**
     * Set ratingAll
     *
     * @param array $ratingAll
     * @return Realty
     */
    public function setRatingAll($ratingAll)
    {
        $this->ratingAll = $ratingAll;

        return $this;
    }

    /**
     * Get ratingAll
     *
     * @return array
     */
    public function getRatingAll()
    {
        return $this->ratingAll;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Realty
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Realty
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Realty
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add leaseApplications
     *
     * @param \AppBundle\Entity\LeaseApplication $leaseApplications
     * @return Realty
     */
    public function addLeaseApplication(\AppBundle\Entity\LeaseApplication $leaseApplications)
    {
        $this->leaseApplications[] = $leaseApplications;

        return $this;
    }

    /**
     * Remove leaseApplications
     *
     * @param \AppBundle\Entity\LeaseApplication $leaseApplications
     */
    public function removeLeaseApplication(\AppBundle\Entity\LeaseApplication $leaseApplications)
    {
        $this->leaseApplications->removeElement($leaseApplications);
    }

    /**
     * Get leaseApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLeaseApplications()
    {
        return $this->leaseApplications;
    }

    /**
     * Add photos
     *
     * @param \AppBundle\Entity\RealtyPhoto $photos
     * @return Realty
     */
    public function addPhoto(\AppBundle\Entity\RealtyPhoto $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \AppBundle\Entity\RealtyPhoto $photos
     */
    public function removePhoto(\AppBundle\Entity\RealtyPhoto $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Realty
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\RealtyType $type
     * @return Realty
     */
    public function setType(\AppBundle\Entity\RealtyType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\RealtyType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add comfort
     *
     * @param \AppBundle\Entity\RealtyComfort $comfort
     * @return Realty
     */
    public function addComfort(\AppBundle\Entity\RealtyComfort $comfort)
    {
        $this->comfort[] = $comfort;

        return $this;
    }

    /**
     * Remove comfort
     *
     * @param \AppBundle\Entity\RealtyComfort $comfort
     */
    public function removeComfort(\AppBundle\Entity\RealtyComfort $comfort)
    {
        $this->comfort->removeElement($comfort);
    }

    /**
     * Get comfort
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComfort()
    {
        return $this->comfort;
    }

    /**
     * Add favoriteBy
     *
     * @param \AppBundle\Entity\User $favoriteBy
     * @return Realty
     */
    public function addFavoriteBy(\AppBundle\Entity\User $favoriteBy)
    {
        $this->favoriteBy[] = $favoriteBy;

        return $this;
    }

    /**
     * Remove favoriteBy
     *
     * @param \AppBundle\Entity\User $favoriteBy
     */
    public function removeFavoriteBy(\AppBundle\Entity\User $favoriteBy)
    {
        $this->favoriteBy->removeElement($favoriteBy);
    }

    /**
     * Get favoriteBy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteBy()
    {
        return $this->favoriteBy;
    }

    /**
     * Set thread
     *
     * @param \AppBundle\Entity\RealtyThread $thread
     * @return Realty
     */
    public function setThread(\AppBundle\Entity\RealtyThread $thread = null)
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * Get thread
     *
     * @return \AppBundle\Entity\RealtyThread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return Realty
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     * @return Realty
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function getTotalComments()
    {
        if (!$this->getThread()) {
            return 0;
        }

        return ($this->getThread()->getCommentCount()) ? $this->getThread()->getCommentCount() : 0;
    }



    /**
     * Set showEnglish
     *
     * @param boolean $showEnglish
     * @return Realty
     */
    public function setShowEnglish($showEnglish)
    {
        $this->showEnglish = $showEnglish;

        return $this;
    }

    /**
     * Get showEnglish
     *
     * @return boolean 
     */
    public function getShowEnglish()
    {
        return $this->showEnglish;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\RealtyTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\RealtyTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     * @return Realty
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set cityArea
     *
     * @param \AppBundle\Entity\Area $cityArea
     * @return Realty
     */
    public function setCityArea(\AppBundle\Entity\Area $cityArea = null)
    {
        $this->cityArea = $cityArea;

        return $this;
    }

    /**
     * Get cityArea
     *
     * @return \AppBundle\Entity\Area 
     */
    public function getCityArea()
    {
        return $this->cityArea;
    }
}
