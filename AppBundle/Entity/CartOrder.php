<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Clinic
 *
 * @ORM\Table(name="cart_order")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CartOrderRepository")
 * @Gedmo\TranslationEntity(class="CartOrderTranslation")
 */
class CartOrder extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_sum", type="integer" )
     */
    private $totalSum;


    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer" )
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="cart_order", type="string", length=2048 )
     */
    private $cartOrder;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="User", inversedBy="orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;


    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="datetime" )
     */
    private $createdAt;



    function __construct()
    {
        $this->status = 1;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set totalSum
     *
     * @param integer $totalSum
     * @return CartOrder
     */
    public function setTotalSum($totalSum)
    {
        $this->totalSum = $totalSum;

        return $this;
    }

    /**
     * Get totalSum
     *
     * @return integer 
     */
    public function getTotalSum()
    {
        return $this->totalSum;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return CartOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set cartOrder
     *
     * @param string $cartOrder
     * @return CartOrder
     */
    public function setCartOrder($cartOrder)
    {
        $this->cartOrder = $cartOrder;

        return $this;
    }

    /**
     * Get cartOrder
     *
     * @return string 
     */
    public function getCartOrder()
    {
        return $this->cartOrder;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return CartOrder
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CartOrder
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
