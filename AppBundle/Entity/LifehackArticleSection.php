<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * LifehackArticleSection
 *
 * @FileStore\Uploadable
 * @ORM\Table(name="lifehack_sections")
 * @Gedmo\TranslationEntity(class="LifehackArticleSectionTranslation")
 * @ORM\Entity
 */
class LifehackArticleSection extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(length=128, unique=true)
     */
    private $url;

    /**
     * @ORM\OneToMany(targetEntity="LifehackArticle", mappedBy="section")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $articles;

    /**
     * @ORM\OneToMany(
     *   targetEntity="LifehackArticleSectionTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    public function __toString()
    {
        return ($this->getName()) ? $this->getName() : '';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return LifehackArticleSection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return LifehackArticleSection
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add articles
     *
     * @param \AppBundle\Entity\LifehackArticle $articles
     * @return LifehackArticleSection
     */
    public function addArticle(\AppBundle\Entity\LifehackArticle $articles)
    {
        $this->articles[] = $articles;

        return $this;
    }

    /**
     * Remove articles
     *
     * @param \AppBundle\Entity\LifehackArticle $articles
     */
    public function removeArticle(\AppBundle\Entity\LifehackArticle $articles)
    {
        $this->articles->removeElement($articles);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\LifehackArticleSectionTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\LifehackArticleSectionTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }
}
