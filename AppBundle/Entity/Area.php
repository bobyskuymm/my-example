<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Clinic
 *
 * @ORM\Table(name="area_list")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AreaRepository")
 * @Gedmo\TranslationEntity(class="AreaTranslation")
 */
class Area extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=512, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(
     *   targetEntity="AreaTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Clinic", mappedBy="area")
     */
    protected $clinic;
    //city;

    /**
     * @ORM\ManyToMany(targetEntity="City", inversedBy="areas", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="area_cities_index",
     *      joinColumns={@ORM\JoinColumn(name="area_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="city_id", referencedColumnName="id")}
     * )
     */
    private $cities;

    /**
     * @ORM\OneToMany(targetEntity="Doctor", mappedBy="area")
     */
    protected $doctors;

    /**
     * @ORM\OneToMany(targetEntity="Realty", mappedBy="cityArea")
     */
    protected $realties;

    public function __toString()
    {
        if ($this->getName()) {
            return $this->getName();
        }
        return "";
    }


    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Area
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\AreaTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\AreaTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Add clinic
     *
     * @param \AppBundle\Entity\Clinic $clinic
     * @return Area
     */
    public function addClinic(\AppBundle\Entity\Clinic $clinic)
    {
        $this->clinic[] = $clinic;

        return $this;
    }

    /**
     * Remove clinic
     *
     * @param \AppBundle\Entity\Clinic $clinic
     */
    public function removeClinic(\AppBundle\Entity\Clinic $clinic)
    {
        $this->clinic->removeElement($clinic);
    }

    /**
     * Get clinic
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * Add cities
     *
     * @param \AppBundle\Entity\City $cities
     * @return Area
     */
    public function addCity(\AppBundle\Entity\City $cities)
    {
        $this->cities[] = $cities;

        return $this;
    }

    /**
     * Remove cities
     *
     * @param \AppBundle\Entity\City $cities
     */
    public function removeCity(\AppBundle\Entity\City $cities)
    {
        $this->cities->removeElement($cities);
    }

    /**
     * Get cities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * Add doctors
     *
     * @param \AppBundle\Entity\Doctor $doctors
     * @return Area
     */
    public function addDoctor(\AppBundle\Entity\Doctor $doctors)
    {
        $this->doctors[] = $doctors;

        return $this;
    }

    /**
     * Remove doctors
     *
     * @param \AppBundle\Entity\Doctor $doctors
     */
    public function removeDoctor(\AppBundle\Entity\Doctor $doctors)
    {
        $this->doctors->removeElement($doctors);
    }

    /**
     * Get doctors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDoctors()
    {
        return $this->doctors;
    }

    /**
     * Add realties
     *
     * @param \AppBundle\Entity\Realty $realties
     * @return Area
     */
    public function addRealty(\AppBundle\Entity\Realty $realties)
    {
        $this->realties[] = $realties;

        return $this;
    }

    /**
     * Remove realties
     *
     * @param \AppBundle\Entity\Realty $realties
     */
    public function removeRealty(\AppBundle\Entity\Realty $realties)
    {
        $this->realties->removeElement($realties);
    }

    /**
     * Get realties
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRealties()
    {
        return $this->realties;
    }
}
