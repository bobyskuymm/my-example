<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Topic
 *
 * @FileStore\Uploadable
 * @ORM\Table(name="topics")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TopicRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Topic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=512)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $ratingTotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="likes", type="integer", nullable=true)
     */
    private $likeTotal;

    /**
     * @var array
     *
     * @Assert\File(maxSize="2M")
     * @FileStore\UploadableField(mapping="topic_image")
     *
     * @ORM\Column(name="image", type="array", nullable=true)
     */
    protected $image;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="TopicSection", inversedBy="topics")
     * @ORM\JoinColumn(name="section_id", referencedColumnName="id")
     */
    protected $section;

    /**
     * @ORM\ManyToMany(targetEntity="TopicTag", inversedBy="topics", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="topic_tags_index",
     *      joinColumns={@ORM\JoinColumn(name="topic_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     * )
     */
    private $tags;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="likedTopics", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="topic_likes_index",
     *      joinColumns={@ORM\JoinColumn(name="topic_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $likes;

    /**
     * @ORM\ManyToMany(targetEntity="User", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="topic_rating_positive_index",
     *      joinColumns={@ORM\JoinColumn(name="topic_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $positiveRating;

    /**
     * @ORM\ManyToMany(targetEntity="User", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinTable(name="topic_rating_negative_index",
     *      joinColumns={@ORM\JoinColumn(name="topic_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $negativeRating;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    public function __toString()
    {
        return $this->getTitle() ? $this->getTitle() : '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Topic
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->likes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->positiveRating = new \Doctrine\Common\Collections\ArrayCollection();
        $this->negativeRating = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Topic
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set ratingTotal
     *
     * @param integer $ratingTotal
     * @return Topic
     */
    public function setRatingTotal($ratingTotal)
    {
        $this->ratingTotal = $ratingTotal;

        return $this;
    }

    /**
     * Get ratingTotal
     *
     * @return integer
     */
    public function getRatingTotal()
    {
        return $this->ratingTotal;
    }

    /**
     * Set likeTotal
     *
     * @param integer $likeTotal
     * @return Topic
     */
    public function setLikeTotal($likeTotal)
    {
        $this->likeTotal = $likeTotal;

        return $this;
    }

    /**
     * Get likeTotal
     *
     * @return integer
     */
    public function getLikeTotal()
    {
        return $this->likeTotal;
    }

    /**
     * Set image
     *
     * @param array $image
     * @return Topic
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return array
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Topic
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Topic
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set section
     *
     * @param \AppBundle\Entity\TopicSection $section
     * @return Topic
     */
    public function setSection(\AppBundle\Entity\TopicSection $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \AppBundle\Entity\TopicSection
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Add tags
     *
     * @param \AppBundle\Entity\TopicTag $tags
     * @return Topic
     */
    public function addTag(\AppBundle\Entity\TopicTag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \AppBundle\Entity\TopicTag $tags
     */
    public function removeTag(\AppBundle\Entity\TopicTag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add likes
     *
     * @param \AppBundle\Entity\User $likes
     * @return Topic
     */
    public function addLike(\AppBundle\Entity\User $likes)
    {
        $this->likes[] = $likes;

        return $this;
    }

    /**
     * Remove likes
     *
     * @param \AppBundle\Entity\User $likes
     */
    public function removeLike(\AppBundle\Entity\User $likes)
    {
        $this->likes->removeElement($likes);
    }

    /**
     * Get likes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * Add positiveRating
     *
     * @param \AppBundle\Entity\User $positiveRating
     * @return Topic
     */
    public function addPositiveRating(\AppBundle\Entity\User $positiveRating)
    {
        $this->positiveRating[] = $positiveRating;

        return $this;
    }

    /**
     * Remove positiveRating
     *
     * @param \AppBundle\Entity\User $positiveRating
     */
    public function removePositiveRating(\AppBundle\Entity\User $positiveRating)
    {
        $this->positiveRating->removeElement($positiveRating);
    }

    /**
     * Get positiveRating
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPositiveRating()
    {
        return $this->positiveRating;
    }

    /**
     * Add negativeRating
     *
     * @param \AppBundle\Entity\User $negativeRating
     * @return Topic
     */
    public function addNegativeRating(\AppBundle\Entity\User $negativeRating)
    {
        $this->negativeRating[] = $negativeRating;

        return $this;
    }

    /**
     * Remove negativeRating
     *
     * @param \AppBundle\Entity\User $negativeRating
     */
    public function removeNegativeRating(\AppBundle\Entity\User $negativeRating)
    {
        $this->negativeRating->removeElement($negativeRating);
    }

    /**
     * Get negativeRating
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNegativeRating()
    {
        return $this->negativeRating;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return Topic
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     * @return Topic
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function updateLikeCount()
    {
        $count = count($this->likes);
        $this->setLikeTotal($count);
    }

    public function updateRatingCount()
    {
        $positive = count($this->positiveRating);
        $negative = count($this->negativeRating);

        $total    = $positive - $negative;

        $this->setRatingTotal($total);
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     */
    public function prePersist()
    {
        $this->updateLikeCount();
        $this->updateRatingCount();
    }

    public function getSectionExist()
    {
        if ($this->getSection()) {
            return true;
        }
        return false;
    }

}
