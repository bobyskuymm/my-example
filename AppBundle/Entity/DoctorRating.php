<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DoctorRating
 *
 * @ORM\Table(name="doctor_ratings")
 * @ORM\Entity
 */
class DoctorRating
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="punctuality", type="integer", nullable=true)
     */
    private $punctuality;

    /**
     * @var integer
     *
     * @ORM\Column(name="attention", type="integer", nullable=true)
     */
    private $attention;

    /**
     * @var integer
     *
     * @ORM\Column(name="qualification", type="integer", nullable=true)
     */
    private $qualification;

    /**
     * @var integer
     *
     * @ORM\Column(name="pricequality", type="integer", nullable=true)
     */
    private $pricequality;

    /**
     * @ORM\OneToOne(targetEntity="DoctorComment", mappedBy="rating")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $comment;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set punctuality
     *
     * @param integer $punctuality
     * @return DoctorRating
     */
    public function setPunctuality($punctuality)
    {
        $this->punctuality = $punctuality;

        return $this;
    }

    /**
     * Get punctuality
     *
     * @return integer
     */
    public function getPunctuality()
    {
        return $this->punctuality;
    }

    /**
     * Set attention
     *
     * @param integer $attention
     * @return DoctorRating
     */
    public function setAttention($attention)
    {
        $this->attention = $attention;

        return $this;
    }

    /**
     * Get attention
     *
     * @return integer
     */
    public function getAttention()
    {
        return $this->attention;
    }

    /**
     * Set qualification
     *
     * @param integer $qualification
     * @return DoctorRating
     */
    public function setQualification($qualification)
    {
        $this->qualification = $qualification;

        return $this;
    }

    /**
     * Get qualification
     *
     * @return integer
     */
    public function getQualification()
    {
        return $this->qualification;
    }

    /**
     * Set pricequality
     *
     * @param integer $pricequality
     * @return DoctorRating
     */
    public function setPricequality($pricequality)
    {
        $this->pricequality = $pricequality;

        return $this;
    }

    /**
     * Get pricequality
     *
     * @return integer
     */
    public function getPricequality()
    {
        return $this->pricequality;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return DoctorRating
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return DoctorRating
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set comment
     *
     * @param \AppBundle\Entity\DoctorComment $comment
     * @return DoctorRating
     */
    public function setComment(\AppBundle\Entity\DoctorComment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \AppBundle\Entity\DoctorComment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return DoctorRating
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     * @return DoctorRating
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function getTotalRating()
    {
        return $this->punctuality +
               $this->qualification +
               $this->attention +
               $this->pricequality;
    }

    public function getCalculatedRating()
    {
        return $this->getTotalRating() / 4;
    }

}
