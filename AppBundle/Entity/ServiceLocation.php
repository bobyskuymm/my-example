<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * @FileStore\Uploadable
 * @ORM\Table(name="service_locations")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ServiceLocationRepository")
 * @Gedmo\TranslationEntity(class="ServiceLocationTranslation")
 */
class ServiceLocation extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=512, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(
     *   targetEntity="ServiceLocationTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="ServiceAdditional", mappedBy="location")
     */
    protected $additional;

    /**
     * @ORM\OneToMany(targetEntity="ServiceDictionary", mappedBy="location")
     */
    protected $dictionary;


    /**
     * @var array
     *
     * @FileStore\UploadableField(mapping="image")
     *
     * @ORM\Column(name="image", type="array", nullable=true)
     */
    protected $image;


    /**
     * @ORM\OneToMany(targetEntity="ServicePackage", mappedBy="location")
     */
    protected $package;


    public function __toString()
    {
        if ($this->getName()) {
            return $this->getName();
        }
        return "";
    }


    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Clinic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\ClinicTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\ClinicTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }


    /**
     * Add city
     *
     * @param \AppBundle\Entity\Clinic $city
     * @return City
     */
    public function addCity(\AppBundle\Entity\Clinic $city)
    {
        $this->city[] = $city;

        return $this;
    }

    /**
     * Remove city
     *
     * @param \AppBundle\Entity\Clinic $city
     */
    public function removeCity(\AppBundle\Entity\Clinic $city)
    {
        $this->city->removeElement($city);
    }

    /**
     * Get city
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add additional
     *
     * @param \AppBundle\Entity\ServiceAdditional $additional
     * @return ServiceLocation
     */
    public function addAdditional(\AppBundle\Entity\ServiceAdditional $additional)
    {
        $this->additional[] = $additional;

        return $this;
    }

    /**
     * Remove additional
     *
     * @param \AppBundle\Entity\ServiceAdditional $additional
     */
    public function removeAdditional(\AppBundle\Entity\ServiceAdditional $additional)
    {
        $this->additional->removeElement($additional);
    }

    /**
     * Get additional
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * Add dictionary
     *
     * @param \AppBundle\Entity\ServiceDictionary $dictionary
     * @return ServiceLocation
     */
    public function addDictionary(\AppBundle\Entity\ServiceDictionary $dictionary)
    {
        $this->dictionary[] = $dictionary;

        return $this;
    }

    /**
     * Remove dictionary
     *
     * @param \AppBundle\Entity\ServiceDictionary $dictionary
     */
    public function removeDictionary(\AppBundle\Entity\ServiceDictionary $dictionary)
    {
        $this->dictionary->removeElement($dictionary);
    }

    /**
     * Get dictionary
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDictionary()
    {
        return $this->dictionary;
    }

    /**
     * Add package
     *
     * @param \AppBundle\Entity\ServicePackage $package
     * @return ServiceLocation
     */
    public function addPackage(\AppBundle\Entity\ServicePackage $package)
    {
        $this->package[] = $package;

        return $this;
    }

    /**
     * Remove package
     *
     * @param \AppBundle\Entity\ServicePackage $package
     */
    public function removePackage(\AppBundle\Entity\ServicePackage $package)
    {
        $this->package->removeElement($package);
    }

    /**
     * Get package
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * Set image
     *
     * @param array $image
     * @return ServiceLocation
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return array 
     */
    public function getImage()
    {
        return $this->image;
    }
}
