<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * DoctorLanguage
 *
 * @ORM\Table(name="doctor_languages")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="DoctorLanguageTranslation")
 */
class DoctorLanguage extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="Doctor", mappedBy="languages")
     * @ORM\JoinTable(name="doctor_languages_index",
     *      joinColumns={@ORM\JoinColumn(name="language_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="doctor_id", referencedColumnName="id")}
     * )
     */
    private $doctors;

    /**
     * @ORM\OneToMany(
     *   targetEntity="DoctorLanguageTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    public function __toString()
    {
        return $this->getName() ? $this->getName() : '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DoctorLanguage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->doctors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add doctors
     *
     * @param \AppBundle\Entity\Doctor $doctors
     * @return DoctorLanguage
     */
    public function addDoctor(\AppBundle\Entity\Doctor $doctors)
    {
        $this->doctors[] = $doctors;

        return $this;
    }

    /**
     * Remove doctors
     *
     * @param \AppBundle\Entity\Doctor $doctors
     */
    public function removeDoctor(\AppBundle\Entity\Doctor $doctors)
    {
        $this->doctors->removeElement($doctors);
    }

    /**
     * Get doctors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoctors()
    {
        return $this->doctors;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\DoctorLanguageTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\DoctorLanguageTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }
}
