<?php

namespace AppBundle\Entity;

/**
 * ClinicRepository
 *
 */
class ClinicRepository extends \Doctrine\ORM\EntityRepository
{

    public function getMinPrice()
    {
        $q = $this->createQueryBuilder('c');

        $q->where('c.price > 0')
            ->andWhere('c.hide = 0')
            ->andWhere('c.moderated = 1')
            ->orderBy('c.price','ASC')
            ->setMaxResults(1)
        ;

        $clinics  = $q->getQuery()->getResult();
        return $clinics[0];
    }

    public function getByFilter($filter = NULL)
    {

        $clinics =
            $this->createQueryBuilder("Clinic")
                 ->select("Clinic", "Location.name AS HIDDEN location,
                                     Clinic.price AS HIDDEN price,
                                     Clinic.ratingTotal AS HIDDEN rating
                                     ")
                 ->leftJoin("Clinic.location", "Location")
                 ->leftJoin("Clinic.personnel", "Doctor")
        ;

        if (!empty($filter['doctor'])) {
            $clinics = $clinics
                ->andWhere("Doctor.id = :doctor")
                ->setParameter(":doctor", $filter['doctor']);
        }

        if (!empty($filter['city'])) {
            $clinics = $clinics
                ->andWhere("Clinic.city = :city")
                ->setParameter(":city", $filter['city']);
        }

        if (!empty($filter['location'])) {
            $clinics = $clinics
                ->andWhere("Clinic.area = :location")
                ->setParameter(":location", $filter['location']);
        }

        if (!empty($filter['price'])) {
            $clinics = $clinics
                ->andWhere("Clinic.price >= :price_min AND Clinic.price <= :price_max")
                ->setParameter(":price_min", $filter['price'][0])
                ->setParameter(":price_max", $filter['price'][1])
            ;
        }

        if (!empty($filter['locale']) && $filter['locale'] == 'en') {
            $clinics = $clinics
                ->andWhere("Clinic.showEnglish = 1");
        }

        $clinics = $clinics
            ->andWhere('Clinic.hide != 1')
            ->andWhere('Clinic.moderated = 1')
            ->andWhere("Clinic.url != ''")
            ->groupBy('Clinic.id')
        ;

        $clinics = $clinics->getQuery();

        // dump($clinics); dump($filter);
        // die();

        return $clinics;
    }




}
