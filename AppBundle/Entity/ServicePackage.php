<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * @FileStore\Uploadable
 * @ORM\Table(name="service_package")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ServicePackageRepository")
 * @Gedmo\TranslationEntity(class="ServicePackageTranslation")
 */
class ServicePackage extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string" )
     */
    private $name;

    /**
     * @var array
     *
     * @FileStore\UploadableField(mapping="image")
     *
     * @ORM\Column(name="image", type="array", nullable=true)
     */
    protected $image;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer" )
     */
    private $price;


    /**
     * @ORM\ManyToMany(targetEntity="ServiceDictionary", inversedBy="services", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="service_options_index",
     *      joinColumns={@ORM\JoinColumn(name="package_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="options_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $options;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="ServiceLocation", inversedBy="package")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    protected $location;

    /**
     * @ORM\Column(name="enabled", type="boolean")
     * @var boolean
     */
    protected $enabled;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;


    /**
     * @ORM\OneToMany(
     *   targetEntity="ServicePackageTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    
    public function __toString()
    {
        if ($this->getName()) {
            return $this->getName();
        }
        return "";
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->enabled = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }





    /**
     * Set name
     *
     * @param string $name
     * @return ServiceAdditional
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }



    /**
     * Set price
     *
     * @param integer $price
     * @return ServiceAdditional
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\ServiceLocation $location
     * @return ServiceAdditional
     */
    public function setLocation(\AppBundle\Entity\ServiceLocation $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\ServiceLocation 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Remove translations
     *
     * @param \AppBundle\Entity\ServiceAdditionalTranslation $translations
     */
    public function removeTranslation(\AppBundle\Entity\ServiceAdditionalTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }


    

    /**
     * Add options
     *
     * @param \AppBundle\Entity\ServiceDictionary $options
     * @return ServicePackage
     */
    public function addOption(\AppBundle\Entity\ServiceDictionary $options)
    {
        $this->options[] = $options;

        return $this;
    }

    /**
     * Has options
     *
     * @param \AppBundle\Entity\ServiceDictionary $options
     * @return ServicePackage
     */
    public function hasOption(\AppBundle\Entity\ServiceDictionary $options)
    {
        return $this->options->contains($options);
    }

    /**
     * Remove options
     *
     * @param \AppBundle\Entity\ServiceDictionary $options
     */
    public function removeOption(\AppBundle\Entity\ServiceDictionary $options)
    {
        $this->options->removeElement($options);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return ServicePackage
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return ServicePackage
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set image
     *
     * @param array $image
     * @return ServicePackage
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return array 
     */
    public function getImage()
    {
        return $this->image;
    }
}
