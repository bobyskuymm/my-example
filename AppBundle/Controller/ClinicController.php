<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Area;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Knp\Component\Pager\Event\Subscriber\Sortable\Doctrine\ORM\Query\OrderByWalker;

use AppBundle\Entity\Clinic;
use AppBundle\Entity\ClinicComment;
use AppBundle\Entity\ClinicPhoto;

use AppBundle\Form\ClinicType;


class ClinicController extends Controller
{

    /**
     * @Route("/profile", name="clinic_profile")
     * @Template()
     * @Security("is_granted('ROLE_USER')")
     */
    public function profileAction(Request $request)
    {

        $user = $this->getUser();

        $clinic = $user->getClinic();

        if (!$user->getAccountType() || $user->getAccountType()->getId() != 2) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();

        if (!$clinic) {

            $clinic = new Clinic();

            $user->setClinic($clinic);
            $clinic->setUser($user);

            $em->persist($clinic, $user);
            $em->flush();

        }

        $lng = $request->query->get('lng');

        if ($lng && $lng == 'en') {
            $clinic->setLocale('en');
            foreach ($clinic->getServices() as &$item) {
                $item->setLocale('en');
                $em->refresh($item);
            }
            $em->refresh($clinic);
        } else {
            $clinic->setLocale('ru');
            foreach ($clinic->getServices() as &$item) {
                $item->setLocale('ru');
                $em->refresh($item);
            }
            $em->refresh($clinic);
        }

        $form = $this->createForm(new ClinicType(), $clinic);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($clinic->getServices() as $item) {
                if ($item) {
                    $item->setClinic($clinic);
                    if ($lng && $lng == 'en') {
                        $item->setLocale('en');
                    } else {
                        $item->setLocale('ru');
                    }
                }
            }

            foreach ($clinic->getDocuments() as $item) {
                if ($item) $item->setClinic($clinic);
            }

            $clinic->setModerated(false);

            $em->persist($clinic);
            $em->flush();

            return $this->redirect($this->generateUrl('clinic_profile'));

        }

        return [
            'clinic' => $clinic,
            'form'   => $form->createView(),
        ];

    }

    /**
     * @Route("/profile/applications", name="clinic_profile_applications")
     * @Template()
     * @Security("is_granted('ROLE_USER')")
     */
    public function profileApplicationsAction(Request $request)
    {

        $user = $this->getUser();

        if (!$user->getAccountType() || $user->getAccountType()->getId() != 2) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $clinic = $user->getClinic();

        $applications = $clinic->getExcursionApplications();


        return [
            'applications' => $applications,
        ];

    }

    /**
     * @Route("/photo_upload", name="clinic_photo_upload")
     * @Security("is_granted('ROLE_USER')")
     */
    public function photoUploadAction(Request $request)
    {

        $clinic = $this->getUser()->getClinic();

        if (!$clinic) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();

        $return = [];

        foreach ($request->files->all() as $file) {

            $file = array_pop($file);

            $photo = new ClinicPhoto();
            $photo->setClinic($clinic);
            $photo->setPhoto($file);

            $clinic->addPhoto($photo);

            $clinic->setModerated(false);

            $em->persist($photo, $clinic);
            $em->flush();

            $photoData = $photo->getPhoto();

            $return[] = [
                'name' => $photoData['fileName'],
                'size' => $photoData['size'],
                'url'  => $photoData['path'],
                'thumbnailUrl' => $this->get('liip_imagine.cache.manager')->getBrowserPath($photoData['path'], 'clinic_profile_photo_main'),
                'deleteUrl' => $photoData['path'],
                'deleteType' => 'DELETE',
            ];

        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/show/{url}", name="clinic_show")
     * @Template()
     */
    public function showAction(Request $request, $url)
    {

        $clinic = $this->getDoctrine()->getRepository('AppBundle:Clinic')
                  ->findOneByUrl($url);

        // review save
        $r = $this->get('app.clinic_comment')->getCommentForm();
        if ($r instanceof RedirectResponse) {
            return $r;
        }
        //

        if (!$clinic) {
            throw $this->createNotFoundException($this->get('translator')->trans('app.exceptions.page_not_found'));
        }

        return [
            'clinic' => $clinic,
        ];

    }

    /**
     * @Route("/like/{comment}", name="clinic_comment_add_like", requirements={ "comment" = "\d+" })
     * @Security("is_granted('ROLE_USER')")
     */
    public function addLikeAction(Request $request, DoctorComment $comment)
    {

        $already = false;

        foreach ($comment->getLikes() as $item) {
            if ($item == $this->getUser()) {
                $already = true;
            }
        }

        if ($already) {
            return new JsonResponse([
                'error' => $this->get('translator')->trans('app.doctor_comment.likes.error.already_set_by_user'),
                'status' => 0,
            ]);
        }

        $comment->addLike($this->getUser());

        $this->getDoctrine()->getManager()->persist($comment);
        $this->getDoctrine()->getManager()->flush();


        return new JsonResponse([
            'status' => 1,
            'likes' => $comment->getLikeTotal(),
        ]);

    }

    /**
     * @Route("/favorite/{clinic}", name="clinic_add_favorite", requirements={ "clinic" = "\d+" })
     * @Security("is_granted('ROLE_USER')")
     */
    public function addToFavoriteAction(Request $request, Clinic $clinic)
    {

        $user = $this->getUser();

        $user->addFavoriteClinic($clinic);
        $clinic->addFavoriteBy($user);

        $this->getDoctrine()->getManager()->persist($user, $clinic);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'status' => 1,
        ]);

    }

    /**
     * @Route("/unfavor/{clinic}", name="clinic_remove_favorite", requirements={ "clinic" = "\d+" })
     * @Security("is_granted('ROLE_USER')")
     */
    public function removeFromFavoriteAction(Request $request, Clinic $clinic)
    {

        $user = $this->getUser();

        $user->removeFavoriteClinic($clinic);
        $clinic->removeFavoriteBy($user);

        $this->getDoctrine()->getManager()->persist($user, $clinic);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'status' => 1,
        ]);

    }



    /**
     * @Route("/area/city-list", name="area_city_list")
     * @Template()
     */
    public function cityAction(Request $request)
    {
        $em = $this->getDoctrine();

        /**
         * @var Area $area
         */
        $area = $em->getRepository('AppBundle:Area')->find($request->request->get('area_id'));

        $cities = $area->getCities();

        return ['cities' => $cities ];
    }

    /**
     * @Route("/", name="clinic_index")
     * @Route("/{page}", name="clinic_index_paged")
     * @Template()
     */
    public function indexAction(Request $request, $page = 1)
    {

        $limitPerPage = 20;

        if ($request->cookies->has('CLINICS_QUA')) {
            $limitPerPage = $request->cookies->get('CLINICS_QUA');
        }

        $locations = $this->getDoctrine()->getRepository('AppBundle:DoctorLocation')->findAll();
        $doctors   = $this->getDoctrine()->getRepository('AppBundle:Doctor')->findAll();

        $cities    = $this->getDoctrine()->getRepository('AppBundle:City')->findAll();

        $areas    = $this->getDoctrine()->getRepository('AppBundle:Area')->findAll();

        $priceMin  = $this->getDoctrine()->getRepository('AppBundle:Clinic')->getMinPrice();
        $priceMin  = ($priceMin) ? $priceMin->getPrice() : 0;
        $priceMax  = $this->getDoctrine()->getRepository('AppBundle:Clinic')->findOneBy(['hide' => false, 'moderated' => true], ['price' => 'DESC']);
        $priceMax  = ($priceMax) ? $priceMax->getPrice() : 0;



        $priceMax  = ceil($priceMax / 10) * 10;

        $filter = [
            'price'     => ($request->query->has('price') && $request->query->get('price') != '') ? explode(":", $request->query->get('price')) : null,
            'doctor'    =>  $request->query->get('doctor'),
            'location'  =>  $request->query->get('location'),
            'city'  =>  $request->query->get('city'),
            'locale'    =>  $request->getLocale(),
        ];


        if(!empty($filter['location'])) {
            /**  @var Area $area */
            $area = $this->getDoctrine()->getRepository('AppBundle:Area')->find($filter['location']);
            $cities = $area->getCities();
        }


        $clinicsQuery = $this->getDoctrine()->getRepository('AppBundle:Clinic')->getByFilter($filter);

        $paginator  = $this->get('knp_paginator');
        $clinics    = $paginator->paginate(
            $clinicsQuery,
            $page,
            $limitPerPage
        );

        $clinics->setTemplate('AppBundle:Clinic:pagination.html.twig');
        $clinics->setSortableTemplate('AppBundle:Doctor:sortable_link.html.twig');

        $clinics->setUsedRoute("clinic_index_paged");

        return [
            'locations'    => $locations,
            'clinics'      => $clinics,
            'doctors'      => $doctors,
            'cities'      => $cities,
            'areas'      => $areas,
            'priceMin'     => $priceMin,
            'priceMax'     => $priceMax,
            'filter'       => $filter,
            'limitPerPage' => $limitPerPage,
        ];

    }

}
