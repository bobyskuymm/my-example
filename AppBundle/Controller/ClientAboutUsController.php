<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use AppBundle\Entity\ClientAboutUs;
use AppBundle\Form\ClientAboutUsType;


class ClientAboutUsController extends Controller
{

    /**
     * @Route("/", name="client_review_index")
     * @Route("/{page}", name="client_review_index_paged", requirements={ "page" = "\d+" })
     * @Template()
     */
    public function indexAction(Request $request, $page = 1)
    {

        $limitPerPage = 20;

        $reviewsQuery = $this->getDoctrine()->getRepository('AppBundle:ClientAboutUs')->findBy(['hide' => false], ['createdAt' => 'DESC']);

        $paginator  = $this->get('knp_paginator');
        $reviews = $paginator->paginate(
            $reviewsQuery,
            $page,
            $limitPerPage
        );

        $reviews->setTemplate('AppBundle:ClientAboutUs:pagination.html.twig');
        $reviews->setUsedRoute('client_review_index_paged');


        return [
            'reviews' => $reviews,
        ];

    }

    /**
     * @Route("/save_review", name="client_review_save")
     * @Template()
     */
    public function saveAction(Request $request)
    {

        $review = new ClientAboutUs();

        $form = $this->container->get('form.factory')->create(new ClientAboutUsType($this->container), $review);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->persist($review);
            $this->getDoctrine()->getManager()->flush();

            $adminEmail = $this->getParameter('admin_email');

            if ($adminEmail) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('New client review')
                    ->setFrom($adminEmail)
                    ->setTo($adminEmail)
                    ->setBody(
                        $this->renderView(
                            'AppBundle:ClientAboutUs:newReviewEmail.html.twig',
                            [
                                'review' => $review,
                            ]
                        ),
                        'text/html'
                    )
                ;

                $this->get('mailer')->send($message);
            }

            return [];
        }

        return [
            'status' => 'error',
        ];

    }

    /**
     * @Route("/{url}", name="client_review_show")
     * @Template()
     */
    public function showAction(Request $request, $url)
    {

        $item = $this->getDoctrine()->getRepository('AppBundle:ClientAboutUs')
                ->findOneByUrl($url);

        if (!$item) {
            throw $this->createNotFoundException($this->get('translator')->trans('app.exceptions.page_not_found'));
        }

        return [
            'item' => $item,
        ];

    }

}
