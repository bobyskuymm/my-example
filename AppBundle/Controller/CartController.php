<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Area;
use AppBundle\Entity\CartOrder;
use AppBundle\Entity\ServicePackage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Knp\Component\Pager\Event\Subscriber\Sortable\Doctrine\ORM\Query\OrderByWalker;

use AppBundle\Entity\Clinic;
use AppBundle\Entity\ClinicComment;
use AppBundle\Entity\ClinicPhoto;

use AppBundle\Form\ClinicType;



class CartController extends Controller
{
    public  static $cartKey = 'cart';

    /**
     * @Route("/remove/{cartId}", name="cart_remove")
     *
     * @param Request $request
     * @param string  $cartId
     *
     * @Template()
     * @Security("is_granted('ROLE_USER')")
     *
     * @return array
     */
    public function removeAction(Request $request, $cartId)
    {
        $cart = $request->getSession()->get(self::$cartKey);
        if (!empty($cart[$cartId])) {
            unset($cart[$cartId]);
        }
        $request->getSession()->set(self::$cartKey, $cart);

        return $this->redirectToRoute('cart_index_page');
    }


    /**
     * @Route("/add/{serviceId}/{type}", name="cart_add")
     *
     * @param Request $request
     * @param integer $serviceId
     * @param integer $type
     *
     * @Template()
     * @Security("is_granted('ROLE_USER')")
     *
     * @return array
     */
    public function addAction(Request $request, $serviceId, $type)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface */
        $session = $request->getSession();

        $key = 'cart_'.$serviceId . '_' . $type;
        $cartItems = $session->get(self::$cartKey,[]);

        if( empty($cartItems[$key] ) ) {
             $cartItems[$key] = [
                'serviceId' =>$serviceId,
                'type' => $type,
                'key' => $key,
                'count' => 1,
            ];
        }else{
            $cartItems[$key] = [
                'serviceId' =>$serviceId,
                'key' => $key,
                'type' => $type,
                'count' => $cartItems[$key]['count'] + 1
            ];
        }

        $session->set(self::$cartKey, $cartItems);
        $service = [];

        if( $type == 1 ) {
            $service = $em->getRepository('AppBundle:ServicePackage')->find($serviceId);
        }

        if( $type == 2 ) {
            $service = $em->getRepository('AppBundle:ServiceAdditional')->find($serviceId);
        }

        return $this->redirectToRoute('app_services',['type' => $type, 'location' => $service->getLocation()->getId() ]);

    }

    /**
     * @Route("/cart", name="cart_index_page")
     *
     * @param Request $request
     *
     * @Template()
     *
     * @return array
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cart = $request->getSession()->get(self::$cartKey, []);

        $cartItems = [];
        $totalPrice = 0;


        if ($request->isMethod('post')) {
//            dump($request->request);

            foreach($cart as $k => $v) {
                $cart[$k]['count'] = (int)$request->request->get($v['key']);
            }

//            dump($cart);die;

            $request->getSession()->set(self::$cartKey, $cart);

            if ( $request->request->get('checkout-flag') == 1){
                return $this->redirectToRoute('cart_checkout_page');
            }


        }


        foreach($cart as $c) {

            $service = null;

            if ($c['type'] == 1) {
                $service = $em->getRepository('AppBundle:ServicePackage')->find($c['serviceId']);
            }
            if ($c['type'] == 2) {
                $service = $em->getRepository('AppBundle:ServiceAdditional')->find($c['serviceId']);
            }

            $c['service'] = $service;
            $totalPrice = $totalPrice + ($service->getPrice() * $c['count']);
            $cartItems[] = $c;
        }


        $showModal = $request->getSession()->get('show_modal',false);
        $request->getSession()->set('show_modal',false);

        return [
            'cart_items' => $cartItems,
            'show_modal' => $showModal,
            'total_price' => $totalPrice,
            'user' => $this->getUser()
        ];
    }


    /**
     * @Route("/cart/checkout", name="cart_checkout_page")
     *
     * @param Request $request
     *
     * @Template()
     *
     * @return array
     */
    public function checkoutAction(Request $request)
    {

        $user = $this->getUser();

        if( $user->getPhones()->count() == 0  || $user->getEmail() == '' || ( $user->getFirstName() == '' && $user->getLastName() == '') ) {
            $request->getSession()->set('show_modal',2);
            return $this->redirectToRoute('cart_index_page');
        }

        $em = $this->getDoctrine()->getManager();
        $cart = $request->getSession()->get(self::$cartKey,[]);

        $cartItems = [];
        $totalPrice = 0;



        foreach($cart as $c) {

            $service = null;

            if ($c['type'] == 1) {
                $service = $em->getRepository('AppBundle:ServicePackage')->find($c['serviceId']);
            }
            if ($c['type'] == 2) {
                $service = $em->getRepository('AppBundle:ServiceAdditional')->find($c['serviceId']);
            }

            $totalPrice = $totalPrice + ($service->getPrice() * $c['count']);
            $cartItems[] = $c;
        }

        $cartOrder = new CartOrder();
        $cartOrder->setUser($this->getUser())
            ->setTotalSum($totalPrice)
            ->setCartOrder(serialize($cartItems))
            ->setCreatedAt(new \DateTime())
        ;

        $em->persist($cartOrder);
        $em->flush();

        $request->getSession()->set(self::$cartKey,[]);
        $request->getSession()->set('show_modal',1);


        return $this->redirectToRoute('cart_index_page');
    }

}
