<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use AppBundle\Form\AccountTypeFormType;
use AppBundle\Form\UserType;

use AppBundle\Entity\AccountType;
use AppBundle\Entity\User;
use AppBundle\Entity\UserPhone;


class AccountController extends Controller
{
    /**
     * @Route("/set_type", name="account_set_type")
     * @Template()
     */
    public function setTypeAction(Request $request)
    {

        $user = $this->getUser();

        $form = $this->createForm(new AccountTypeFormType($this->container), $user);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

        }

        return $this->redirect($this->generateUrl('homepage'));

    }

    /**
     * @Route("/set_type/{type}", name="account_set_type_exact")
     * @Template()
     * @Security("is_granted('ROLE_USER')")
     */
    public function setTypeExactAction(Request $request, AccountType $type)
    {

        $user = $this->getUser();

        if ($user->getAccountType()) {
            // throw new AccessDeniedException('This user already have account type.');
        }

        $user->setAccountType($type);

        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->generateUrl('homepage'));

    }

    /**
     * @Route("/favorite/{account}", name="user_add_favorite", requirements={ "account" = "\d+" })
     * @Security("is_granted('ROLE_USER')")
     */
    public function addToFavoriteAction(Request $request, User $account)
    {

        $user = $this->getUser();

        $user->addFavoriteUser($account);
        $account->addFavoriteBy($user);

        $this->getDoctrine()->getManager()->persist($user, $account);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'status' => 1,
        ]);

    }

    /**
     * @Route("/unfavor/{account}", name="user_remove_favorite", requirements={ "account" = "\d+" })
     * @Security("is_granted('ROLE_USER')")
     */
    public function removeFromFavoriteAction(Request $request, User $account)
    {

        $user = $this->getUser();

        $user->removeFavoriteUser($account);
        $account->removeFavoriteBy($user);

        $this->getDoctrine()->getManager()->persist($user, $account);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'status' => 1,
        ]);

    }

    /**
     * @Route("/rating/up/{account}", name="user_increase_rating", requirements={ "account" = "\d+" })
     * @Security("is_granted('ROLE_USER')")
     */
    public function increaseRatingAction(Request $request, User $account)
    {

        $already = false;

        foreach ($account->getPositiveRating() as $item) {
            if ($item == $this->getUser()) {
                $already = true;
            }
        }

        foreach ($account->getNegativeRating() as $item) {
            if ($item == $this->getUser()) {
                $already = true;
            }
        }

        if ($already) {
            return new JsonResponse([
                'error' => $this->get('translator')->trans('app.user.rating.error.already_set_by_user'),
                'status' => 0,
            ]);
        }

        $account->addPositiveRating($this->getUser());

        $this->getDoctrine()->getManager()->persist($account);
        $this->getDoctrine()->getManager()->flush();


        return new JsonResponse([
            'status' => 1,
            'rating' => (($account->getRatingTotal() > 0) ? '+' : '') . $account->getRatingTotal(),
        ]);

    }

    /**
     * @Route("/rating/down/{account}", name="user_decrease_rating", requirements={ "account" = "\d+" })
     * @Security("is_granted('ROLE_USER')")
     */
    public function decreaseRatingAction(Request $request, User $account)
    {

        $already = false;

        foreach ($account->getPositiveRating() as $item) {
            if ($item == $this->getUser()) {
                $already = true;
            }
        }

        foreach ($account->getNegativeRating() as $item) {
            if ($item == $this->getUser()) {
                $already = true;
            }
        }

        if ($already) {
            return new JsonResponse([
                'error' => $this->get('translator')->trans('app.account.rating.error.already_set_by_user'),
                'status' => 0,
            ]);
        }

        $account->addNegativeRating($this->getUser());

        $this->getDoctrine()->getManager()->persist($account);
        $this->getDoctrine()->getManager()->flush();


        return new JsonResponse([
            'status' => 1,
            'rating' => (($account->getRatingTotal() > 0) ? '+' : '') . $account->getRatingTotal(),
        ]);

    }

    /**
     * @Route("/show/{user}", name="account_show")
     * @Template()
     */
    public function showAction(Request $request, User $user)
    {

        // $em = $this->getDoctrine()->getManager();
        // $lng = $request->getLocale();

        // if ($lng && $lng == 'en') {
        //     $user->setLocale('en');
        //     $em->refresh($user);
        // } else {
        //     $user->setLocale('ru');
        //     $em->refresh($user);
        // }

        // $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneById($user->getId());
        // $user->setLocale($request->getLocale());
        // $em->refresh($user);
        // dump($user->getTranslation('about', 'en')); die();

        return [
            'user' => $user,
        ];

    }

    /**
     * @Route("/edit", name="account_edit")
     * @Template()
     * @Security("is_granted('ROLE_USER')")
     */
    public function editAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $lng = $request->query->get('lng');

        if ($lng && $lng == 'en') {
            $user->setLocale('en');
            $em->refresh($user);
        } else {
            $user->setLocale('ru');
            $em->refresh($user);
        }

        $form = $this->createForm(new UserType(), $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('account_edit'));

        }

        return [
            'user' => $user,
            'form' => $form->createView(),
        ];

    }

    /**
     * @Route("/avatar_upload", name="account_avatar_upload")
     * @Security("is_granted('ROLE_USER')")
     */
    public function avatarUploadAction(Request $request)
    {

        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $return = [];

        // dump($request->files->all());
        // die();

        foreach ($request->files->all() as $file) {

            $file = array_pop($file);

            $user->setAvatar($file);

            $em->persist($user);
            $em->flush();

            $photoData = $user->getAvatar();

            $return[] = [
                'name' => $photoData['fileName'],
                'size' => $photoData['size'],
                'url'  => $photoData['path'],
                'thumbnailUrl' => $this->get('liip_imagine.cache.manager')->getBrowserPath($photoData['path'], 'user_profile_photo_main'),
                'deleteUrl' => $photoData['path'],
                'deleteType' => 'DELETE',
            ];

        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/verify_phone/{phone}/{userPhone}", name="account_verify_phone_step1")
     * @Security("is_granted('ROLE_USER')")
     */
    public function verifyPhoneActionStep1(Request $request, $phone = null, UserPhone $userPhone = null)
    {

        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        if (!$userPhone) {

            $userPhone = new UserPhone();
            $userPhone->setUser($user);
            $userPhone->setNumber($phone);
            $user->addPhone($userPhone);

            $em->persist($userPhone, $user);
            $em->flush();

        }

        $code = rand(1111, 9999);

        $userPhone->setVerifyCode($code);
        $userPhone->setVerifiedAt(null);
        $userPhone->setNumber($phone);

        $em->persist($userPhone);
        $em->flush();

        $return = [
            'status' => 'ok',
            'id'     => $userPhone->getId(),
        ];

        $smsResult = (array) $this->get('app.turboSMS')->sendSMS($userPhone->getNumber(), "Verification code: " . $code);

        if (!empty($smsResult['error'])) {
            $return['sms_status'] = 'error';
            $return['sms_error']  = $smsResult['error'];
            $return['code']       = $code;
        } else {
            $return['sms_status'] = 'ok';
        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/verify_phone/confirm/{userPhone}/{code}", name="account_verify_phone_step2")
     * @Security("is_granted('ROLE_USER')")
     */
    public function verifyPhoneActionStep2(Request $request, UserPhone $userPhone = null, $code)
    {

        $em = $this->getDoctrine()->getManager();

        if ($userPhone->getVerifyCode() == $code) {
            $userPhone->setVerifiedAt(new \DateTime());

            $em->persist($userPhone);
            $em->flush();

            return new JsonResponse([
                'status' => 'ok',
                'verified' => $userPhone->getVerifiedAt()->format('d.m.Y H:i:s'),
            ]);
        }

        return new JsonResponse([
            'status' => 'error',
            'code' => $code,
            // 'needed' => $userPhone->getVerifyCode(),
        ]);

    }

    /**
     * @Route("/topics/{user}/{page}", name="account_show_get_topics")
     * @Template()
     */
    public function ajaxGetUserTopicsAction(Request $request, User $user, $page = 1)
    {
        $limitPerPage = 9;

        $paginator  = $this->get('knp_paginator');

        $topics    = $paginator->paginate(
            $this->getDoctrine()->getRepository('AppBundle:Topic')->findBy(['createdBy' => $user], ['createdAt' => 'DESC']),
            $page,
            $limitPerPage
        );

        $topics->setTemplate('AppBundle:Account:pagination.html.twig');

        if ($topics->getTotalItemCount() == 0) {
            return new Response("no data");
        }

        return [
            'topics' => $topics,
            'page' => $page,
        ];

    }

    /**
     * @Route("/publications/{user}/{page}", name="account_show_get_publications")
     * @Template()
     */
    public function ajaxGetUserPublicationsAction(Request $request, User $user, $page = 1)
    {
        $limitPerPage = 6;

        $paginator  = $this->get('knp_paginator');

        $publications    = $paginator->paginate(
            $this->getDoctrine()->getRepository('AppBundle:PersonalExperience')->findBy(['createdBy' => $user], ['createdAt' => 'DESC']),
            $page,
            $limitPerPage
        );

        $publications->setTemplate('AppBundle:Account:pagination.html.twig');

        if ($publications->getTotalItemCount() == 0) {
            return new Response("no data");
        }

        return [
            'publications' => $publications,
            'page' => $page,
        ];

    }

}
