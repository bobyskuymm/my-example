<?php
namespace AppBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class FOSUBUserProvider
 * @package Econtech\AuthBundle\Security\Core\User
 */
class FOSUBUserProvider extends BaseClass
{

    protected $doctrine;

    protected $session;

    protected $router;

    protected $webDir;


    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {

        // dump($response);
        // die();
        $property = $this->getProperty($response);
        $username = $response->getUsername();

        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();

        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'Id';
        $setter_token = $setter.'AccessToken';

        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());

        $this->userManager->updateUser($user);

    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $email = $response->getEmail();


        $userTmp = array();
        $redirect = false;

        if ($email || $username) {
            $userTmp = $this->userManager->findUserBy(array('email' => $email));
            if (!$userTmp) {
                $userTmp = $this->userManager->findUserBy(array('email' => $username));
            }
        }

        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));
        if (($user && $user->getId() && $userTmp && $user->getId() != $userTmp->getId()) || ($userTmp && !$user)) {
            $redirect = true;
        }
        if ($redirect) {
            $this->session->getFlashBag()->set('warning', 'main.email_already_used');
            header('location: ' . $this->router->generate('homepage'));
            exit();
        }

        $service = $response->getResourceOwner()->getName();
        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'Id';
        $setter_token = $setter.'AccessToken';



        if (null === $user) {

            // create new user here
            $user = $this->userManager->createUser();

            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());

            $user->setUsername($response->getUsername());

            if ($response->getEmail()) {
                $user->setEmail($response->getEmail());
            } else {
                $user->setEmail($response->getUsername());
            }

            if ($response->getFirstName()) {
                $user->setFirstName($response->getFirstName());
            }

            if ($response->getLastName()) {
                $user->setLastName($response->getLastName());
            }

            $user->setPlainPassword($response->getUsername());
            $user->addRole('ROLE_USER');
            $user->setEnabled(true);

            $this->userManager->updateUser($user);

            if (!$user->getAvatar() && $response->getAvatar()) {
                $avatar = $this->downloadImage($response->getAvatar(), $response->getUsername());
                if ($avatar) {
                    $em = $this->getDoctrine()->getManager();
                    $sql = "UPDATE accounts SET avatar = '" . serialize($avatar) . "' WHERE id = " . $user->getId();
                    $connection = $em->getConnection();
                    $statement = $connection->prepare($sql);
                    $statement->execute();
                }
            }

            $this->userManager->updateUser($user);

            return $user;

        } else {

            if (!$user->getFirstName() && $response->getFirstName()) {
                $user->setFirstName($response->getFirstName());
            }

            if (!$user->getLastName() && $response->getLastName()) {
                $user->setLastName($response->getLastName());
            }

            $this->userManager->updateUser($user);
        }

        $user = parent::loadUserByOAuthUserResponse($response);

        $user->$setter_token($response->getAccessToken());

        return $user;
    }

    /**
     * @return mixed
     */
    public function getDoctrine() {
        return $this->doctrine;
    }

    /**
     * @param mixed $doctrine
     */
    public function setDoctrine($doctrine) {
        $this->doctrine = $doctrine;
    }

    /**
     * @param $session
     */
    public function setSession($session) {
        $this->session = $session;
    }

    /**
     * @param mixed $router
     */
    public function setRouter($router) {
        $this->router = $router;
    }

    public function setWebDir($dir) {
        $this->webDir = $dir;
    }

    private function downloadImage($path = null, $name = null) {

        $uploadPath = '/avatar/';
        if (!$path || !$name || !$uploadPath) return null;

        $explode = explode(".", $path);
        $end = end($explode);
        $nameImage = $name . "." . mb_strtolower($end);

        if (strstr('?', $nameImage)) {
            list($nameImage, $else) = explode("?", $nameImage);
        }

        $newFilename = $this->webDir . $uploadPath . $nameImage;

        $opts = array(
          'http'=>array(
            'method'=>"GET",
            'header'=>"Accept-language: en\r\n" .
                      "Cookie: foo=bar\r\n"
          )
        );

        $context = stream_context_create($opts);

        if (copy($path, $newFilename, $context)) {
            $info = getimagesize($newFilename);
            $size = filesize($newFilename);
            if ($info && $size) {
                $image = array(
                    'fileName' => "/" . $nameImage,
                    'originalName' => $nameImage,
                    'mimeType' => $info['mime'],
                    'size' => $size,
                    'path' => $uploadPath . $nameImage,
                    'width' => $info[0],
                    'height' => $info[1],
                );

                // dump($path); dump($image); die();

                return $image;
            }
            // dump($info); dump($size); die();
        }

        // dump('aaa!'); die();

        return null;
    }

}