<?php

namespace AppBundle\OAuth\Response;

use HWI\Bundle\OAuthBundle\OAuth\Response\PathUserResponse;

/**
 * Class VkontakteUserResponse
 * @package Econtech\AuthBundle\OAuth\Response
 */
class VkontakteUserResponse extends PathUserResponse
{

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        $response = $this->response['response'][0];
        return $response['uid'];
    }

    /**
     * {@inheritdoc}
     */
    public function getRealname()
    {
        $response = $this->response['response'][0];
        return $response['first_name'] . ' ' . $response['last_name'];
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->getValueForPath('password', false);
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return NULL;
    }
    /**
     * @return mixed
     */
    public function getAvatar() {
        return $this->getValueForPath('profilepicture', false);
    }

    /**
     * @return mixed
     */
    public function getGender() {
        return NULL;
    }

    /**
     * @return mixed
     */
    public function getFirstName() {
        $response = $this->response['response'][0];
        return $response['first_name'];
    }

    /**
     * @return mixed
     */
    public function getLastName() {
        $response = $this->response['response'][0];
        return $response['last_name'];
    }

}