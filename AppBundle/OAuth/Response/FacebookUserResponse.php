<?php

namespace AppBundle\OAuth\Response;

use HWI\Bundle\OAuthBundle\OAuth\Response\PathUserResponse;

/**
 * FacebookUserResponse
 *
 */
class FacebookUserResponse extends PathUserResponse
{

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->response['id'];
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->getValueForPath('password', false);
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        // dump($this->response);
        // die();
        $email = $this->response['email'];

        if (!$email) {
            $email = $this->response['id'] . '@facebook.com';
        }

        return $email;
    }

    /**
     * @return mixed
     */
    public function getAvatar() {
        $url = 'https://graph.facebook.com/' . $this->response['id'] . '/picture?type=large';
        stream_context_set_default(array(
            'http' => array(
                'method' => 'HEAD'
            )
        ));
        $headers = get_headers($url, 1);
        if ($headers && isset($headers['Location'])) {
            return $headers['Location'];
        }
        return $url;
    }

    /**
     * @return mixed
     */
    public function getFirstName() {
        // dump($this->response);
        // die();
        return $this->response['first_name'];
    }

    /**
     * @return mixed
     */
    public function getLastName() {
        return $this->response['last_name'];
    }

}