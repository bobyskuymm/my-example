<?php

namespace AppBundle\OAuth\Response;

use HWI\Bundle\OAuthBundle\OAuth\Response\PathUserResponse;

/**
 * TwitterUserResponse
 *
 */
class TwitterUserResponse extends PathUserResponse
{

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->getValueForPath('username', false);
    }

    /**
     * {@inheritdoc}
     */
    public function getRealname()
    {
        return $this->getValueForPath('realname', false);
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->getValueForPath('password', false);
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return NULL;
    }

    /**
     * @return mixed
     */
    public function getAvatar() {
        if (!$this->response['default_profile_image']) {
            return str_replace('_normal.', '.', $this->response['profile_image_url']);
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getGender() {
        return null;
    }

    /**
     * @return mixed
     */
    public function getFirstName() {
        return $this->response['name'];
    }

    /**
     * @return mixed
     */
    public function getLastName() {
        return '';
    }

}