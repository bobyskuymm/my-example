<?php

namespace AppBundle\EventListener;

use Symfony\Component\Routing\RouterInterface;

use Presta\SitemapBundle\Service\SitemapListenerInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;

class SitemapListener implements SitemapListenerInterface
{
    private $router;
    private $event;
    private $doctrine;

    public function __construct(RouterInterface $router, $doctrine)
    {
        $this->router = $router;
        $this->doctrine = $doctrine;

    }

    public function populateSitemap(SitemapPopulateEvent $event)
    {
        $section = $event->getSection();
        $this->event = $event;

        if (is_null($section) || $section == 'default') {

            $this->addUrl('homepage');
            $this->addUrl('infographic');
            $this->addUrl('person_index');
            $this->addUrl('search');

        }

        if (is_null($section) || $section == 'users') {

            array_map(function($item) {
                $this->addUrl('account_show', ['user' => $item->getId()], 'users');
            }, $this->doctrine->getRepository('AppBundle:User')->findAll());

        }

        if (is_null($section) || $section == 'client_about_us') {

            $this->addUrl('client_review_index', [], 'client_about_us');

            array_map(function($item) {
                $this->addUrl('client_review_show', ['url' => $item->getUrl()], 'client_about_us', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:ClientAboutUs')->findByHide(false));

        }

        if (is_null($section) || $section == 'clinics') {

            $this->addUrl('clinic_index', [], 'clinics');

            array_map(function($item) {
                $this->addUrl('clinic_show', ['url' => $item->getUrl()], 'clinics', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:Clinic')->findBy(['moderated' => true, 'hide' => false]));

        }

        if (is_null($section) || $section == 'doctors') {

            array_map(function($item) {
                $this->addUrl('doctor_index', ['type' => $item->getUrl()], 'doctors');
            }, $this->doctrine->getRepository('AppBundle:DoctorType')->findAll());

            array_map(function($item) {
                $this->addUrl('doctor_show', ['url' => $item->getUrl()], 'doctors', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:Doctor')->findBy(['moderated' => true, 'hide' => false]));

        }

        if (is_null($section) || $section == 'lifehack') {

            $this->addUrl('lifehack_index', [], 'lifehack');

            array_map(function($item) {
                $this->addUrl('lifehack_show', ['url' => $item->getUrl()], 'lifehack', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:LifehackArticle')->findAll());

        }


        if (is_null($section) || $section == 'media_about_us') {

            $this->addUrl('media_index', [], 'media_about_us');

            array_map(function($item) {
                $this->addUrl('media_show', ['url' => $item->getUrl()], 'media_about_us', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:MediaAboutUs')->findAll());

        }

        if (is_null($section) || $section == 'news') {

            $this->addUrl('news_index', [], 'news');

            array_map(function($item) {
                $this->addUrl('news_show', ['url' => $item->getUrl()], 'news', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:NewsArticle')->findAll());

        }

        if (is_null($section) || $section == 'pages') {

            array_map(function($item) {
                $this->addUrl('app_pages', ['page' => $item->getUrl()], 'pages', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:Page')->findAll());

        }

        if (is_null($section) || $section == 'personal_experience') {

            $this->addUrl('pexp_index', [], 'personal_experience');

            array_map(function($item) {
                $this->addUrl('pexp_show', ['url' => $item->getUrl()], 'personal_experience', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:PersonalExperience')->findByModerated(true));

        }

        if (is_null($section) || $section == 'realty') {

            $this->addUrl('realty_index', [], 'realty');

            array_map(function($item) {
                $this->addUrl('realty_show', ['url' => $item->getUrl()], 'realty', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:Realty')->findBy(['moderated' => true, 'hide' => false]));

        }

        if (is_null($section) || $section == 'topics') {

            $this->addUrl('topic_index', [], 'topics');

            array_map(function($item) {
                $this->addUrl('topic_show', ['topic' => $item->getId()], 'topics', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:Topic')->findAll());

        }

        if (is_null($section) || $section == 'vacancies') {

            $this->addUrl('vacancy_index', [], 'vacancies');

            array_map(function($item) {
                $this->addUrl('vacancy_show', ['url' => $item->getUrl()], 'vacancies', $item->getUpdatedAt());
            }, $this->doctrine->getRepository('AppBundle:Vacancy')->findAll());

        }

    }

    private function addUrl($route, $params = [], $section = null, $updated = null)
    {

        if (!$updated) {
            $updated = new \DateTime();
        }

        $url = $this->router->generate($route, $params, true);

        $this->event->getGenerator()->addUrl(
            new UrlConcrete(
                $url,
                $updated,
                UrlConcrete::CHANGEFREQ_HOURLY,
                1
            ),
            ($section ? $section : 'default')
        );
    }

}