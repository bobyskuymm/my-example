<?php
namespace AppBundle\Command;

use AppBundle\Entity\Doctor;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Yaml\Yaml;

class DoctorCommand extends ContainerAwareCommand
{

    protected $input;
    protected $output;
    protected $helper;


    protected $templateFiles = [];
    protected $cachedKeys = [];
    protected $structuredKeys = [];


    protected function configure()
    {
        $this
            ->setName('doctors:update:position')
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input  = $input;
        $this->output = $output;

        $em = $this->getContainer()->get('doctrine')->getManager();

        $doctors = $em->getRepository('AppBundle:Doctor')->getDoctors();


        $i = 0;

        /**
         * @var Doctor $d
         */
        foreach($doctors as  $key => $d) { $i++;
            echo "UPDATE  `doctors` SET  `position` =  '".$i."' WHERE  `doctors`.`id` =".$d->getId().";\n";
        }

        $em->flush();
    }



}
