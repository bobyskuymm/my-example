<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File as UploadFile;
use Symfony\Component\Debug\Exception\ContextErrorException;

use AppBundle\Entity\InstagramPhoto;

class FetchInstagramCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('site:instagram:fetch')
            ->setDescription('Fetch needed instagram photos')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $userId   = $this->getContainer()->getParameter('instagram_user_id');
        $clientId = $this->getContainer()->getParameter('instagram_client_id');
        $count    = $this->getContainer()->getParameter('instagram_count');

        $url = "https://api.instagram.com/v1/users/" . $userId . "/media/recent/?client_id=" . $clientId . "&count=" . $count;

        $doctrine = $this->getContainer()->get('doctrine');
        $em       = $doctrine->getManager();

        $inData = file_get_contents($url);

        $inData = (array) json_decode($inData);

        $photosAdded = 0; $photosUpdated = 0;

        if (array_key_exists("data", $inData))
        foreach ($inData['data'] as $image) {

            $found = $doctrine->getRepository('AppBundle:InstagramPhoto')->findOneByImId($image->id);

	    list($imageUrl, $trash) = explode("?ig_cache_key", $image->images->standard_resolution->url);
	    if (empty($imageUrl)) $imageUrl = $image->images->standard_resolution->url; 

            if (!$found) {

                $photo = new InstagramPhoto();

                $createdAt = new \DateTime(); $createdAt->setTimestamp($image->created_time);
                $photo->setCreatedAt($createdAt);
                $photo->setLink($image->link);
                $photo->setLikes($image->likes->count);
                $photo->setComments($image->comments->count);
                $photo->setImageUrl($imageUrl);
                $photo->setImId($image->id);
                $em->persist($photo);
                $em->flush();

                $photosAdded++;

            } else {

                $found->setLikes($image->likes->count);
                $found->setComments($image->comments->count);
                $found->setImageUrl($imageUrl);

                $em->persist($found);
                $em->flush();

                $photosUpdated++;

            }

        }

        $output->writeln('Added ' . $photosAdded . ' photos.');
        $output->writeln('Updated ' . $photosUpdated . ' photos.');

    }

}
