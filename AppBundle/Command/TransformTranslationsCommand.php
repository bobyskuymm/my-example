<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Yaml\Yaml;

class TransformTranslationsCommand extends ContainerAwareCommand
{

    protected $input;
    protected $output;
    protected $helper;

    protected $basePath = "src/AppBundle/Resources/views/";

    protected $templateFiles = [];
    protected $cachedKeys = [];
    protected $structuredKeys = [];


    protected function configure()
    {
        $this
            ->setName('site:translations:transform')
            ->setDescription('Transform text phrases to codes')
            ->addOption('backups', null, InputOption::VALUE_NONE, 'Make backups of templates')
            ->addOption('debug', null, InputOption::VALUE_NONE, 'Dumps translation in process')
            ->addArgument('output', InputArgument::REQUIRED, "Output language file")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->input  = $input;
        $this->output = $output;

        if ($this->input->getArgument('output')) {

            $yaml = file_get_contents($this->input->getArgument('output'));
            $this->structuredKeys = Yaml::parse($yaml);

            if (!empty($this->structuredKeys['app']['site'])) {
                $this->structuredKeys = $this->structuredKeys['app']['site'];
            }

        }

        $this->helper = $this->getHelper('question');

        $this->runDir();

        $output->writeln('found ' . count($this->templateFiles) . ' templates.');

        foreach ($this->templateFiles as $template) {

            $cQuestion = new ConfirmationQuestion('<fg=blue>Process "' . $template . '"?</fg=blue>', false);

            if (!$this->helper->ask($this->input, $this->output, $cQuestion)) {
                continue;
            }

            $output->writeln('<fg=green>processing template "' . $template . '":</fg=green>');

            $data = file_get_contents($template);

            // Перевірка для {% trans %}{% endtrans %}

            $fragments = explode('/' , $template);

            $baseKey = "app.site";

            $fragment = "";
            if (count($fragments) == 6) {
                $baseKey .= '.' . strtolower($fragments[4]);
                $fragment = strtolower($fragments[4]);
            }

            preg_match_all("/{% trans %}([^{]+){% endtrans %}/", $data, $matches);
            $data = $this->processTemplate($data, $matches, $baseKey, $fragment);

            preg_match_all("/\(\'([^\']+)\'\|trans\)/", $data, $matches);
            $data = $this->processTemplate($data, $matches, $baseKey, $fragment);

            $wQuestion = new ConfirmationQuestion("<fg=red>Save changes?</fg=red>", false);

            $yaml = Yaml::dump(['app' => ['site' => $this->structuredKeys]], 10);

            if ($this->input->getOption('debug')) {
                dump($yaml); // DO NOT REMOVE
            }

            if ($this->helper->ask($this->input, $this->output, $wQuestion)) {

                if ($input->getOption('backups')){
                    copy($template, $template . '.' . date('dmy-hi') . '.old');
                }

                file_put_contents($template, $data);
                $this->output->writeln('<fg=blue>saved template: "' . $template . "'</fg=blue>");
            }

            if ($this->input->getArgument('output')) {

                file_put_contents($this->input->getArgument('output'), $yaml);

            }

        }

    }

    protected function processTemplate($data, $matches, $baseKey, $fragment)
    {

        foreach($matches[1] as $index => $match) {

            $key = $baseKey;

            if (preg_match("/^app\.site\./", $match)) {
                continue;
            }

            $fixedMatch = preg_replace("/\s+/", " ", $match);

            if (!array_key_exists($fixedMatch, $this->cachedKeys)) {
                $question     = new Question('Enter key for "' . $fixedMatch . '":', false);
                $kTemp = $key;
                do {
                    $key = $kTemp . '.' . $this->helper->ask($this->input, $this->output, $question);
                } while(in_array($key, $this->cachedKeys));
            } else {
                $key = $this->cachedKeys[$fixedMatch];
            }

            $this->output->writeln('key: "' . $fixedMatch . '": ' . $key);
            $this->cachedKeys[$fixedMatch] = $key;

            $d = explode(".", $key);
            $userKey = $d[count($d)-1];

            if ($fragment != '') {

                if (!array_key_exists($fragment, $this->structuredKeys)) {
                    $this->structuredKeys[$fragment] = [];
                }

                $this->structuredKeys[$fragment][$userKey] = $fixedMatch;

            } else {

                $this->structuredKeys[$userKey] = $fixedMatch;

            }

            $data = str_replace($fixedMatch, $key, $data);

        }

        return $data;
    }

    protected function runDir($dirName = null)
    {

        $dir = opendir($this->basePath . $dirName);

        while ($item = readdir($dir)) {
            if ($item != '.' && $item != '..') {

                if (is_dir($this->basePath . $dirName . $item)) {
                    $this->runDir($item . '/');
                } else {
                    $this->templateFiles[] = $this->basePath . $dirName . $item;
                }

            }
        }

        closedir($dir);

    }

}
