<?php


namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\AccountTypeFormType;
use AppBundle\Form\TopicType;
use AppBundle\Form\CallbackApplicationType;
use AppBundle\Form\ExcursionApplicationType;
use AppBundle\Form\AppointmentApplicationType;
use AppBundle\Form\ClientAboutUsType;
use AppBundle\Form\LeaseApplicationType;

use AppBundle\Entity\Topic;
use AppBundle\Entity\CallbackApplication;
use AppBundle\Entity\ExcursionApplication;
use AppBundle\Entity\AppointmentApplication;
use AppBundle\Entity\ClientAboutUs;
use AppBundle\Entity\LeaseApplication;


use AppBundle\Controller\CartController;

class GeneralService
{

    protected $container;
    protected $doctrine;
    protected $router;

    /**
     * @var Request
     */
    protected $request;
    protected $locale;

    public function __construct($container)
    {
        $this->container = $container;
        $this->doctrine  = $this->container->get('doctrine');
        $this->router    = $this->container->get('router');
    }


    /**
     * Return count items in cart
     *
     * @return int
     */
    public function getCartCount() {

        $cartItems = $this->request->getSession()->get(CartController::$cartKey);

        $cartCount = 0;

        if (!empty($cartItems)) {
            foreach ($cartItems as $item){
                $cartCount += $item['count'];
            }
        }

        return $cartCount;
    }



    public function getLastNewsArticle()
    {

        $newsItem = $this->doctrine->getRepository('AppBundle:NewsArticle')->findOneBy([], ['createdAt' => 'DESC']);

        return $newsItem;

    }

    public function translateMonthShort($month)
    {

        if ($this->locale && $this->locale == 'en') {

            return date("M", strtotime(date("d") . '-' . $month . '.' . date("Y")));

        } else {

            $monthes = [
                 1 => 'Янв',
                 2 => 'Фев',
                 3 => 'Мар',
                 4 => 'Апр',
                 5 => 'Май',
                 6 => 'Июн',
                 7 => 'Июл',
                 8 => 'Авг',
                 9 => 'Сен',
                10 => 'Окт',
                11 => 'Ноя',
                12 => 'Дек',
            ];

            if (array_key_exists($month, $monthes)) {
                return $monthes[$month];
            }

        }

        return null;

    }

    public function translateMonth($month)
    {

        if ($this->locale && $this->locale == 'en') {

            return date("F", strtotime(date("d") . '-' . $month . '.' . date("Y")));

        } else {

            $monthes = [
                 1 => 'Январь',
                 2 => 'Февраль',
                 3 => 'Март',
                 4 => 'Апрель',
                 5 => 'Май',
                 6 => 'Июнь',
                 7 => 'Июль',
                 8 => 'Август',
                 9 => 'Сентябрь',
                10 => 'Октябрь',
                11 => 'Ноябрь',
                12 => 'Декабрь',
            ];

            if (array_key_exists($month, $monthes)) {
                return $monthes[$month];
            }

        }

        return null;

    }

    public function getNewTopicForm()
    {

        if (!$this->container->get('security.context')->getToken()) {
            return false;
        }

        $topic = new Topic();

        $form = $this->container->get('form.factory')->create(new TopicType($this->container), $topic);

        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->doctrine->getManager()->persist($topic);
            $this->doctrine->getManager()->flush();

            return new RedirectResponse($this->router->generate('topic_show', [ 'topic' => $topic->getId() ]));

        }

        return $form->createView();

    }

    public function setRequest(Request $request = null)
    {
        if ($request !== null) {
            $this->request = $request;
            $this->locale = $this->request->getLocale();
        }
    }

    public function getCallbackApplicationForm()
    {

        $application = new CallbackApplication();

        $form = $this->container->get('form.factory')->create(new CallbackApplicationType($this->container), $application);

        return $form->createView();

    }

    public function getExcursionApplicationForm()
    {

        $application = new ExcursionApplication();

        $application->setDate(new \DateTime());

        $form = $this->container->get('form.factory')->create(new ExcursionApplicationType($this->container), $application);

        return $form->createView();

    }

    public function getAppointmentApplicationForm()
    {

        $application = new AppointmentApplication();

        $application->setDate(new \DateTime());

        $form = $this->container->get('form.factory')->create(new AppointmentApplicationType($this->container), $application);

        return $form->createView();

    }

    public function getClientAboutUsForm()
    {

        $review = new ClientAboutUs();

        $form = $this->container->get('form.factory')->create(new ClientAboutUsType($this->container), $review);

        return $form->createView();

    }

    public function getRealtyApplicationForm()
    {

        $application = new LeaseApplication();

        $application->setDate(new \DateTime());

        $form = $this->container->get('form.factory')->create(new LeaseApplicationType($this->container), $application);

        return $form->createView();

    }

    public function getUnreadPrivateMessages()
    {

        if (!$this->container->get('security.context')->getToken() || $this->container->get('security.context')->getToken()->getUser() == 'anon.') {
            return 0;
        }

        $user = $this->container->get('security.context')->getToken()->getUser();

        $unread = $this->doctrine->getRepository('AppBundle:PrivateMessage')->getUnread($user);

        return $unread;

    }

}