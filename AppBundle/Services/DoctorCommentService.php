<?php


namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Doctor;
use AppBundle\Entity\DoctorThread;
use AppBundle\Entity\DoctorComment;

use AppBundle\Form\DoctorCommentFormType;

class DoctorCommentService
{

    protected $container;
    protected $doctrine;
    protected $router;
    protected $user;
    protected $request;
    protected $formFactory;

    protected $thread;

    public function __construct($container)
    {

        $this->container   = $container;
        $this->doctrine    = $this->container->get('doctrine');
        $this->router      = $this->container->get('router');
        $this->formFactory = $this->container->get('form.factory');

        if ($this->container->get('security.context')->getToken()) {
            $this->user = $this->container->get('security.context')->getToken()->getUser();
        }

    }

    public function getThread(Doctor $doctor)
    {

        $thread = $doctor->getThread();

        if (!$thread) {

            $thread = new DoctorThread();
            $thread->setDoctor($doctor);
            $doctor->setThread($thread);

            $this->doctrine->getManager()->persist($thread, $doctor);
            $this->doctrine->getManager()->flush();

        }

        $thread->updateCommentCount();

        $this->thread = $thread;

        return $thread;

    }

    public function getCommentForm($parent = null)
    {

        if (!$this->container->get('security.context')->getToken()) {
            return false;
        }

        $comment = new DoctorComment();

        $comment->setParent($parent);
        $comment->setThread($this->thread);

        $form = $this->formFactory->create(new DoctorCommentFormType($this->container), $comment);

        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {

            $thread = $form->getData()->getThread();

            if ($comment->getRating()->getTotalRating() == 0) {
                $comment->setRating(null);
            } else {
                $rating = $comment->getRating();
                $rating->setComment($comment);
                $this->doctrine->getManager()->persist($rating);
            }

            $this->doctrine->getManager()->persist($comment, $thread);
            $this->doctrine->getManager()->flush();

            if ($thread) {
                $thread->updateCommentCount();
                $thread->updateDoctorTotalRating();
            }

            $this->doctrine->getManager()->persist($comment, $thread);
            $this->doctrine->getManager()->flush();

            return new RedirectResponse($this->request->getUri());

        }

        return $form->createView();

    }

    public function countComments($id)
    {

        $thread = $this->getThread($id);

        return $thread->getCommentCount();

    }

    public function setRequest(Request $request = null)
    {
        if ($request !== null) {
            $this->request = $request;
        }
    }

}