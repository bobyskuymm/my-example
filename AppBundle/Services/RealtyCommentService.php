<?php


namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Realty;
use AppBundle\Entity\RealtyThread;
use AppBundle\Entity\RealtyComment;

use AppBundle\Form\RealtyCommentFormType;

class RealtyCommentService
{

    protected $container;
    protected $doctrine;
    protected $router;
    protected $user;
    protected $request;
    protected $formFactory;

    protected $thread;

    public function __construct($container)
    {

        $this->container   = $container;
        $this->doctrine    = $this->container->get('doctrine');
        $this->router      = $this->container->get('router');
        $this->formFactory = $this->container->get('form.factory');

        if ($this->container->get('security.context')->getToken()) {
            $this->user = $this->container->get('security.context')->getToken()->getUser();
        }

    }

    public function getThread(Realty $realty)
    {

        $thread = $realty->getThread();

        if (!$thread) {

            $thread = new RealtyThread();
            $thread->setRealty($realty);
            $realty->setThread($thread);

            $this->doctrine->getManager()->persist($thread, $realty);
            $this->doctrine->getManager()->flush();

        }

        $thread->updateCommentCount();

        $this->thread = $thread;

        return $thread;

    }

    public function getCommentForm($parent = null)
    {

        if (!$this->container->get('security.context')->getToken()) {
            return false;
        }

        $comment = new RealtyComment();

        $comment->setParent($parent);
        $comment->setThread($this->thread);


        $form = $this->formFactory->create(new RealtyCommentFormType($this->container), $comment);

        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {

            $thread = $form->getData()->getThread();

            if ($comment->getRating()->getTotalRating() == 0) {
                $comment->setRating(null);
            } else {
                $rating = $comment->getRating();
                $rating->setComment($comment);
                $this->doctrine->getManager()->persist($rating);
            }

            $this->doctrine->getManager()->persist($comment, $thread);
            $this->doctrine->getManager()->flush();

            if ($thread) {
                $thread->updateCommentCount();
                $thread->updateRealtyTotalRating();
            }

            $this->doctrine->getManager()->persist($comment, $thread);
            $this->doctrine->getManager()->flush();

            return new RedirectResponse($this->request->getUri());

        }

        return $form->createView();

    }

    public function countComments($id)
    {

        $thread = $this->getThread($id);

        return $thread->getCommentCount();

    }

    public function setRequest(Request $request = null)
    {
        if ($request !== null) {
            $this->request = $request;
        }
    }

}