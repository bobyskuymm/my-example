<?php


namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


class TurboSMSService
{

    protected $container;
    protected $doctrine;
    protected $request;
    protected $authData;
    protected $client;

    public function __construct($container)
    {
        $this->container = $container;
        $this->doctrine  = $this->container->get('doctrine');
        $this->authData  = [
            'login'    => $this->container->getParameter('turbo_sms_login'),
            'password' => $this->container->getParameter('turbo_sms_password'),
        ];
        $this->sender    = $this->container->getParameter('turbo_sms_sender');
        $this->authorize();

    }

    public function sendSMS($phone, $message)
    {

        $result = $this->checkBalance();

        if ((int) $result < 1) {
            return [
                'error' => 'out of money',
            ];
        }

        $result =
        $this->client->SendSMS([
            'sender'       => $this->sender,
            'destination'  => $phone,
            'text'         => $message,
        ]);

        return $result;

    }

    public function checkBalance()
    {
        return $this->client->getCreditBalance()->GetCreditBalanceResult;
    }

    protected function authorize()
    {

        $this->client = new \SoapClient ('http://turbosms.in.ua/api/wsdl.html');

        $result = $this->client->Auth($this->authData);

        return $result;

    }

}