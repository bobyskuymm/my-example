<?php


namespace AppBundle\Services;

use AppBundle\Form\AccountTypeFormType;

class UserTypeCheckService
{

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function checkType()
    {

        if ($this->container->get('security.context')->getToken()) {
            $user = $this->container->get('security.context')->getToken()->getUser();
        } else {
            return true;
        }

        if (!$user || $user === 'anon.') return true;

        if ($user->getAccountType()) return true;

        return false;

    }

    public function getSetTypeForm()
    {

        $user = $this->container->get('security.context')->getToken()->getUser();

        $form = $this->container->get('form.factory')->create(new AccountTypeFormType($this->container), $user);

        return $form->createView();

    }

}