<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\HttpFoundation\File\File;

class DoctorPhotoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('photo', 'file')
        ;

        $builder
            ->get('photo')
            ->addModelTransformer(new CallbackTransformer(
                function($file) {
                    if (is_array($file)) {
                        $file = new File($file['path'], false);
                    }
                    return $file;
                },
                function($file) {
                    return $file;
                }
              ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\DoctorPhoto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_doctorphoto';
    }
}
