<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CallbackApplicationType extends AbstractType
{

    protected $container;
    protected $router;


    public function __construct($container)
    {

        $this->container = $container;
        $this->router    = $this->container->get('router');

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($this->router->generate('callback_application_save'))
            ->add('name', null, [
                'required' => true,
              ])
            ->add('phone', null, [
                'required' => true,
              ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\CallbackApplication'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_callbackapplication';
    }
}
