<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Form\DoctorPhoneType;
use AppBundle\Form\DoctorEducationType;
use AppBundle\Form\DoctorMedicalPracticeType;
use AppBundle\Form\DoctorServiceType;
use AppBundle\Form\DoctorDocumentType;
use AppBundle\Form\DoctorPhotoType;


class DoctorType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('nickName')
            ->add('birthday', 'birthday', [
                'years' => range(1960, date("Y")),
              ])
            ->add('email')
            ->add('phones', 'collection', [
                'type' => new DoctorPhoneType(),
                'allow_add' => true,
                'allow_delete' => true,
                'options' => [
                    'required' => false,
                    'attr' => [
                        'class' => 'personal-data-phone',
                    ],
                ],
              ])
            ->add('education', 'collection', [
                'type' => new DoctorEducationType(),
                'allow_add' => true,
                'allow_delete' => true,
                'options' => [
                    'required' => false,
                    'attr' => [
                        'class' => 'personal-data-phone',
                    ],
                ],
              ])
            ->add('medicalPractice', 'collection', [
                'type' => new DoctorMedicalPracticeType(),
                'allow_add' => true,
                'allow_delete' => true,
                'options' => [
                    'required' => false,
                    'attr' => [
                        'class' => 'personal-data-phone',
                    ],
                ],
              ])
            ->add('services', 'collection', [
                'type' => new DoctorServiceType(),
                'allow_add' => true,
                'allow_delete' => true,
                'options' => [
                    'required' => false,
                    'attr' => [
                        'class' => 'personal-data-phone',
                    ],
                ],
              ])
            ->add('documents', 'collection', [
                'type' => new DoctorDocumentType(),
                'allow_add' => true,
                'allow_delete' => true,
                'options' => [
                    'required' => false,
                    'attr' => [
                        'class' => 'personal-data-phone',
                    ],
                ],
              ])
            ->add('hide')
            ->add('showEnglish', null, [
                'required' => false,
              ])
            ->add('specialization', 'text', [
                'required' => false,
              ])
            ->add('fullSpecialization')
            ->add('specialOffer', 'textarea', [
                'required' => false,
              ])
            ->add('homeVisits')
            ->add('cabinetAddress')
            ->add('cabinetPhoto', 'iphp_file', [
                'required' => false,
              ])
            ->add('cabinetAbout')
            ->add('publicationCount')
            ->add('about')
            ->add('fullAbout')
            ->add('scienceDegree', 'text', [
                'required' => false,
              ])
            ->add('childbirthPrice')
            ->add('currentOccupation')
            ->add('experience')
            ->add('currentClinic')
            ->add('alternateDoctor')
            ->add('type')
            ->add('location')
            ->add('languages')
            ->add('socialVk')
            ->add('socialFb')
            ->add('socialGp')
            ->add('socialOk')
            ->add('socialIn')
            ->add('skype')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Doctor'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_doctor';
    }
}
