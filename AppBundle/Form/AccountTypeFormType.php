<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AccountTypeFormType extends AbstractType
{

    protected $container;
    protected $router;


    public function __construct($container)
    {

        $this->container = $container;
        $this->router    = $this->container->get('router');

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($this->router->generate('account_set_type'))
            ->add('accountType', null, [
                'label' => 'app.user.account_type',
              ])
            ->add('save', 'submit', [
                'label' => 'app.user.account_type_save_button',
              ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_user_account_type';
    }
}
