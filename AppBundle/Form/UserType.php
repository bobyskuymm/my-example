<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\HttpFoundation\File\File;

use AppBundle\Form\UserPhoneType;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('nickname')
            ->add('hide')
            ->add('birthday', 'birthday', [
                'years' => range(1960, date("Y")),
              ])
            ->add('socialVk')
            ->add('socialFb')
            ->add('socialGp')
            ->add('socialOk')
            ->add('socialIn')
            ->add('about')
            ->add('skype')
            ->add('avatar', 'file')
            ->add('location')
            ->add('phones', 'collection', [
                'type' => new UserPhoneType(),
                'allow_add' => true,
                'allow_delete' => true,
                'options' => [
                    'required' => false,
                    'attr' => [
                        'class' => 'personal-data-phone',
                    ],
                ],
              ])
        ;

        $builder
            ->get('avatar')
            ->addModelTransformer(new CallbackTransformer(
                function($file) {
                    if (is_array($file)) {
                        $file = new File($file['path'], false);
                    }
                    return $file;
                },
                function($file) {
                    return $file;
                }
              ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_user';
    }
}
