<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;

class AppointmentApplicationType extends AbstractType
{

    protected $container;
    protected $router;


    public function __construct($container)
    {

        $this->container = $container;
        $this->router    = $this->container->get('router');

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($this->router->generate('appointment_application_save'))
            ->add('date', null, [
                'required' => true,
                'years' => range(date('Y'), date('Y') + 1),
              ])
            ->add('phone', null, [
                'required' => false,
              ])
            ->add('email', null, [
                'required' => false,
              ])
            ->add('doctor', 'hidden')
        ;

        $builder
            ->get('doctor')
            ->addModelTransformer(new CallbackTransformer(
                function($doctor) {
                    if ($doctor) {
                        return $doctor->getId();
                    }
                    return null;
                },
                function($doctorId) {
                    return $this->container->get('doctrine')->getRepository('AppBundle:Doctor')
                           ->findOneById($doctorId);
                }
              ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\AppointmentApplication'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_appointmentapplication';
    }
}
