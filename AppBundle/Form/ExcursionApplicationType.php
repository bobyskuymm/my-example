<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;

class ExcursionApplicationType extends AbstractType
{

    protected $container;
    protected $router;


    public function __construct($container)
    {

        $this->container = $container;
        $this->router    = $this->container->get('router');

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($this->router->generate('excursion_application_save'))
            ->add('date', null, [
                'required' => true,
                'years' => range(date('Y'), date('Y') + 1),
              ])
            ->add('phone', null, [
                'required' => false,
              ])
            ->add('email', null, [
                'required' => false,
              ])
            ->add('clinic', 'hidden')
        ;

        $builder
            ->get('clinic')
            ->addModelTransformer(new CallbackTransformer(
                function($clinic) {
                    if ($clinic) {
                        return $clinic->getId();
                    }
                    return null;
                },
                function($clinicId) {
                    return $this->container->get('doctrine')->getRepository('AppBundle:Clinic')
                           ->findOneById($clinicId);
                }
              ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ExcursionApplication'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_excursionapplication';
    }
}
