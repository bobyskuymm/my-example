<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;

use AppBundle\Form\RealtyRatingType;
use AppBundle\Entity\RealtyRating;

class RealtyCommentFormType extends AbstractType
{


    protected $container;
    protected $router;


    public function __construct($container)
    {

        $this->container = $container;
        $this->router    = $this->container->get('router');

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parent', 'hidden')
            ->add('thread', 'hidden')
            ->add('body', null, [
                'label' => 'app.comment.body',
              ])
            ->add('image', 'iphp_file', [
                'required' => false,
              ])
            ->add('rating', new RealtyRatingType())
            // ->add('save', 'submit', [
            //     'label' => 'app.comment.save_button',
            //   ])
        ;

        $builder
            ->get('parent')
            ->addModelTransformer(new CallbackTransformer(
                function($comment) {
                    if ($comment) {
                        return $comment->getId();
                    }
                    return null;
                },
                function($commentId) {
                    return $this->container->get('doctrine')->getRepository('AppBundle:RealtyComment')
                           ->findOneById($commentId);
                }
              ))
        ;

        $builder
            ->get('thread')
            ->addModelTransformer(new CallbackTransformer(
                function($thread) {
                    if ($thread) {
                        return $thread->getId();
                    }
                    return null;
                },
                function($threadId) {
                    return $this->container->get('doctrine')->getRepository('AppBundle:RealtyThread')
                           ->findOneById($threadId);
                }
              ))
        ;

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\RealtyComment'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_realty_comment_type';
    }
}
