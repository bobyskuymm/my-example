<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;

class LeaseApplicationType extends AbstractType
{

    protected $container;
    protected $router;


    public function __construct($container)
    {

        $this->container = $container;
        $this->router    = $this->container->get('router');

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($this->router->generate('lease_application_save'))
            ->add('date', null, [
                'required' => true,
                'years' => range(date('Y'), date('Y') + 1),
              ])
            ->add('phone', null, [
                'required' => false,
              ])
            ->add('email', null, [
                'required' => false,
              ])
            ->add('realty', 'hidden')
        ;

        $builder
            ->get('realty')
            ->addModelTransformer(new CallbackTransformer(
                function($realty) {
                    if ($realty) {
                        return $realty->getId();
                    }
                    return null;
                },
                function($realtyId) {
                    return $this->container->get('doctrine')->getRepository('AppBundle:Realty')
                           ->findOneById($realtyId);
                }
              ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\LeaseApplication'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_leaseapplication';
    }
}
