<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Form\RealtyComfortType;


class RealtyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('price')
            ->add('about', 'ckeditor', [
                'required' => false,
              ])
            ->add('shortAbout')
            ->add('possibleDiscounts')
            ->add('address')
            ->add('bedrooms')
            ->add('bathrooms')
            ->add('beds')
            ->add('area')
            ->add('floor')
            ->add('minRentalPeriod')
            ->add('childrenAllowed')
            ->add('animalsAllowed')
            ->add('notIncluded')
            ->add('hide')
            ->add('showEnglish', null, [
                'required' => false,
              ])
            ->add('type')
            ->add('comfort', 'collection', [
                'type' => new RealtyComfortType(),
                'allow_add' => true,
                'allow_delete' => true,
                'options' => [
                    'required' => false,
                    'attr' => [
                        'class' => 'personal-data-comfort',
                    ],
                ],
              ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Realty'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_realty';
    }
}
