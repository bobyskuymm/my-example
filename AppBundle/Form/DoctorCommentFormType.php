<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;

use AppBundle\Form\DoctorRatingType;
use AppBundle\Entity\DoctorRating;


class DoctorCommentFormType extends AbstractType
{

    protected $container;
    protected $router;


    public function __construct($container)
    {

        $this->container = $container;
        $this->router    = $this->container->get('router');

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('parent', 'hidden')
            ->add('thread', 'hidden')
            ->add('body', null, [
                'label' => 'app.comment.body',
              ])
            ->add('image', 'iphp_file', [
                'required' => false,
              ])
            ->add('rating', new DoctorRatingType())
            // ->add('save', 'submit', [
            //     'label' => 'app.comment.save_button',
            //   ])
        ;

        $builder
            ->get('parent')
            ->addModelTransformer(new CallbackTransformer(
                function($comment) {
                    if ($comment) {
                        return $comment->getId();
                    }
                    return null;
                },
                function($commentId) {
                    return $this->container->get('doctrine')->getRepository('AppBundle:DoctorComment')
                           ->findOneById($commentId);
                }
              ))
        ;

        $builder
            ->get('thread')
            ->addModelTransformer(new CallbackTransformer(
                function($thread) {
                    if ($thread) {
                        return $thread->getId();
                    }
                    return null;
                },
                function($threadId) {
                    return $this->container->get('doctrine')->getRepository('AppBundle:DoctorThread')
                           ->findOneById($threadId);
                }
              ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\DoctorComment'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_doctor_comment_type';
    }
}
