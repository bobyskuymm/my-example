<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Entity\ClinicDocument;
use AppBundle\Entity\ClinicPhoto;
use AppBundle\Entity\ClinicService;

use AppBundle\Form\ClinicDocumentType;
use AppBundle\Form\ClinicServiceType;

class ClinicType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('price')
            ->add('foundationYear')
            ->add('about')
            ->add('shortAbout')
            ->add('fullAbout')
            ->add('specialOffer')
            ->add('address')
            ->add('hide')
            ->add('showEnglish', null, [
                'required' => false,
              ])
            ->add('location')
            ->add('services', 'collection', [
                'type' => new ClinicServiceType(),
                'allow_add' => true,
                'allow_delete' => true,
                'options' => [
                    'required' => false,
                    'attr' => [
                        'class' => 'personal-data-phone',
                    ],
                ],
              ])
            ->add('documents', 'collection', [
                'type' => new ClinicDocumentType(),
                'allow_add' => true,
                'allow_delete' => true,
                'options' => [
                    'required' => false,
                    'attr' => [
                        'class' => 'personal-data-phone',
                    ],
                ],
              ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Clinic'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_clinic';
    }
}
