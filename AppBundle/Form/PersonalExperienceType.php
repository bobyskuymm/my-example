<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;

use AppBundle\Entity\PersonalExperienceTag;

class PersonalExperienceType extends AbstractType
{

    protected $container;
    protected $doctrine;
    protected $router;


    public function __construct($container)
    {

        $this->container = $container;
        $this->doctrine  = $this->container->get('doctrine');
        $this->router    = $this->container->get('router');

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('image', 'iphp_file')
            ->add('shortDescription')
            ->add('body', 'ckeditor')
            ->add('tags', 'hidden')
        ;

        $builder
            ->get('tags')
            ->addModelTransformer(new CallbackTransformer(
                function($tags) {
                    $tagNames = [];
                    foreach ($tags as $item) {
                        $tagNames[] = $item->getName();
                    }
                    return implode(",", $tagNames);
                },
                function($tagNames) {
                    $tagNames = explode(",", $tagNames);
                    $tags = [];
                    foreach ($tagNames as $tagName) {
                        if ($tagName != "") {
                            $tag = $this->doctrine->getRepository('AppBundle:PersonalExperienceTag')
                                   ->findOneByName($tagName);
                            if (!$tag) {
                                $tag = new PersonalExperienceTag();
                                $tag->setName($tagName);
                                $this->doctrine->getManager()->persist($tag);
                                $this->doctrine->getManager()->flush();
                            }
                            $tags[] = $tag;
                        }
                    }
                    return $tags;
                }
              ))
        ;

    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PersonalExperience'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_personalexperience';
    }
}
