<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ClientAboutUsAdmin extends Admin
{

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('clientName', null, [
                'label' => 'app.sonata_client_about_us.labels.client_name',
              ])
            ->add('clientAddress', null, [
                'label' => 'app.sonata_client_about_us.labels.client_address',
              ])
            ->add('url', null, [
                'label' => 'app.sonata_client_about_us.labels.url',
              ])
            ->add('hide', null, [
                'label' => 'app.sonata_client_about_us.labels.hide',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('clientName', null, [
                'label' => 'app.sonata_client_about_us.labels.client_name',
              ])
            ->add('clientAddress', null, [
                'label' => 'app.sonata_client_about_us.labels.client_address',
              ])
            ->add('description', null, [
                'label' => 'app.sonata_client_about_us.labels.description',
              ])
            ->add('content', null, [
                'label' => 'app.sonata_client_about_us.labels.content',
              ])
            ->add('link', null, [
                'label' => 'app.sonata_client_about_us.labels.link',
              ])
            ->add('photo', 'iphp_file', [
                'required' => false,
                'label' => 'app.sonata_client_about_us.labels.photo',
              ])
            ->add('hide', null, [
                'required' => false,
                'label' => 'app.sonata_client_about_us.labels.hide',
              ])
            ->add('url', null, [
                'required' => false,
                'label' => 'app.sonata_client_about_us.labels.url',
              ])
        ;
    }

}
