<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class VacancyAdmin extends Admin
{

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('title', null, [
                'label' => 'app.sonata_vacancies.labels.title',
              ])
            ->add('fee', null, [
                'label' => 'app.sonata_vacancies.labels.fee',
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_vacancies.labels.created_at',
              ])
            ->add('updatedAt', null, [
                'label' => 'app.sonata_vacancies.labels.updated_at',
              ])
            ->add('createdBy', null, [
                'label' => 'app.sonata_vacancies.labels.created_by',
              ])
            ->add('updatedBy', null, [
                'label' => 'app.sonata_vacancies.labels.updated_by',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', null, [
                'label' => 'app.sonata_vacancies.labels.title',
              ])
            ->add('fee', null, [
                'label' => 'app.sonata_vacancies.labels.fee',
              ])
            ->add('shortDescription', 'ckeditor', [
                'required' => false,
                'label' => 'app.sonata_vacancies.labels.short_description',
              ])
            ->add('description', 'ckeditor', [
                'required' => false,
                'label' => 'app.sonata_vacancies.labels.description',
              ])
            ->add('email', null, [
                'label' => 'app.sonata_vacancies.labels.email',
              ])
            ->add('contact', null, [
                'label' => 'app.sonata_vacancies.labels.contact',
              ])
            ->add('image', 'iphp_file', [
                'required' => false,
                'label' => 'app.sonata_vacancies.labels.image',
              ])
            ->add('url', null, [
                'required' => false,
                'label' => 'app.sonata_vacancies.labels.url',
              ])
        ;
    }

}
