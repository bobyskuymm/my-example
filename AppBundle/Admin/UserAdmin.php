<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UserAdmin extends Admin
{

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }


    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username', null, [
                'label' => 'app.sonata_users.labels.username',
              ])
            ->add('email', null, [
                'label' => 'app.sonata_users.labels.email',
              ])
            ->add('enabled', null, [
                'label' => 'app.sonata_users.labels.enabled',
              ])
            ->add('locked', null, [
                'label' => 'app.sonata_users.labels.locked',
              ])
            // ->add('expired', null, [
            //     'label' => 'app.sonata_users.labels.expired',
            //   ])
            ->add('firstName', null, [
                'label' => 'app.sonata_users.labels.first_name',
              ])
            ->add('lastName', null, [
                'label' => 'app.sonata_users.labels.last_name',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('username', null, [
                'label' => 'app.sonata_users.labels.username',
              ])
            ->add('email', null, [
                'label' => 'app.sonata_users.labels.email',
              ])
            ->add('firstName', null, [
                'label' => 'app.sonata_users.labels.first_name',
              ])
            ->add('lastName', null, [
                'label' => 'app.sonata_users.labels.last_name',
              ])
            ->add('lastLogin', null, [
                'label' => 'app.sonata_users.labels.last_login',
              ])
            ->add('enabled', null, [
                'editable' => true,
                'label' => 'app.sonata_users.labels.enabled',
              ])
            ->add('locked', null, [
                'editable' => true,
                'label' => 'app.sonata_users.labels.locked',
              ])
            // ->add('expired')
            // ->add('avatar')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $roles = [];
        foreach (array_keys($this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles')) as $role) {
            $roles[$role] = $role;
        }

        $formMapper
            ->add('firstName', null, [
                'required' => false,
                'label' => 'app.sonata_users.labels.first_name',
              ])
            ->add('lastName', null, [
                'required' => false,
                'label' => 'app.sonata_users.labels.last_name',
              ])
            ->add('accountType', null, [
                'label' => 'app.sonata_users.labels.account_type',
              ])
            ->add('avatar', 'iphp_file', [
                // 'data_class' => null,
                'required' => false,
                'label' => 'app.sonata_users.labels.avatar',
              ])
            ->add('username', null, [
                'label' => 'app.sonata_users.labels.username',
              ])
            ->add('email', null, [
                'label' => 'app.sonata_users.labels.email',
              ])
            ->add('roles', 'choice', [
                'choices' => $roles,
                'multiple' => true,
                'required' => false,
                'label' => 'app.sonata_users.labels.roles',
              ])
            ->add('plainPassword', 'repeated', [
                'type' => 'password',
                'required' => false,
                'first_options' => [
                    'label' => 'app.sonata_users.labels.password',
                    'data' => '',
                ],
                'second_options' => [
                    'label' => 'app.sonata_users.labels.password_confirmation',
                ],
                'invalid_message' => 'app.sonata_users.labels.password_mismatch',
              ])
            ->add('enabled', null, [
                'required' => false,
                'label' => 'app.sonata_users.labels.enabled',
              ])
            ->add('locked', null, [
                'required' => false,
                'label' => 'app.sonata_users.labels.locked',
              ])
        ;
    }

    public function preUpdate($user)
    {

        if ($user->getPlainPassword() != "") {
            $user->setPassword($user->getPlainPassword());
        }

    }
}
