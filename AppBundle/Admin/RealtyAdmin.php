<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RealtyAdmin extends Admin
{


    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    public function getTemplate($name)
    {
        echo $name;
        die;
        return parent::getTemplate($name);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'app.sonata_realty.labels.name',
              ])
            ->add('user', null, [
                'label' => 'app.sonata_realty.labels.user',
              ])
            ->add('moderated', null, [
                'label' => 'app.sonata_realty.labels.moderated',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add('name', null, [
                'label' => 'app.sonata_realty.labels.name'
              ])
            ->add('ratingTotal', null, [
                'label' => 'app.sonata_realty.labels.rating_total',
              ])
            ->add('hide', null, [
                'label' => 'app.sonata_realty.labels.hide',
                // 'editable' => true,
              ])
            ->add('moderated', null, [
                'label' => 'app.sonata_realty.labels.moderated',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with($this->trans('app.sonata_realty.tabs.general'), [ 'tab' => true ])
                ->with(null)
                    ->add('moderated', null, [
                        'label' => 'app.sonata_realty.labels.moderated',
                      ])
                    ->add('moderatorComment', null, [
                        'label' => 'app.sonata_realty.labels.moderator_comment',
                      ])
                    ->add('city', null, [
                        'label' => 'app.sonata_clinic.labels.city',
                    ])
                    ->add('cityArea', null, [
                        'label' => 'app.sonata_clinic.labels.area',
                    ])
                    ->add('showEnglish', null, [
                        'required' => false,
                        'label' => 'app.sonata_realty.labels.show_english',
                      ])
                    ->add('user', null, [
                        'label' => 'app.sonata_realty.labels.user',
                      ])
                    ->add('name', null, [
                        'label' => 'app.sonata_realty.labels.name',
                      ])
                    ->add('type', null, [
                        'label' => 'app.sonata_realty.labels.type',
                      ])
                    ->add('hide', null, [
                        'label' => 'app.sonata_realty.labels.hide',
                      ])
                    ->add('price', null, [
                        'label' => 'app.sonata_realty.labels.price',
                      ])
                    ->add('shortAbout', null, [
                        'label' => 'app.sonata_realty.labels.short_about',
                      ])
                    ->add('about', 'ckeditor', [
                        'label' => 'app.sonata_realty.labels.about',
                        'required' => false,
                      ])

                    ->add('url', null, [
                        'label' => 'url',
                        'required' => false,
                      ])


                    ->add('address', null, [
                        'label' => 'app.sonata_realty.labels.address',
                    ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_realty.tabs.comfort'), [ 'tab' => true ])
                ->with(null)
                    ->add('comfort', 'sonata_type_collection', [
                        'label' => 'app.sonata_realty.labels.comfort',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_realty.tabs.photos'), ['tab' => true])
                ->with(null)
                    ->add('photos', 'sonata_type_collection', [
                        'label' => 'app.sonata_realty.labels.photos',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_realty.tabs.living_area'), ['tab' => true])
                ->with(null)
                    ->add('bedrooms', null, [
                        'label' => 'app.sonata_realty.labels.bedrooms',
                      ])
                    ->add('bathrooms', null, [
                        'label' => 'app.sonata_realty.labels.bathrooms',
                      ])
                    ->add('beds', null, [
                        'label' => 'app.sonata_realty.labels.beds',
                      ])
                    ->add('area', null, [
                        'label' => 'app.sonata_realty.labels.area',
                      ])
                    ->add('floor', null, [
                        'label' => 'app.sonata_realty.labels.floor',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_realty.tabs.additional'), ['tab' => true])
                ->with(null)
                    ->add('minRentalPeriod', null, [
                        'label' => 'app.sonata_realty.labels.min_rental_period',
                      ])
                    ->add('childrenAllowed', null, [
                        'label' => 'app.sonata_realty.labels.children_allowed',
                      ])
                    ->add('animalsAllowed', null, [
                        'label' => 'app.sonata_realty.labels.animals_allowed',
                      ])
                    ->add('notIncluded', null, [
                        'label' => 'app.sonata_realty.labels.not_included',
                      ])
                    ->add('possibleDiscounts', null, [
                        'label' => 'app.sonata_realty.labels.possible_discounts',
                      ])
                ->end()
            ->end()
        ;
    }


    public function prePersist($realty)
    {
        foreach ($realty->getPhotos() as $item) {
            $item->setRealty($realty);
        }
        foreach ($realty->getComfort() as $item) {
            $item->setRealty($realty);
        }
    }

    public function preUpdate($realty)
    {
        foreach ($realty->getPhotos() as $item) {
            $item->setRealty($realty);
        }
        foreach ($realty->getComfort() as $item) {
            $item->setRealty($realty);
        }
    }

}
