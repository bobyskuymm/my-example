<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Form\DoctorDocumentType;

class DoctorAdmin extends Admin
{
    public $baseUrl = "";
    public $last_position = 0;

    private $positionService;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    private $container;

    public function setContainer($c)
    {
        $this->container = $c;
    }

    public function setPositionService(\Pix\SortableBehaviorBundle\Services\PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')

            ;
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');

    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstName', null, [
                'label' => 'app.sonata_doctor.labels.first_name',
              ])
            ->add('lastName', null, [
                'label' => 'app.sonata_doctor.labels.last_name',
              ])
            ->add('type', null, [
                'label' => 'app.sonata_doctor.labels.type',
              ])
            ->add('moderated', null, [
                'label' => 'app.sonata_doctor.labels.moderated',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $this->last_position = $this->positionService->getLastPosition($this->getRoot()->getClass());


        unset($this->listModes['mosaic']);

        $listMapper
            ->add('name', null, [
                'label' => 'app.sonata_doctor.labels.name',
              ])
            ->add('type', null, [
                'label' => 'app.sonata_doctor.labels.type',
              ])
            ->add('ratingTotal', null, [
                'label' => 'app.sonata_doctor.labels.rating_total',
              ])
            ->add('hide', null, [
                'label' => 'app.sonata_doctor.labels.hide',
                // 'editable' => true,
              ])
            ->add('moderated', null, [
                'label' => 'app.sonata_doctor.labels.moderated',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'move' => array(
                        'template' => 'AppBundle:CRUDDefault:_sort.html.twig'
                    ),
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->with($this->trans('app.sonata_doctor.tabs.general'), [ 'tab' => true ])
                ->with(null)
                    ->add('moderated', null, [
                        'label' => 'app.sonata_doctor.labels.moderated',
                      ])

                    ->add('city', null, [
                        'label' => 'app.sonata_clinic.labels.city',
                    ])
                    ->add('area', null, [
                        'label' => 'app.sonata_clinic.labels.area',
                    ])

                    ->add('moderatorComment', null, [
                        'label' => 'app.sonata_doctor.labels.moderator_comment',
                      ])
                    ->add('showEnglish', null, [
                        'required' => false,
                        'label' => 'app.sonata_doctor.labels.show_english',
                      ])
                    ->add('user', null, [
                        'label' => 'app.sonata_doctor.labels.user',
                      ])
                    ->add('firstName', null, [
                        'label' => 'app.sonata_doctor.labels.first_name',
                      ])
                    ->add('lastName', null, [
                        'label' => 'app.sonata_doctor.labels.last_name',
                      ])
                    ->add('nickName', null, [
                        'label' => 'app.sonata_doctor.labels.nick_name',
                      ])
                    ->add('email', null, [
                        'label' => 'app.sonata_doctor.labels.email',
                      ])
                    ->add('birthday', 'birthday', [
                        'label' => 'app.sonata_doctor.labels.birthday',
                        // 'widget' => 'single_text',
                        'required' => false,
                      ])
                    ->add('socialVk', null, [
                        'label' => 'app.sonata_doctor.labels.social_vk',
                      ])
                    ->add('socialFb', null, [
                        'label' => 'app.sonata_doctor.labels.social_fb',
                      ])
                    ->add('socialGp', null, [
                        'label' => 'app.sonata_doctor.labels.social_gp',
                      ])
                    ->add('socialOk', null, [
                        'label' => 'app.sonata_doctor.labels.social_ok',
                      ])
                    ->add('socialIn', null, [
                        'label' => 'app.sonata_doctor.labels.social_in',
                      ])
                    ->add('skype', null, [
                        'label' => 'app.sonata_doctor.labels.skype',
                      ])
                    ->add('hide', null, [
                        'label' => 'app.sonata_doctor.labels.hide',
                        'required' => false,
                      ])
                    ->add('type', null, [
                        'label' => 'app.sonata_doctor.labels.type',
                      ])
                    ->add('specialization', null, [
                        'label' => 'app.sonata_doctor.labels.specialization',
                      ])
                    ->add('fullSpecialization', 'ckeditor', [
                        'label' => 'app.sonata_doctor.labels.full_specialization',
                      ])
                    ->add('about', 'ckeditor', [
                        'label' => 'app.sonata_doctor.labels.about_doctor',
                      ])
                    ->add('fullAbout', 'ckeditor', [
                        'label' => 'app.sonata_doctor.labels.about_doctor_full',
                      ])
                    ->add('specialOffer', null, [
                        'label' => 'app.sonata_doctor.labels.special_offer',
                      ])
                    ->add('homeVisits', null, [
                        'label' => 'app.sonata_doctor.labels.home_visits',
                      ])
                    ->add('scienceDegree', null, [
                        'label' => 'app.sonata_doctor.labels.science_degree',
                      ])
                    ->add('childbirthPrice', null, [
                        'label' => 'app.sonata_doctor.labels.childbirth_price',
                      ])
                    ->add('experience', null, [
                        'label' => 'app.sonata_doctor.labels.experience',
                      ])
                    ->add('alternateDoctor', null, [
                        'label' => 'app.sonata_doctor.labels.alternate_doctor',
                      ])
                    ->add('location', null, [
                        'label' => 'app.sonata_doctor.labels.location',
                      ])
                    ->add('url', null, [
                        'label' => 'app.sonata_doctor.labels.url',
                        'required' => false,
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_doctor.tabs.cabinet'), [ 'tab' => true ])
                ->with(null)
                    ->add('cabinetAddress', null, [
                        'label' => 'app.sonata_doctor.labels.cabinet_address',
                      ])
                    ->add('cabinetPhoto', 'iphp_file', [
                        'label' => 'app.sonata_doctor.labels.cabiner_photo',
                        'required' => false,
                      ])
                    ->add('cabinetAbout', 'ckeditor', [
                        'label' => 'app.sonata_doctor.labels.cabinet_about',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_doctor.tabs.current_occupation'), [ 'tab' => true ])
                ->with(null)
                    ->add('currentClinic', null, [
                        'label' => 'app.sonata_doctor.labels.current_clinic',
                      ])
                    ->add('currentOccupation', null, [
                        'label' => 'app.sonata_doctor.labels.current_occupation',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_doctor.tabs.documents'), [ 'tab' => true ])
                ->with(null)
                    ->add('documents', 'sonata_type_collection', [
                        'label' => 'app.sonata_doctor.labels.documents',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_doctor.tabs.education'), [ 'tab' => true ])
                ->with(null)
                    ->add('education', 'sonata_type_collection', [
                        'label' => 'app.sonata_doctor.labels.education',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_doctor.tabs.photos'), [ 'tab' => true ])
                ->with(null)
                    ->add('photos', 'sonata_type_collection', [
                        'label' => 'app.sonata_doctor.labels.photos',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_doctor.tabs.medical_practice'), [ 'tab' => true ])
                ->with(null)
                    ->add('medicalPractice', 'sonata_type_collection', [
                        'label' => 'app.sonata_doctor.labels.medical_practice',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_doctor.tabs.languages'), [ 'tab' => true ])
                ->with(null)
                    ->add('languages', null, [
                        'label' => 'app.sonata_doctor.labels.languages',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_doctor.tabs.services'), [ 'tab' => true ])
                ->with(null)
                    ->add('services', 'sonata_type_collection', [
                        'label' => 'app.sonata_doctor.labels.services',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_doctor.tabs.phones'), [ 'tab' => true ])
                ->with(null)
                    ->add('phones', 'sonata_type_collection', [
                        'label' => 'app.sonata_doctor.labels.phones',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
        ;
    }

    public function prePersist($doctor)
    {
        foreach ($doctor->getDocuments() as $item) {
            $item->setDoctor($doctor);
        }
        foreach ($doctor->getEducation() as $item) {
            $item->setDoctor($doctor);
        }
        foreach ($doctor->getPhotos() as $item) {
            $item->setDoctor($doctor);
        }
        foreach ($doctor->getMedicalPractice() as $item) {
            $item->setDoctor($doctor);
        }
        foreach ($doctor->getServices() as $item) {
            $item->setDoctor($doctor);
        }
        foreach ($doctor->getPhones() as $item) {
            $item->setDoctor($doctor);
        }

    }

    public function preUpdate($doctor)
    {
        foreach ($doctor->getDocuments() as $item) {
            $item->setDoctor($doctor);
        }
        foreach ($doctor->getEducation() as $item) {
            $item->setDoctor($doctor);
        }
        foreach ($doctor->getPhotos() as $item) {
            $item->setDoctor($doctor);
        }
        foreach ($doctor->getMedicalPractice() as $item) {
            $item->setDoctor($doctor);
        }
        foreach ($doctor->getServices() as $item) {
            $item->setDoctor($doctor);
        }
        foreach ($doctor->getPhones() as $item) {
            $item->setDoctor($doctor);
        }

    }

}
