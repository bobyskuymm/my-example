<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class RealtyPhotoAdmin extends Admin
{

    protected $formOptions = [
        'allow_extra_fields' => true,
        'validation_groups'  => false,
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('photo', 'iphp_file', [
                'label' => 'app.sonata_realty.labels.photo',
                'required' => false,
                'allow_extra_fields' => true,
              ])
        ;
    }

}
