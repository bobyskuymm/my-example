<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PersonalExperienceAdmin extends Admin
{

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            // ->remove('create')
            ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'app.sonata_pexp.labels.name'
              ])
            ->add('createdBy', null, [
                'label' => 'app.sonata_pexp.labels.created_by'
              ])
            ->add('moderated', null, [
                'label' => 'app.sonata_pexp.labels.moderated'
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('createdBy', null, [
                'label' => 'app.sonata_pexp.labels.created_by',
              ])
            ->add('name', null, [
                'label' => 'app.sonata_pexp.labels.name',
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_pexp.labels.created_at',
              ])
            ->add('moderated', null, [
                'label' => 'app.sonata_pexp.labels.moderated',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('moderated', null, [
                'label' => 'app.sonata_pexp.labels.moderated',
              ])
            ->add('name', null, [
                'label' => 'app.sonata_pexp.labels.name',
              ])
            ->add('image', 'iphp_file', [
                'label' => 'app.sonata_pexp.labels.image',
                'required' => false,
              ])
            ->add('tags', null, [
                'label' => 'app.sonata_pexp.labels.tags',
              ])
            ->add('shortDescription', null, [
                'label' => 'app.sonata_pexp.labels.short_description',
              ])
            ->add('body', 'ckeditor', [
                'label' => 'app.sonata_pexp.labels.body',
              ])
            ->add('createdBy', null, [
                'label' => 'app.sonata_pexp.labels.created_by',
              ])
            ->add('url', null, [
                'label' => 'app.sonata_pexp.labels.url',
                'required' => false,
              ])
        ;
    }

}
