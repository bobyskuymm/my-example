<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class MediaAboutUsAdmin extends Admin
{

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('publisher', null, [
                'label' => 'app.sonata_media_about_us.labels.',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('date', null, [
                'label' => 'app.sonata_media_about_us.labels.date',
              ])
            ->add('title', null, [
                'label' => 'app.sonata_media_about_us.labels.title',
              ])
            ->add('publisher', null, [
                'label' => 'app.sonata_media_about_us.labels.publisher',
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_media_about_us.labels.created_at',
              ])
            ->add('updatedAt', null, [
                'label' => 'app.sonata_media_about_us.labels.updated_at',
              ])
            ->add('createdBy', null, [
                'label' => 'app.sonata_media_about_us.labels.created_by',
              ])
            ->add('updatedBy', null, [
                'label' => 'app.sonata_media_about_us.labels.updated_by',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('date', null, [
                'label' => 'app.sonata_media_about_us.labels.date',
              ])
            ->add('publisher', null, [
                'label' => 'app.sonata_media_about_us.labels.publisher',
              ])
            ->add('title', null, [
                'label' => 'app.sonata_media_about_us.labels.title',
              ])
            ->add('shortDescription', 'ckeditor', [
                'required' => false,
                'label' => 'app.sonata_media_about_us.labels.short_description',
              ])
            ->add('content', 'ckeditor', [
                'required' => false,
                'label' => 'app.sonata_media_about_us.labels.content',
              ])
            ->add('link', null, [
                'label' => 'app.sonata_media_about_us.labels.link',
              ])
            ->add('image', 'iphp_file', [
                'required' => false,
                'label' => 'app.sonata_media_about_us.labels.image',
              ])
            ->add('file', 'iphp_file', [
                'required' => false,
                'label' => 'app.sonata_media_about_us.labels.file',
              ])
            ->add('url', null, [
                'required' => false,
                'label' => 'app.sonata_media_about_us.labels.url',
              ])
        ;
    }

}
