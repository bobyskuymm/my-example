<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GalleryItemAdmin extends Admin
{

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('description', null, [
                'label' => 'app.sonata_clinic.labels.description',
              ])

        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper


            ->add('photo', 'iphp_file', [
                'label' => 'app.sonata_doctor.labels.photo',
                'template' => 'AppBundle:CRUDDefault:_image.html.twig'
            ])


            ->add('description', null, [
                'label' => 'app.sonata_clinic.labels.description',
            ])

            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

            ->add('photo', 'iphp_file', [
                'label' => 'app.sonata_doctor.labels.photo',
                'required' => false,
                'allow_extra_fields' => true,
            ])

            ->add('description', 'textarea', [
                'label' => 'app.sonata_clinic.labels.description'
            ]);
    }
}
