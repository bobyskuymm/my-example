<?php

namespace AppBundle\Admin;
die;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClinicAdmin extends Admin
{

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'app.sonata_clinic.labels.name',
              ])
            ->add('location', null, [
                'label' => 'app.sonata_clinic.labels.location',
              ])
            ->add('moderated', null, [
                'label' => 'app.sonata_clinic.labels.moderated',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('name', null, [
                'label' => 'app.sonata_clinic.labels.name'
              ])
            ->add('ratingTotal', null, [
                'label' => 'app.sonata_clinic.labels.rating_total',
              ])
            ->add('hide', null, [
                'label' => 'app.sonata_clinic.labels.hide',
                // 'editable' => true,
              ])
            ->add('moderated', null, [
                'label' => 'app.sonata_clinic.labels.moderated',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with($this->trans('app.sonata_clinic.tabs.general'), [ 'tab' => true ])
                ->with(null)
                    ->add('moderated', null, [
                        'label' => 'app.sonata_clinic.labels.moderated',
                      ])
                    ->add('city', null, [
                        'label' => 'app.sonata_clinic.labels.city',
                    ])
                    ->add('area', null, [
                        'label' => 'app.sonata_clinic.labels.area',
                    ])
                    ->add('moderatorComment', null, [
                        'label' => 'app.sonata_clinic.labels.moderator_comment',
                      ])
                    ->add('showEnglish', null, [
                        'required' => false,
                        'label' => 'app.sonata_clinic.labels.show_english',
                      ])
                    ->add('user', null, [
                        'label' => 'app.sonata_clinic.labels.user',
                      ])

                    ->add('name', null, [
                        'label' => 'app.sonata_clinic.labels.name'
                      ])
                    ->add('location', null, [
                        'label' => 'app.sonata_clinic.labels.location'
                      ])
                    ->add('foundationYear', null, [
                        'label' => 'app.sonata_clinic.labels.foundation_year'
                      ])
                    ->add('address', null, [
                        'label' => 'app.sonata_clinic.labels.address'
                      ])
                    ->add('hide', null, [
                        'label' => 'app.sonata_clinic.labels.hide'
                      ])
                    ->add('url', null, [
                        'label' => 'app.sonata_clinic.labels.url'
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_clinic.tabs.about'), [ 'tab' => true ])
                ->with(null)
                    ->add('about', null, [
                        'label' => 'app.sonata_clinic.labels.about'
                      ])
                    ->add('shortAbout', null, [
                        'label' => 'app.sonata_clinic.labels.short_about'
                      ])
                    ->add('fullAbout', null, [
                        'label' => 'app.sonata_clinic.labels.full_about'
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_clinic.tabs.prices'), [ 'tab' => true ])
                ->with(null)
                    ->add('price', null, [
                        'label' => 'app.sonata_clinic.labels.price'
                      ])
                    ->add('specialOffer', null, [
                        'label' => 'app.sonata_clinic.labels.special_offer'
                      ])
                    ->add('services', 'sonata_type_collection', [
                        'label' => 'app.sonata_clinic.labels.services',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_clinic.tabs.photos'), [ 'tab' => true ])
                ->with(null)
                    ->add('photos', 'sonata_type_collection', [
                        'label' => 'app.sonata_clinic.labels.photos',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
            ->with($this->trans('app.sonata_clinic.tabs.documents'), [ 'tab' => true ])
                ->with(null)
                    ->add('documents', 'sonata_type_collection', [
                        'label' => 'app.sonata_clinic.labels.documents',
                        'required' => false,
                      ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'allow_add' => true,
                        'sortable' => 'position',
                      ])
                ->end()
            ->end()
        ;
    }


    public function prePersist($clinic)
    {
        foreach ($clinic->getDocuments() as $item) {
            $item->setClinic($clinic);
        }
        foreach ($clinic->getPhotos() as $item) {
            $item->setClinic($clinic);
        }
        foreach ($clinic->getServices() as $item) {
            $item->setClinic($clinic);
        }

    }

    public function preUpdate($clinic)
    {
        foreach ($clinic->getDocuments() as $item) {
            $item->setClinic($clinic);
        }
        foreach ($clinic->getPhotos() as $item) {
            $item->setClinic($clinic);
        }
        foreach ($clinic->getServices() as $item) {
            $item->setClinic($clinic);
        }
    }

}
