<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TopicAdmin extends Admin
{

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('create')
            ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, [
                'label' => 'app.sonata_topics.labels.title',
              ])
            ->add('section', null, [
                'label' => 'app.sonata_topics.labels.section',
                'operator_type' => 'sonata_type_boolean',
              ])
            ->add('sectionExist', 'doctrine_orm_callback', [
                'label' => 'app.sonata_topics.labels.section_does_not_exist',
                'callback' => [$this, 'getSectionDoesNotExist'],
                'field_type' => 'checkbox',
                'advanced_filter' => false,
              ])
            ->add('body', null, [
                'label' => 'app.sonata_topics.labels.body',
              ])
            ->add('createdBy', null, [
                'label' => 'app.sonata_topics.labels.created_by',
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_topics.labels.created_at',
              ])
        ;
    }


    public function getSectionDoesNotExist($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {
            return;
        }
        // dump(get_class_methods(get_class($queryBuilder->expr()))); die();
        // Use `andWhere` instead of `where` to prevent overriding existing `where` conditions
        $queryBuilder->andWhere(
            $queryBuilder->expr()->isNull($alias.'.section')
        );

        return true;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('section', null, [
                'label' => 'app.sonata_topics.labels.section',
              ])
            ->add('title', null, [
                'label' => 'app.sonata_topics.labels.title',
              ])
            ->add('ratingTotal', null, [
                'label' => 'app.sonata_topics.labels.rating_total',
              ])
            ->add('likeTotal', null, [
                'label' => 'app.sonata_topics.labels.like_total',
              ])
            ->add('createdBy', null, [
                'label' => 'app.sonata_topics.labels.created_by',
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_topics.labels.created_at',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('section', null, [
                'label' => 'app.sonata_topics.labels.section',
                'required' => true,
              ])
            ->add('title', null, [
                'label' => 'app.sonata_topics.labels.title',
              ])
            ->add('body', null, [
                'label' => 'app.sonata_topics.labels.body',
              ])
            ->add('image', 'iphp_file', [
                'label' => 'app.sonata_topics.labels.image',
                'required' => false,
              ])
        ;
    }

}
