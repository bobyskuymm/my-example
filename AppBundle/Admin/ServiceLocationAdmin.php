<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServiceLocationAdmin extends Admin
{
    public $baseUrl = "";

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    private $container;

    public function setContainer($c)
    {
        $this->container = $c;
    }

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'app.sonata_clinic.labels.name',
              ])

        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $this->baseUrl = $this->container->getParameter('base_url');

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('name', null, [
                'label' => 'app.sonata_clinic.labels.name'
              ])
            ->add('image', null, [
                'label' => 'app.sonata_groups.labels.image',
                'template' => 'AppBundle:CRUDDefault:_image.html.twig'
            ])

            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with($this->trans('app.sonata_clinic.tabs.general'), [ 'tab' => true ])

                    ->add('name', null, [
                        'label' => 'app.sonata_clinic.labels.name'
                      ])
            ->add('image', 'iphp_file', [
                // 'data_class' => null,
                'required' => true,
                'label' => 'app.sonata_groups.labels.image',
            ])
        ;
    }


    public function prePersist($clinic)
    {


    }

    public function preUpdate($clinic)
    {

    }

}
