<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class BannerAdmin extends Admin
{
    public $baseUrl = "";
    public $last_position = 0;

    private $positionService;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    private $container;

    public function setContainer($c)
    {
        $this->container = $c;
    }

    public function setPositionService(\Pix\SortableBehaviorBundle\Services\PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection->remove('show');
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'app.sonata_clinic.labels.name',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
       $this->baseUrl = $this->container->getParameter('base_url');

        $this->last_position = $this->positionService->getLastPosition($this->getRoot()->getClass());


        unset($this->listModes['mosaic']);

        $listMapper
            ->add('name', null, [
                'label' => 'app.sonata_clinic.labels.name'
              ])
            ->add('location', null, [
                'label' => 'app.sonata_clinic.labels.location',
            ])
            ->add('link', null, [
                'label' => 'app.sonata_groups.labels.link'
              ])
            ->add('type', null, [
                'label' => 'app.sonata_groups.labels.type',
                'template' => 'AppBundle:CRUDDefault:_banner_type.html.twig'
              ])
            ->add('image', null, [
                'label' => 'app.sonata_groups.labels.image',
                'template' => 'AppBundle:CRUDDefault:_image.html.twig'
              ])
            ->add('enabled', null, [
                'editable' => true,
                'label' => 'app.sonata_groups.labels.isactive',
            ])
            ->add('_action', null, array(
                'actions' => array(
                    'move' => array(
                        'template' => 'AppBundle:CRUDDefault:_sort.html.twig'
                    ),
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),

                )))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with($this->trans('app.sonata_clinic.tabs.general'), [ 'tab' => true ])

                    ->add('name', null, [
                        'label' => 'app.sonata_clinic.labels.name'
                      ])
                    ->add('location', null, [
                        'label' => 'app.sonata_clinic.labels.location',
                        'required' => true,
                    ])
                    ->add('link', null, [
                        'label' => 'app.sonata_groups.labels.link',
                        'required' => false,
                      ])
                    ->add('type', 'choice', array(
                        'label' => 'app.sonata_groups.labels.type',
                        'choices' => array(
                            1 => 'Пакетные услуги',
                            2 => 'Доп. услуги',
                            3 => 'Спец. предложения',
                            4 => 'Документы',
                            5 => 'Каталог'
                        ),
                    ))
                    ->add('image', 'iphp_file', [
                        // 'data_class' => null,
                        'required' => true,
                        'label' => 'app.sonata_groups.labels.image',
                    ])
                    ->add('enabled', null, [
                        'required' => false,
                        'label' => 'app.sonata_groups.labels.isactive',
                    ])

        ;
    }


    public function prePersist($clinic)
    {


    }

    public function preUpdate($clinic)
    {

    }

}
