<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PageAdmin extends Admin
{

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('name', null, [
                'label' => 'app.sonata_pages.labels.name',
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_pages.labels.createdAt',
              ])
            ->add('updatedAt', null, [
                'label' => 'app.sonata_pages.labels.updatedAt',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, [
                'label' => 'app.sonata_pages.labels.name',
              ])
            ->add('body', 'ckeditor', [
                'label' => 'app.sonata_pages.labels.body',
                'required' => true,
              ])
            ->add('seoTitle', null, [
                'label' => 'app.sonata_pages.labels.seoTitle',
              ])
            ->add('seoKeywords', null, [
                'label' => 'app.sonata_pages.labels.seoKeywords',
              ])
            ->add('seoDescription', null, [
                'label' => 'app.sonata_pages.labels.seoDescription',
              ])
            ->add('url', null, [
                'label' => 'app.sonata_pages.labels.url',
                'required' => false,
              ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name', null, [
                'label' => 'app.sonata_pages.labels.name',
              ])
            ->add('body', 'ckeditor', [
                'label' => 'app.sonata_pages.labels.body',
                'config' => [
                    'toolbar' => 'full_toolbar',
                    'filebrowserImageUploadUrl' => 'app_page_upload_image',
                    'filebrowserUploadRouteParameters' => [
                    ],
                ]
              ])
            ->add('seoTitle', null, [
                'label' => 'app.sonata_pages.labels.seoTitle',
              ])
            ->add('seoKeywords', null, [
                'label' => 'app.sonata_pages.labels.seoKeywords',
              ])
            ->add('seoDescription', null, [
                'label' => 'app.sonata_pages.labels.seoDescription',
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_pages.labels.createdAt',
              ])
            ->add('updatedAt', null, [
                'label' => 'app.sonata_pages.labels.updatedAt',
              ])
            ->add('url', null, [
                'label' => 'app.sonata_pages.labels.url',
              ])
        ;
    }
}
