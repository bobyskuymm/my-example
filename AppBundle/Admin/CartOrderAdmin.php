<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class CartOrderAdmin extends Admin
{
    public $baseUrl = "";

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public $container;

    public function setContainer($c)
    {
        $this->container = $c;
    }


    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper

            ->add('id', null, [
                'label' => 'Номер',
            ])
            ->add('user', null, [
                'label' => 'Пользовтель',
            ])
            ->add('totalSum', null, [
                'label' => 'Общая сумма',
            ])
            ->add('status', null, [
                'label' => 'app.sonata_groups.labels.status',
                'template' => 'AppBundle:CRUDDefault:_status_order_type.html.twig'
            ])
            ->add('createdAt', 'date', ['pattern' => 'dd MMM y Y', 'label' => 'Дата'])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                )))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with($this->trans('app.sonata_clinic.tabs.general'), [ 'tab' => true ])

                ->add('status', 'choice', array(
                    'label' => 'app.sonata_groups.labels.status',
                    'choices' => array(
                        1 => 'Новый',
                        2 => 'В работе',
                        3 => 'Завершенный',
                        4 => 'Отклонен ',
                    ),
                ))
        ;
    }


    public function prePersist($clinic)
    {


    }

    public function preUpdate($clinic)
    {

    }

}
