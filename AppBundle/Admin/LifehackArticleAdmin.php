<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class LifehackArticleAdmin extends Admin
{

    protected $baseRouteName = 'sonata_lifehack_article';
    protected $baseRoutePattern = 'lifehack_article';


    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'app.sonata_lifehack_article.labels.name',
              ])
            ->add('section', null, [
                'label' => 'app.sonata_lifehack_article.labels.section',
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_lifehack_article.labels.createdAt',
              ])
            ->add('updatedAt', null, [
                'label' => 'app.sonata_lifehack_article.labels.updatedAt',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('name', null, [
                'label' => 'app.sonata_lifehack_article.labels.name',
              ])
            ->add('section', null, [
                'label' => 'app.sonata_lifehack_article.labels.section',
                'required' => true,
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_lifehack_article.labels.createdAt',
              ])
            ->add('updatedAt', null, [
                'label' => 'app.sonata_lifehack_article.labels.updatedAt',
              ])
            ->add('createdBy', null, [
                'label' => 'app.sonata_lifehack_article.labels.createdBy',
              ])
            ->add('updatedBy', null, [
                'label' => 'app.sonata_lifehack_article.labels.updatedBy',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, [
                'label' => 'app.sonata_lifehack_article.labels.name',
              ])
            ->add('section', null, [
                'label' => 'app.sonata_lifehack_article.labels.section',
              ])
            ->add('tags', null, [
                'label' => 'app.sonata_lifehack_article.labels.tags',
              ])
            ->add('shortDescription', 'ckeditor', [
                'label' => 'app.sonata_lifehack_article.labels.shortDescription',
              ])
            ->add('body', 'ckeditor', [
                'label' => 'app.sonata_lifehack_article.labels.body',
              ])
            ->add('seoTitle', null, [
                'label' => 'app.sonata_lifehack_article.labels.seoTitle',
              ])
            ->add('seoKeywords', null, [
                'label' => 'app.sonata_lifehack_article.labels.seoKeywords',
              ])
            ->add('seoDescription', null, [
                'label' => 'app.sonata_lifehack_article.labels.seoDescription',
              ])
            ->add('image', 'iphp_file', [
                'label' => 'app.sonata_lifehack_article.labels.image',
                'required' => false,
              ])

            ->add('url', null, [
                'required' => false,
                'label' => 'app.sonata_lifehack_article.labels.url',
              ])
            ->add('linkedClinics', null, [
                'required' => false,
                'label' => 'app.sonata_lifehack_article.labels.linked_clinics',
              ])
        ;
    }


    public function setDoctrine($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $this->parseForClinics($object);
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $this->parseForClinics($object);
    }

    protected function parseForClinics($object)
    {
        $clinics = $this->doctrine->getRepository('AppBundle:Clinic')
                   ->findBy(['moderated' => true, 'hide' => false]);

        if ($object->getLinkedClinics()) {
            foreach ($object->getLinkedClinics() as $clinic) {
                $object->removeLinkedClinic($clinic);
            }
        }

        foreach ($clinics as $clinic) {
            $allText = $object->getName() . $object->getShortDescription() . $object->getBody();

            if (preg_match('/' . $clinic->getName() . '/', $allText)) {
                $object->addLinkedClinic($clinic);
            }

        }

        $this->doctrine->getManager()->persist($object);
        $this->doctrine->getManager()->flush();

    }

}
