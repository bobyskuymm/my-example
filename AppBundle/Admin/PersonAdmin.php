<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PersonAdmin extends Admin
{


    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('name', null, [
                'label' => 'app.sonata_persons.labels.name',
              ])
            ->add('slogan', null, [
                'label' => 'app.sonata_persons.labels.slogan',
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_persons.labels.created_at',
              ])
            ->add('updatedAt', null, [
                'label' => 'app.sonata_persons.labels.updated_at',
              ])
            ->add('createdBy', null, [
                'label' => 'app.sonata_persons.labels.created_by',
              ])
            ->add('updatedBy', null, [
                'label' => 'app.sonata_persons.labels.updated_by',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, [
                'label' => 'app.sonata_persons.labels.name',
              ])
            ->add('photo', 'iphp_file', [
                'required' => false,
                'label' => 'app.sonata_persons.labels.photo',

              ])
            ->add('occupation', null, [
                'label' => 'app.sonata_persons.labels.occupation',
              ])
            ->add('slogan', null, [
                'label' => 'app.sonata_persons.labels.slogan',
              ])
            ->add('about', 'ckeditor', [
                'required' => false,
                'label' => 'app.sonata_persons.labels.about',

              ])
            ->add('position', null, [
                'label' => 'app.sonata_persons.labels.position',
              ])
            // ->add('url', null, [
            //     'required' => false,
            //   ])
        ;
    }

}
