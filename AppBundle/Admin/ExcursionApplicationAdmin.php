<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ExcursionApplicationAdmin extends Admin
{

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('delete')
            ->remove('create')
            ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('isProcessed', null, [
                'label' => 'app.sonata_excursion_applications.labels.is_processed',
              ])
            ->add('clinic', null, [
                'label' => 'app.sonata_excursion_applications.labels.clinic',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        unset($this->listModes['mosaic']);

        $listMapper
            ->add('clinic', null, [
                'label' => 'app.sonata_excursion_applications.labels.clinic',
              ])
            ->add('user', null, [
                'label' => 'app.sonata_excursion_applications.labels.user',
              ])
            ->add('date', null, [
                'label' => 'app.sonata_excursion_applications.labels.date',
              ])
            ->add('phone', null, [
                'label' => 'app.sonata_excursion_applications.labels.phone',
              ])
            ->add('email', null, [
                'label' => 'app.sonata_excursion_applications.labels.email',
              ])
            ->add('isProcessed', null, [
                'label' => 'app.sonata_excursion_applications.labels.is_processed',
              ])
            ->add('createdAt', null, [
                'label' => 'app.sonata_excursion_applications.labels.created_at',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('clinic', null, [
                'label' => 'app.sonata_excursion_applications.labels.clinic',
              ])
            ->add('user', null, [
                'label' => 'app.sonata_excursion_applications.labels.user',
              ])
            ->add('date', null, [
                'label' => 'app.sonata_excursion_applications.labels.date',
              ])
            ->add('phone', null, [
                'label' => 'app.sonata_excursion_applications.labels.phone',
              ])
            ->add('email', null, [
                'label' => 'app.sonata_excursion_applications.labels.email',
              ])
            ->add('isProcessed', null, [
                'required' => false,
                'label' => 'app.sonata_excursion_applications.labels.is_processed',
              ])
        ;
    }

}
